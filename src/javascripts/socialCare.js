

$(function(){

  



    $('#socOptions').on('change', function() {
      
        $(".svgs").remove();
        $(".svgs2").remove();
        $(".svgs3").remove();
        $("#Trend").removeClass("hidden");

        var selectedText = $("#socOptions option:selected").html();
        

        
        selectedText = makeAMP(selectedText);

        $(".labelIndicator").text(selectedText);
        $(".subtit").text("Adult Social Care 2019");
          
          if ( this.value == 'community')
          {
        
            sampleFunc("/socialCare/adult-social-care-in-bristol-by-ward.json",
"clients_receiving_a_community_based_service_aged_65_rate_per_1_000","year_as_at_march_end",["2019","2018","2017"],"&");
         
    
          }
          else if ( this.value == 'social')
          {
            sampleFunc("/socialCare/children-in-social-care-within-bristol-by-ward.json",
"total_children_in_social_care_by_ward_rate_per_1000_child_population","year",["2019","2018","2017"],"&");
         
          }
          else if ( this.value == 'homes')
          {
            sampleFunc("/socialCare/adult-social-care-in-bristol-by-ward.json",
"clients_in_care_homes_aged_65_rate_per_1_000","year_as_at_march_end",["2019","2018","2017"],"&");
         
           
          }
          else if ( this.value == 'domestic')
          {
            sampleFunc("/socialCare/adult-social-care-in-bristol-by-ward.json",
"clients_receiving_a_domestic_care_services_aged_65_rate_per_1_000","year_as_at_march_end",["2019","2018","2017"],"&");
         
          }
          else if ( this.value == 'community2')
          {
            sampleFunc("/socialCare/adult-social-care-in-bristol-by-ward.json",
"clients_receiving_a_community_based_service_aged_18_64_rate_per_1_000","year_as_at_march_end",["2019","2018","2017"],"&");
         
          
          }
          else if ( this.value == 'lonely')
          {
             sampleFuncForQofL("/q_of_life/qol-indicator-data-rag-ratings.json","% who feel lonely because they don't see friends and family enough","&"); 
             
             trend("/q_of_life/qol-indicators-trend-ward-profile-2019.json","% who feel lonely because they don't see friends and family enough","&",["2018","2017","2015"]); 

             //$("#Trend").addClass("hidden");
             $(".labelIndicator").text("% "+selectedText);
             $(".subtit").text("Quality of Life Survey 2018-19");
            }
          else if ( this.value == 'phys')
          {
            sampleFuncForQofL("/q_of_life/qol-indicator-data-rag-ratings.json","% whose physical health prevents them from leaving their home when they want to","&"); 
             
             trend("/q_of_life/qol-indicators-trend-ward-profile-2019.json","% whose physical health prevents them from leaving their home when they want to","&",["2018","2017","2015"]); 

             $(".labelIndicator").text("% "+selectedText);
             $(".subtit").text("Quality of Life Survey 2018-19");
          }
          else
          {
            console.log("Indicator not found:",this.value);
          }
          
       
        });//end of if statement
        
        $('#socOptions').val("community").trigger('change');
        
        
        });//end of script