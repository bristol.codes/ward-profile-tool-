
$(function(){

   
    function housing(fileName,sign){
        var totFlat=0
        var totHouse=0
        var tempArray =[];
     
        $.getJSON( "../../Json/"+fileName, function( data ) {
       
            var testing = changeName(ward,sign);
          
            ward = testing;

            $.each(data, function(i, obj) {

               
                var wardN = data[i].fields["2016_ward_name"];
                var space = data[i].fields.total_number_of_household_spaces;

                var terraced =  parseInt(data[i].fields.terraced);
                var semi =  data[i].fields.semi_detached;
                var detached = data[i].fields.detached;

               
                totHouse = terraced + semi + detached;
             

                var flatComm = data[i].fields.flat_in_commercial_building;
                var flatShared = data[i].fields.flat_part_of_a_converted_or_shared_house_including_bed_sits;
                var flatBlock =  parseInt(data[i].fields.flat_purpose_built_block_of_flats);

               
                totFlat= flatComm + flatShared + flatBlock;
               
                
                var someData = {name: wardN, house: totHouse, flat: totFlat,space:space};
            
            
                tempArray.push(someData);




            });
            

            sampleStck(tempArray);
            
        });    




    };

    function tenure(fileName,sign){

        $.getJSON( "../../Json/"+fileName, function( data ) {

            

            var testing = changeName(ward,sign);

            var ward2 = ward;
          
            ward = testing;

            var ownedNrBrs = 0;

            var prvNrBrs = 0;

            var socNrBrs = 0;

            var tempArray = [];

            var tempArray2 =[];
            var total =0;


            var bristolProcCalcOwned =0;

            var bristolProcCalcSocial =0;

            var bristolProcCalcPrivate =0;
       
            var countWard =0;
            
            var eachWardNrOwned =[];
            var eachWardNrPrivate =[];
            var eachWardNrSocial =[];
       
            

            var mean =0;
            var mean2 =0;
            var mean3 =0;
     
            $.each(data, function(i, obj) {

               

                var wardN = data[i].fields["2016_ward_name"];
                var totH = data[i].fields.total_number_of_households;
                var ownedNr = data[i].fields.owned;
                var ownedProc= to15sf(ownedNr/totH*100);// data[i].fields.owned0;
                var prvNr = data[i].fields.private_and_other_rented;
                var prvPrc = to15sf(prvNr/totH*100);// data[i].fields.private_and_other_rented0;
                var socNr = data[i].fields.social_rented;
                var socPrc = to15sf(socNr/totH *100);// data[i].fields.social_rented0;
                
                ownedNrBrs = ownedNrBrs + ownedNr;

                prvNrBrs = prvNrBrs + prvNr;

                socNrBrs = socNrBrs + socNr;

                total = total + totH;


                if (wardN == ward){

                    
                    tempArray2.push(ownedNr,prvNr,socNr,ownedProc,prvPrc,socPrc);


                }

                if((wardN !== "Bristol" ) && (wardN !== "England & Wales")){
    
                    mean = mean + ownedProc;
                    mean2 = mean2 + prvPrc;
                    mean3 = mean3 + socPrc;
              
                   

                    countWard++;

                    eachWardNrOwned.push(ownedProc);
                    eachWardNrPrivate.push(prvPrc);
                    eachWardNrSocial.push(socPrc);
              
                  }



            });

          

            var bristolProcCalcOwned = to15sf(ownedNrBrs / total * 100);

            var bristolProcCalcSocial = to15sf(socNrBrs / total * 100);

            var bristolProcCalcPrivate = to15sf(prvNrBrs / total * 100);


            tempArray.push(ownedNrBrs,prvNrBrs,socNrBrs,bristolProcCalcOwned,bristolProcCalcPrivate,bristolProcCalcSocial);

            
            


            var someData2 = {
                wardOwnedNr: tempArray2[0],
                wardPrivateNr: tempArray2[1],
                wardSocialNr: tempArray2[2],
                wardOwnedPrc: tempArray2[3],
                wardPrivatePrc: tempArray2[4],
                wardSocialPrc: tempArray2[5],
                brsOwnedNr: tempArray[0],
                brsPrivateNr: tempArray[1],
                brsSocialNr: tempArray[2],
                brsOwnedPrc: tempArray[3],
                brsPrivatePrc: tempArray[4],
                brsSocialPrc: tempArray[5]
            };


            var standardDeviationOwned = standardDeviation(mean,countWard,eachWardNrOwned);
            var standardDeviationPrivate = standardDeviation(mean2,countWard,eachWardNrPrivate);
            var standardDeviationSocial = standardDeviation(mean3,countWard,eachWardNrSocial);

     

            var clrs = colStd(fileName,tempArray2[3],tempArray[3],standardDeviationOwned);
            var clrs2 = colStd(fileName,tempArray2[5],tempArray[5],standardDeviationSocial);
            var clrs3 = colStd(fileName,tempArray2[4],tempArray[4],standardDeviationPrivate);

            var clrArray = [clrs,clrs2,clrs3];
           
            
            makeTenure(someData2,clrArray);
        
        
        });        

   

    };

    function makeTenure(data,clrArray){

       

        $(".owned").find(".ward").find(".nr").append(numberWithCommas(data.wardOwnedNr));
        $(".owned").find(".ward").find(".textPrc").append(oneDcm(data.wardOwnedPrc)+"%");
        $(".owned").find(".ward").find(".dot").addClass(clrArray[0]);

        $(".owned").find(".bristol").find(".nr").append(numberWithCommas(data.brsOwnedNr));
        $(".owned").find(".bristol").find(".textPrc").append(oneDcm(data.brsOwnedPrc)+"%");

        $(".social").find(".ward").find(".nr").append(numberWithCommas(data.wardSocialNr));
        $(".social").find(".ward").find(".textPrc").append(oneDcm(data.wardSocialPrc)+"%");
        $(".social").find(".ward").find(".dot").addClass(clrArray[1]);

        $(".social").find(".bristol").find(".nr").append(numberWithCommas(data.brsSocialNr));
        $(".social").find(".bristol").find(".textPrc").append(oneDcm(data.brsSocialPrc)+"%");

        $(".private").find(".ward").find(".nr").append(numberWithCommas(data.wardPrivateNr));
        $(".private").find(".ward").find(".textPrc").append(oneDcm(data.wardPrivatePrc)+"%");
        $(".private").find(".ward").find(".dot").addClass(clrArray[2]);

        $(".private").find(".bristol").find(".nr").append(numberWithCommas(data.brsPrivateNr));
        $(".private").find(".bristol").find(".textPrc").append(oneDcm(data.brsPrivatePrc)+"%");





    };

    function accomodation(fileName,sign){

        var tempArray =[];
        var tempArray2 =[];


        var terracedBrs =0;
        var terracedProcBrs = 0;

        var semiBrs =  0;
        var semiProcBrs =  0;

        var detachedBrs = 0;
        var detachedProcBrs =0;


        var flatCommBrs = 0;
        var flatCommProcBrs = 0;

        var flatSharedBrs = 0;
        var flatSharedProcBrs =0;

        var flatBlockBrs =  0;
        var flatBlockProcBrs =  0;

        var totFlat =0;
        var totflatProc =0;
        
        var totFlatBrs =0;
        var totflatProcBrs =0;

        var brsTotSpace =0;

        var jsonData ={};

        var countWard =0;
        
        var eachWardNrDetached =[];
        var eachWardNrSemi =[];
        var eachWardNrTerraced =[];
        var eachWardNrFlat =[];
        

        var mean =0;
        var mean2 =0;
        var mean3 =0;
        var mean4 =0;
        
        $.getJSON( "../../Json/"+fileName, function( data ) {

            var testing = changeName(ward,sign);

            ward = testing;

        

            $.each(data, function(i, obj) {

                var wardN = data[i].fields["2016_ward_name"];
                var space = data[i].fields.total_number_of_household_spaces;

                var terraced =  parseInt(data[i].fields.terraced);
                var terracedProc = to15sf(terraced/space*100); //data[i].fields.terraced0;

                var semi =  data[i].fields.semi_detached;
                var semiProc =  to15sf(semi/space*100); // data[i].fields.semi_detached0;

                var detached = data[i].fields.detached;
                var detachedProc = to15sf(detached/space*100); // data[i].fields.detached0;


                var flatComm = data[i].fields.flat_in_commercial_building;
                var flatCommProc = to15sf(flatComm/space*100); // data[i].fields.flat_in_commercial_building0;

                var flatShared = data[i].fields.flat_part_of_a_converted_or_shared_house_including_bed_sits;
                var flatSharedProc = to15sf(flatShared/space*100); // data[i].fields.flat_part_of_a_converted_or_shared_house_including_bed_sits0;

                var flatBlock =  parseInt(data[i].fields.flat_purpose_built_block_of_flats);
                var flatBlockProc = to15sf(flatBlock/space*100); //  data[i].fields.flat_purpose_built_block_of_flats0;

               
                totFlat= flatComm + flatShared + flatBlock;
                totflatProc = flatCommProc + flatSharedProc + flatBlockProc;

                terracedBrs = terracedBrs + terraced;
        
                semiBrs =  semiBrs + semi;
        
                detachedBrs = detachedBrs + detached;
        
                flatCommBrs = flatCommBrs + flatComm;
        
                flatSharedBrs = flatSharedBrs + flatShared;
        
                flatBlockBrs =  flatBlockBrs + flatBlock;
                
                totFlatBrs = flatCommBrs + flatSharedBrs + flatBlockBrs;
                


                brsTotSpace = brsTotSpace + space;

                if(wardN == ward){
            
                    tempArray.push(detached,detachedProc,semi,semiProc,terraced,terracedProc,totFlat,totflatProc);

                }

                if((wardN !== "Bristol" ) && (wardN !== "England & Wales")){
    
                    mean = mean + detachedProc;
                    mean2 = mean2 + semiProc;
                    mean3 = mean3 + terracedProc;
                    mean4 = mean4 + totflatProc;
              
                    countWard++;

                    eachWardNrDetached.push(detachedProc);
                    eachWardNrSemi.push(semiProc);
                    eachWardNrTerraced.push(terracedProc);
                    eachWardNrFlat.push(totflatProc);
              
                  }

            });    

            var newTotalFlatPrc = to15sf(totFlatBrs / brsTotSpace * 100);

            var newTotalTerracedPrc = to15sf(terracedBrs / brsTotSpace * 100);

            var newTotalSemiPrc = to15sf(semiBrs / brsTotSpace * 100);

            var newTotalDetached = to15sf(detachedBrs / brsTotSpace * 100);

            
            tempArray2.push(detachedBrs,newTotalDetached,semiBrs,newTotalSemiPrc,terracedBrs,newTotalTerracedPrc,totFlatBrs,newTotalFlatPrc);

            jsonData = {wardDetachedNr:tempArray[0],wardDetachedProc:tempArray[1],wardSemiNr:tempArray[2],wardSemiPrc:tempArray[3],wardTerracedNr:tempArray[4],wardTerracedPrc:tempArray[5],wardFlatNr:tempArray[6],wardFlatPrc:tempArray[7],brsDetachedNr:tempArray2[0],brsDetachedProc:tempArray2[1],brsSemiNr:tempArray2[2],brsSemiPrc:tempArray2[3],brsTerracedNr:tempArray2[4],brsTerracedPrc:tempArray2[5],brsFlatNr:tempArray2[6],brsFlatPrc:tempArray2[7]};

            var standard = standardDeviation(mean,countWard,eachWardNrDetached);
            var standard2 = standardDeviation(mean2,countWard,eachWardNrSemi);
            var standard3 = standardDeviation(mean3,countWard,eachWardNrTerraced);
            var standard4 = standardDeviation(mean3,countWard,eachWardNrFlat);

     
            var clrs = colStd(fileName,tempArray[1],tempArray2[1],standard); // this was using tempArray2[3] !!
            var clrs2 = colStd(fileName,tempArray[3],tempArray2[3],standard2);
            var clrs3 = colStd(fileName,tempArray[5],tempArray2[5],standard3);
            var clrs4 = colStd(fileName,tempArray[7],tempArray2[7],standard4);
      
            
            var clrArray = [clrs,clrs2,clrs3,clrs4];

            makeAccomodation(jsonData,clrArray);

            
        });  
       
    };       

    function makeAccomodation(data,clrArray){


        

        $(".detached").find(".ward").find(".nr").append(numberWithCommas(data.wardDetachedNr));
        $(".detached").find(".ward").find(".textPrc").append(oneDcm(data.wardDetachedProc)+"%");
        $(".detached").find(".ward").find(".dot").addClass(clrArray[0]);

        $(".detached").find(".bristol").find(".nr").append(numberWithCommas(data.brsDetachedNr));
        $(".detached").find(".bristol").find(".textPrc").append(oneDcm(data.brsDetachedProc)+"%");

        $(".semi-detached").find(".ward").find(".nr").append(numberWithCommas(data.wardSemiNr));
        $(".semi-detached").find(".ward").find(".textPrc").append(oneDcm(data.wardSemiPrc)+"%");
        $(".semi-detached").find(".ward").find(".dot").addClass(clrArray[1]);
        

        $(".semi-detached").find(".bristol").find(".nr").append(numberWithCommas(data.brsSemiNr));
        $(".semi-detached").find(".bristol").find(".textPrc").append(oneDcm(data.brsSemiPrc)+"%");

        $(".terraced").find(".ward").find(".nr").append(numberWithCommas(data.wardTerracedNr));
        $(".terraced").find(".ward").find(".textPrc").append(oneDcm(data.wardTerracedPrc)+"%");
        $(".terraced").find(".ward").find(".dot").addClass(clrArray[2]);

        $(".terraced").find(".bristol").find(".nr").append(numberWithCommas(data.brsTerracedNr));
        $(".terraced").find(".bristol").find(".textPrc").append(oneDcm(data.brsTerracedPrc)+"%");

        $(".flat").find(".ward").find(".nr").append(numberWithCommas(data.wardFlatNr));
        $(".flat").find(".ward").find(".textPrc").append(oneDcm(data.wardFlatPrc)+"%");
        $(".flat").find(".ward").find(".dot").addClass(clrArray[3]);

        $(".flat").find(".bristol").find(".nr").append(numberWithCommas(data.brsFlatNr));
        $(".flat").find(".bristol").find(".textPrc").append(oneDcm(data.brsFlatPrc)+"%");



    }


    housing("/housing/housing-type-2011-census-by-2016-ward.json","&");
     
    tenure("/housing/housing-tenure-2011-census-by-2016-ward.json","&");

    accomodation("/housing/housing-type-2011-census-by-2016-ward.json","&");

    

});    