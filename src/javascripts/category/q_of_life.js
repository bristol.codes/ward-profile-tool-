   
    function q_of_life(fileName,sign,indicator,tit,checker){
      const numberOfDecimalPlaces=0;
      
        var testing = changeName(ward,sign);
      
      
        ward = testing;
      
        var info= 0;
        var info2 = 0;
        var colour = "red"; // this is using the css classes .red .amber etc from the field significance_colour
        var brsIndicator = 0;
      
        $.getJSON( "../../Json/"+fileName, function( data ) {
      
      
          $.each(data, function(i, obj) {
            
            //var wardN = data[i]["Ward Name"];
            var wardN = obj.fields.ward_name;
            
            //var signi = data[i]["Significance colour"];
            var signi = obj.fields.significance_colour;
  
            // if(indicator == data[i].Indicator){
            if(indicator == obj.fields.indicator){
              
            
       
              
              if (wardN == ward){  
      
                // info = to15sf(data[i].Statistic);   
                info = to15sf(obj.fields.statistic);   
                colour = signi;
             
                
              } 
              if (wardN == ".Bristol Average"){
                
                // info2 = to15sf(data[i].Statistic);
                info2 = to15sf(obj.fields.statistic);
              }


            }
      
          });
     
          if (checker == false){
            $(".catHolder").find(tit).find(".pHQ").append("<div class='ql_inidicator'><p class='qIndicator'>"+indicator +"</p><div class='wInfoC'><div class='wInfo "+colour+"'>"+correctDp(info,numberOfDecimalPlaces)+ "</div></div><div class='brInfoC'><div class='brInfo'>"+ correctDp(info2,numberOfDecimalPlaces) +"</div></div></div>");
          }else if (checker == true){
            $(".catHolder").prepend("<div class='ql_inidicator'><p class='qIndicator noBorder'></p><div class='wInfoC'><div class='wrInd'>"+ward+"</div><div class='wInfo"+colour+"'></div></div><div class='brInfoC space'><div class='brInd'>Bristol</div><div class='brInfo'></div></div></div>");
           
            $(".catHolder").find(tit).find(".pHQ").append("<div class='ql_inidicator'><p class='qIndicator'>"+indicator +"</p><div class='wInfoC'><div class='wInfo space "+colour+"'>"+correctDp(info,numberOfDecimalPlaces)+ "</div></div><div class='brInfoC space'><div class='brInfo'>"+ correctDp(info2,numberOfDecimalPlaces) +"</div></div></div>");
            //$(".wInfoC").first().append("<div class='wrInd'>"+ward+"</div>");
           
            
          }else{
            $(".catHolder").find(tit).find(".pHQ").append("<div class='ql_inidicator'><p class='qIndicator'>"+indicator +"</p><div class='wInfoC'><div class='wInfo "+colour+"'>"+correctDp(info,numberOfDecimalPlaces)+ "</div></div><div class='brInfoC'><div class='brInfo'>"+ correctDp(info2,numberOfDecimalPlaces) +"</div></div></div>");
           }  
        });

      
      };
  
      function ql(arr1,arr2,arr3){

        var biggestNumbArray = Math.max(arr1.length,arr2.length,arr3.length);
    
        var checker = false;
        const jsonFilename="/q_of_life/qol-indicator-data-rag-ratings.json";
        //"/q_of_life/QoL_Indicators_overview.json";

        for (var i =0;i <biggestNumbArray;i++){
          
          if (i == 0){
            checker = true;
          }else if (i > 0){
            checker = false;
          }
    
          if (arr1.length > i){
            q_of_life(jsonFilename,"&",arr1[i],".cd",checker);
          }
          if (arr2.length > i){
            q_of_life(jsonFilename,"&",arr2[i],".cl");
          }
          if (arr3.length > i){
            q_of_life(jsonFilename,"&",arr3[i],".cs");
          }
         
        }
    };