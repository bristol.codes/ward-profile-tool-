$(function(){

      var council_and_democracyArr =["% satisfied with the way the Council runs things"
      ,"% who feel Bristol City Council provides value for money"
      ,"% who feel an elected mayor is improving the leadership of the city"
      ,"% who agree they can influence decisions that affect their local area"
      ,"% who agree they can influence decisions that affect the public services they use"];

      var community_and_living = ["% who feel they belong to their neighbourhood",
      "% who agree people from different backgrounds get on well together in their neighbourhood",
      "% who volunteer or help out in their community at least 3 times a year",
      "% who think noise from residential neighbours is a problem",
      "% who lack the information to get involved in their community",
      '% whose local area has changed due to "gentrification"',
      "% who find it difficult to manage financially",
      // "\n% satisfied with their local area"]; 
      // carriage return not needed if using qol-indicator-data-rag-ratings.json instead of QoL_Indicators_overview.json
      "% satisfied with their local area"];


      var crime_and_safety = ["% whose fear of crime affects their day-to-day lives",
      "% who feel crime and safety has got worse in their area in the last 3 years",
      "% who feel police and public services successfully tackle crime and anti-social behaviour locally",
      "% who think domestic abuse is a private matter"];
    

      ql(council_and_democracyArr,community_and_living,crime_and_safety);
     
    });
      
   

