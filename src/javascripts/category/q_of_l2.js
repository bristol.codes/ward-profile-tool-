$(function(){
    var health_and_wellbeing =["% satisfied with life"
    ,"% in good health"
    ,"% below average mental wellbeing"
    ,"% who see friends and family as much as they want to"
    ,"% who do enough regular exercise each week (at least 150 mins moderate or 75 mins vigorous exercise)"
    ,"% who play sport at least once a week"
    ,"% households with a smoker"
    ,"% who drink alcohol 4 or more times per week"
    ,"% who eat at least five portions of fruit or vegetables per day"

];

    var transport = ["% who think traffic congestion is a problem locally",
    "% who think air quality and traffic pollution is a problem locally",
    "% who ride a bicycle at least once a week",
    "% satisfied with the local bus service"];


    var education_and_skills = ["% who need to develop at least one of their skills",
    "% who know where to get information, advice and guidance about employment and training",
    "% satisfied with adult learning opportunities"];
  
    ql(health_and_wellbeing,transport,education_and_skills);

  });
    
 

