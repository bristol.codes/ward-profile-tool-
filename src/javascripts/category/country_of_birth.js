$(function(){

    function country_of_birth(fileName,sign){
 

        $.getJSON( "../../Json/"+fileName, function( data ) {

            var testing = changeName(ward,sign);
              
            ward = testing;
            
            var countryList = [];
            var tempTopThree =[];
      
                    var antarctica=0;
                    var australia =0;
                    var bangladesh =0;
                    //data[i].fields.born_in_central_america: 1
                    //data[i].fields.born_in_central_asia: 0
                    var china =0;
                    var france =0;
                    var germany =0;
                    var ghana = 0;
                    var hong_kong = 0;
                    var india = 0;
                    var iran = 0;
                    var ireland = 0;
                    var italy = 0;
                    var jamaica = 0;
                    var kenya = 0;
                    var lithuania = 0;
                    var nigeria = 0;
                  
                    var pakistan =0;
                    var philippines =0;
                    var poland = 0;
                    var portugal = 0;
                    var romania = 0;
                    var somalia = 0;
                    var south_africa = 0;
       
                    var spain =0;
                    var sri_lanka =0;
                    var turkey = 0;
                    var uk =0;
                    var united_states = 0;
                    var zimbabwe = 0;
                    var totP =  0;

                    var born_outside_uk=0;
                    var bornOutsideBrs = 0;
                    var totalPopBrs =0;
                    var outside = 0;
                    var newCalcUk =0;
                    var totalBornOutsideBristol = 0;

                    // These indicators were missing from display/calculation
var africa_not_otherwise_specified     = 0;
var central_america = 0;
var central_asia = 0;
var northAfrica = 0;
var other_australasia  = 0;
var other_caribbean  = 0;
var other_central_and_western_africa  = 0;
var other_eastern_asia   = 0;
var other_eu_accession_countries  = 0;
var other_eu_member_countries  = 0;
var other_europe   = 0;
var other_eu_march2001=0;
var other_middle_east   = 0;
var other_north_america  = 0;
var other_oceania   = 0;
var other_south_and_eastern_africa   = 0;
var other_south_east_asia   = 0;
var other_southern_asia   = 0;
var south_america = 0;
                    
            $.each(data, function(i, obj) {

                    var wardN = data[i].fields["2016_ward_name"];
                  
                    totP =  data[i].fields.total_population;
                    born_outside_uk = data[i].fields.born_outside_uk;
                    uk = data[i].fields.born_in_uk;

                    newCalcUk = totP - uk;

                    bornOutsideBrs = bornOutsideBrs + newCalcUk;

                    totalPopBrs = totalPopBrs + totP;

                   
                    if (wardN == ward){

                        //  These indicators are unused, along with those ending Zero (0)
                        // 
                        //  "born_in_other_countries": 1184,
                        //   "born_in_republic_of_ireland": 95,
                        //   "born_in_uk": 9197,
                        // -- Ireland also repeats as Republic of Ireland

                        africa_not_otherwise_specified= data[i].fields.born_in_africa_not_otherwise_specified;
                        antarctica = data[i].fields.born_in_antarctica;
                        australia = data[i].fields.born_in_australia;
                        bangladesh = data[i].fields.born_in_bangladesh;
                        central_america=  data[i].fields.born_in_central_america;
                        central_asia = data[i].fields.born_in_central_asia;
                        china = data[i].fields.born_in_china;
                        france = data[i].fields.born_in_france;
                        germany = data[i].fields.born_in_germany;
                        ghana = data[i].fields.born_in_ghana;
                        hong_kong = data[i].fields.born_in_hong_kong;
                        india = data[i].fields.born_in_india;
                        iran = data[i].fields.born_in_iran;
                        ireland = data[i].fields.born_in_ireland;
                        italy = data[i].fields.born_in_italy;
                        jamaica = data[i].fields.born_in_jamaica;
                        kenya = data[i].fields.born_in_kenya;
                        lithuania = data[i].fields.born_in_lithuania;
                        nigeria = data[i].fields.born_in_nigeria;
                        northAfrica = data[i].fields.born_in_north_africa;
                        // Plain OTHER category not used in excel tool.
                        other_australasia = data[i].fields.born_in_other_australasia;
                        other_caribbean = data[i].fields.born_in_other_caribbean;
                        other_central_and_western_africa = data[i].fields.born_in_other_central_and_western_africa;
                        other_eastern_asia = data[i].fields.born_in_other_eastern_asia;
                        other_eu_accession_countries = data[i].fields.born_in_other_eu_accession_countries;
                        other_eu_member_countries = data[i].fields.born_in_other_eu_member_countries;
                        other_europe = data[i].fields.born_in_other_europe;
                        other_eu_march2001 = data[i].fields.born_in_other_eu_member_countries_in_march_2001
                        other_middle_east = data[i].fields.born_in_other_middle_east;
                        other_north_america = data[i].fields.born_in_other_north_america;
                        other_oceania = data[i].fields.born_in_other_oceania;
                        other_south_and_eastern_africa = data[i].fields.born_in_other_south_and_eastern_africa;
                        other_south_east_asia = data[i].fields.born_in_other_south_east_asia;
                        other_southern_asia = data[i].fields.born_in_other_southern_asia;
                        pakistan = data[i].fields.born_in_pakistan;
                        philippines = data[i].fields.born_in_philippines;
                        poland = data[i].fields.born_in_poland;
                        portugal = data[i].fields.born_in_portugal;
                        romania = data[i].fields.born_in_romania;
                        somalia = data[i].fields.born_in_somalia;
                        south_africa = data[i].fields.born_in_south_africa;
                        south_america = data[i].fields.born_in_south_america;
                        spain = data[i].fields.born_in_spain;
                        sri_lanka = data[i].fields.born_in_sri_lanka;
                        turkey = data[i].fields.born_in_turkey;
                        united_states = data[i].fields.born_in_united_states;
                        zimbabwe = data[i].fields.born_in_zimbabwe;
                        outside = born_outside_uk;
                       
                    }



            });

            countryList.push( {
                country:"Africa not specified",
                 people: africa_not_otherwise_specified
                });
            
                    var countryObj = new Object();
                    countryObj.country = "Antarctica";
                    countryObj.people = antarctica;
                    countryList.push(countryObj);
                    
                    var countryObj2 = new Object();
                    countryObj2.country = "Australia";
                    countryObj2.people = australia;
                    countryList.push(countryObj2);
                    
                    var countryObj3 = new Object();
                    countryObj3.country = "Bangladesh";
                    countryObj3.people = bangladesh;
                    countryList.push(countryObj3);
                    
                    countryList.push( {
                        country:"Central America",
                         people: central_america
                        });
                    countryList.push( {
                        country:"Central Asia",
                            people: central_asia
                        });
                    var countryObj4 = new Object();
                    countryObj4.country = "China";
                    countryObj4.people = china;
                    countryList.push(countryObj4);

                    var countryObj5 = new Object();
                    countryObj5.country = "France";
                    countryObj5.people = france;
                    countryList.push(countryObj5);

                    var countryObj6 = new Object();
                    countryObj6.country = "Germany";
                    countryObj6.people = germany;
                    countryList.push(countryObj6);

                    var countryObj7 = new Object();
                    countryObj7.country = "Ghana";
                    countryObj7.people = ghana;
                    countryList.push(countryObj7);

                    var countryObj8 = new Object();
                    countryObj8.country = "Hong Kong";
                    countryObj8.people = hong_kong;
                    countryList.push(countryObj8);
                    
                    var countryObj9 = new Object();
                    countryObj9.country = "India";
                    countryObj9.people = india;
                    countryList.push(countryObj9);
                

                    var countryObj10 = new Object();
                    countryObj10.country = "Iran";
                    countryObj10.people = iran;
                    countryList.push(countryObj10);
                 
                    var countryObj11 = new Object();
                    countryObj11.country = "Ireland";
                    countryObj11.people = ireland;
                    countryList.push(countryObj11);

                    var countryObj12 = new Object();
                    countryObj12.country = "Italy";
                    countryObj12.people = italy;
                    countryList.push(countryObj12);
                 
                
                    var countryObj13 = new Object();
                    countryObj13.country = "Jamaica";
                    countryObj13.people = jamaica;
                    countryList.push(countryObj13);


                    var countryObj14 = new Object();
                    countryObj14.country = "Kenya";
                    countryObj14.people = kenya;
                    countryList.push(countryObj14);
                    
                    var countryObj15 = new Object();
                    countryObj15.country = "Lithuania";
                    countryObj15.people = lithuania;
                    countryList.push(countryObj15);
                    
                    var countryObj16 = new Object();
                    countryObj16.country = "Nigeria";
                    countryObj16.people = nigeria;
                    countryList.push(countryObj16);

                    countryList.push( {
                        country:"North Africa",
                         people: northAfrica
                        });

                    countryList.push( {  country:"Other Australasia" , people: other_australasia });
                    countryList.push( {  country:"Other Caribbean" , people: other_caribbean });
                    countryList.push( {  country:"Other Central and Western Africa" , people: other_central_and_western_africa });
                    countryList.push( {  country:"Other Eastern Asia" , people: other_eastern_asia });
                    countryList.push( {  country:"Other EU accession countries" , people: other_eu_accession_countries });
                    // countryList.push( {  country:"Other EU countries" , people: other_eu_member_countries });
                    countryList.push( {  country:"Other EU countries in March 2001" , people: other_eu_march2001 });
                    countryList.push( {  country:"Other Europe" , people: other_europe });
                    countryList.push( {  country:"Other Middle East" , people: other_middle_east });
                    countryList.push( {  country:"Other North America" , people: other_north_america });
                    countryList.push( {  country:"Other Oceania" , people: other_oceania });
                    countryList.push( {  country:"Other South and Eastern Africa" , people: other_south_and_eastern_africa });
                    countryList.push( {  country:"Other South-East Asia" , people: other_south_east_asia });
                    countryList.push( {  country:"Other Southern Asia" , people: other_southern_asia });
                    
                    countryList.push( {  country:"South America" , people: south_america });
                    
                        
                    var countryObj17 = new Object();
                    countryObj17.country = "Pakistan";
                    countryObj17.people = pakistan;
                    countryList.push(countryObj17);
                    
                    var countryObj18 = new Object();
                    countryObj18.country = "Philippines";
                    countryObj18.people = philippines;
                    countryList.push(countryObj18);    
                      
                 
                    var countryObj20 = new Object();
                    countryObj20.country = "Poland";
                    countryObj20.people = poland;
                    countryList.push(countryObj20);  

                    var countryObj21 = new Object();
                    countryObj21.country = "Portugal";
                    countryObj21.people = portugal;
                    countryList.push(countryObj21); 

                    // var countryObj22 = new Object();
                    // countryObj22.country = "Ireland Republic";
                    // countryObj22.people = ireland_republic;
                    // countryList.push(countryObj22); 

                    // var countryObj23 = new Object();
                    // countryObj23.country = "Romania";
                    // countryObj23.people = romania;
                    // countryList.push(countryObj23); 

                    var countryObj24 = new Object();
                    countryObj24.country = "Romania";
                    countryObj24.people = romania;
                    countryList.push(countryObj24); 

                    var countryObj25 = new Object();
                    countryObj25.country = "Somalia";
                    countryObj25.people = somalia;
                    countryList.push(countryObj25); 

                    var countryObj26 = new Object();
                    countryObj26.country = "South Africa";
                    countryObj26.people = south_africa;
                    countryList.push(countryObj26); 

                    
                    var countryObj27 = new Object();
                    countryObj27.country = "Spain";
                    countryObj27.people = spain;
                    countryList.push(countryObj27); 

                       
                    var countryObj28 = new Object();
                    countryObj28.country = "Sri Lanka";
                    countryObj28.people = sri_lanka;
                    countryList.push(countryObj28); 


                    var countryObj29 = new Object();
                    countryObj29.country = "Turkey";
                    countryObj29.people = turkey;
                    countryList.push(countryObj29); 
                    
                    var countryObj30 = new Object();
                    countryObj30.country = "United States";
                    countryObj30.people = united_states;
                    countryList.push(countryObj30); 
                    
                    var countryObj31 = new Object();
                    countryObj31.country = "Zimbabwe";
                    countryObj31.people = zimbabwe;
                    countryList.push(countryObj31); 


      
                    let sortedInput = countryList.slice(0).sort((a,b) => b.people - a.people);

                

                    tempTopThree.push(sortedInput[0],sortedInput[1],sortedInput[2]);

                    totalBornOutsideBristol = oneDcm(bornOutsideBrs / totalPopBrs * 100); 
                

                    $(".countryWardH").append("<p>"+ ward +" "+ outside +"%</p>");
                    
                    $(".countryBrsH").append("<p>Bristol "+ totalBornOutsideBristol+"%</p>");

                    sampleBarv2(".countryBar",tempTopThree);

        });
    };//end of func

    function main_language (fileName,sign){
 

        $.getJSON( "../../Json/"+fileName, function( data ) {

            var testing = changeName(ward,sign);
              
            ward = testing;
           

            var barJson =[];
            var mainEngNr =0;  
            var notMainengNr =0;  
            var cannotSpeakWellNr =0;  
            var cannotSpeakWellPrc =0;  
            var mainEngPrc = 0;  
            var notMainEngPrc = 0;      
            
            var wardMainEng = 0;
            var wardNotMainEng = 0;

            var brsMainEng = 0;
            var brsdNotMainEng = 0;

            var allPeople =0;
            var brsAll =0;
            var wardTot =0;
            $.each(data, function(i, obj) {

                var wardN = data[i].fields["2016_ward_name"];
                
                               
                mainEngNr = data[i].fields.main_language_is_english;
                notMainengNr = data[i].fields.main_language_is_not_english;
                cannotSpeakWellNr = data[i].fields.people_who_cannot_speak_english_or_cannot_speak_it_very_well;
                cannotSpeakWellPrc = data[i].fields.people_who_cannot_speak_english_or_cannot_speak_it_very_well0;
                mainEngPrc = data[i].fields.whose_main_language_is_english;
                notMainEngPrc = data[i].fields.whose_main_language_is_not_english;           
                allPeople = data[i].fields.all_people_aged_3_and_over;

                brsMainEng = brsMainEng + mainEngNr;
                brsdNotMainEng = brsdNotMainEng + notMainengNr;
                brsAll = brsAll + allPeople;

                if (wardN == ward){
                    
                    wardMainEng = mainEngPrc;
                    wardNotMainEng = notMainEngPrc;
                    wardTot = allPeople;

                }

                


            });


            var brsPrcMain = oneDcm (brsMainEng / brsAll * 100);
            var brsPrcNotMain = oneDcm(brsdNotMainEng / brsAll * 100);
          

            var feed = new Object();
            feed.ward = ward;
            feed.main = wardMainEng;
            feed.notMain = wardNotMainEng;
            feed.space = 100;
            barJson.push(feed);   

            var feed2= new Object();
            feed2.ward = "Bristol";
            feed2.main = brsPrcMain;
            feed2.notMain = brsPrcNotMain;
            feed2.space =  100;
            barJson.push(feed2);


            stackedv2(".lanugageBar",barJson);
         





        });    

    };/*end of func*/

    main_language("/population/main-language-and-proficiency-in-english-2011-census-by-2016-ward.json","&");
    country_of_birth("/population/country-of-birth-by-ward-copy.json","&");
    
});    