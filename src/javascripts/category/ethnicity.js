$(function(){

    function ethnicity(fileName,sign){
 

        $.getJSON( "../../Json/"+fileName, function( data ) {

            var testing = changeName(ward,sign);
              
            ward = testing;
            
            var tempBrs =[];
            var tempWard =[];
            var ethnicH =[];
            var barJson= [];

            var countWard =0;
            
            var eachWardNr =[];
            var eachWardNr2 =[];
            var eachWardNr3 =[];
            var eachWardNr4 =[];
            var eachWardNr5 =[];
            var eachWardNr6 =[];
            var eachWardNr7 =[];
            var eachWardNr8 =[];
            var eachWardNr9 =[];
            var eachWardNr10 =[];
            var eachWardNr11 =[];
            var eachWardNr12 =[];
            var eachWardNr13 =[];
            var eachWardNr14 =[];
            var eachWardNr15 =[];
            var eachWardNr16 =[];
            

            var mean =0;
            var mean2 =0;
            var mean3 =0;
            var mean4 =0;
            var mean5 =0;
            var mean6 =0;
            var mean7 =0;
            var mean8 =0;
            var mean9 =0;
            var mean10 =0;
            var mean11 =0;
            var mean12 =0;
            var mean13 =0;
            var mean14 =0;
            var mean15 =0;
            var mean16 =0;

            $.each(data, function(i, obj) {
                

                var arab = data[i].fields.arab;
                var bangladeshi = data[i].fields.bangladeshi;
                var black_african = data[i].fields.black_african;
                var black_and_minority_ethnic_group =  to15sf(data[i].fields.black_and_minority_ethnic_group);
                var black_caribbean = data[i].fields.black_caribbean;
                var chinese = data[i].fields.chinese;
                var indian = data[i].fields.indian;
                var mixed = data[i].fields.mixed;
                var wardN = data[i].fields.name;
                var other_asian = data[i].fields.other_asian;
                var other_black = data[i].fields.other_black;
                var other_ethnic_group = data[i].fields.other_ethnic_group;
                var other_white = data[i].fields.other_white;
                var pakistani = data[i].fields.pakistani;
                var total_black_and_minority_ethnic_group = data[i].fields.total_black_and_minority_ethnic_group;
                var total_population = data[i].fields.total_population;
                var total_white = data[i].fields.total_white;
                var total_white_non_british= data[i].fields.total_white_non_british;
                var white_british = data[i].fields.white_british;
                var white_gypsy_or_irish_traveller = data[i].fields.white_gypsy_or_irish_traveller;
                var white_irish = data[i].fields.white_irish;

                if (wardN !== "England & Wales"){

                    var feed = new Object();
                    feed.ward = wardN;
                    feed.info = black_and_minority_ethnic_group;
                    barJson.push(feed);


                }

           



                if (wardN == ward){


                    var white_britishAvg = to15sf(white_british / total_population * 100);
                    var white_irishAvg = to15sf(white_irish / total_population * 100);
                    var white_gypsy_or_irish_travellerAvg = to15sf(white_gypsy_or_irish_traveller / total_population * 100);
                    var other_whiteAvg = to15sf(other_white / total_population * 100);
                    var mixedAvg = to15sf(mixed / total_population * 100);
                    var indianAvg = to15sf(indian / total_population * 100);
                    var pakistaniAvg = to15sf(pakistani / total_population * 100);
                    var bangladeshiAvg = to15sf(bangladeshi / total_population * 100);
                    var chineseAvg = to15sf(chinese / total_population * 100);
                    var other_asianAvg = to15sf(other_asian / total_population * 100);
                    var black_africanAvg = to15sf(black_african / total_population * 100);
                    var black_caribbeanAvg = to15sf(black_caribbean / total_population * 100);
                    var other_blackAvg = to15sf(other_black / total_population * 100);
                    var arabAvg = to15sf(arab / total_population * 100);
                    var other_ethnic_groupAvg = to15sf(other_ethnic_group / total_population * 100);

                    tempWard.push(white_britishAvg,white_irishAvg,white_gypsy_or_irish_travellerAvg,other_whiteAvg,
                    mixedAvg,indianAvg,pakistaniAvg,bangladeshiAvg,chineseAvg,other_asianAvg,black_africanAvg,black_caribbeanAvg,
                    other_blackAvg,arabAvg,other_ethnic_groupAvg,black_and_minority_ethnic_group);


                }

                
                if (wardN == "Bristol"){


                    var white_britishAvg = to15sf(white_british / total_population * 100);
                    var white_irishAvg = to15sf(white_irish / total_population * 100);
                    var white_gypsy_or_irish_travellerAvg = to15sf(white_gypsy_or_irish_traveller / total_population * 100);
                    var other_whiteAvg = to15sf(other_white / total_population * 100);
                    var mixedAvg = to15sf(mixed / total_population * 100);
                    var indianAvg = to15sf(indian / total_population * 100);
                    var pakistaniAvg = to15sf(pakistani / total_population * 100);
                    var bangladeshiAvg = to15sf(bangladeshi / total_population * 100);
                    var chineseAvg = to15sf(chinese / total_population * 100);
                    var other_asianAvg = to15sf(other_asian / total_population * 100);
                    var black_africanAvg = to15sf(black_african / total_population * 100);
                    var black_caribbeanAvg = to15sf(black_caribbean / total_population * 100);
                    var other_blackAvg = to15sf(other_black / total_population * 100);
                    var arabAvg = to15sf(arab / total_population * 100);
                    var other_ethnic_groupAvg = to15sf(other_ethnic_group / total_population * 100);

                    tempBrs.push(white_britishAvg,white_irishAvg,white_gypsy_or_irish_travellerAvg,other_whiteAvg,
                    mixedAvg,indianAvg,pakistaniAvg,bangladeshiAvg,chineseAvg,other_asianAvg,black_africanAvg,black_caribbeanAvg,
                    other_blackAvg,arabAvg,other_ethnic_groupAvg,black_and_minority_ethnic_group);

                
                }

                if((wardN !== "Bristol" ) && (wardN !== "England & Wales")){
    
                    mean = mean + to15sf(white_british / total_population * 100);
                    mean2 = mean2 + to15sf(white_irish / total_population * 100);
                    mean3 = mean3 + to15sf(white_gypsy_or_irish_traveller / total_population * 100);
                    mean4 = mean4 + to15sf(other_white / total_population * 100);
                    mean5 = mean5 + to15sf(mixed / total_population * 100);
                    mean6 = mean6 + to15sf(indian / total_population * 100);
                    mean7 = mean7 + to15sf(pakistani / total_population * 100);
                    mean8 = mean8 + to15sf(bangladeshi / total_population * 100);
                    mean9 = mean9 + to15sf(chinese / total_population * 100);
                    mean10 = mean10 + to15sf(other_asian / total_population * 100);
                    mean11 = mean11 + to15sf(black_african / total_population * 100);
                    mean12 = mean12 + to15sf(black_caribbean / total_population * 100);
                    mean13 = mean13 + to15sf(other_black / total_population * 100);
                    mean14 = mean14 + to15sf(arab / total_population * 100);
                    mean15 = mean15 + to15sf(other_ethnic_group / total_population * 100);
                    mean16 = mean16 + black_and_minority_ethnic_group;

                    countWard++;

                    eachWardNr.push(to15sf(white_british / total_population * 100));
                    eachWardNr2.push(to15sf(white_irish / total_population * 100));
                    eachWardNr3.push(to15sf(white_gypsy_or_irish_traveller / total_population * 100));
                    eachWardNr4.push(to15sf(other_white / total_population * 100));
                    eachWardNr5.push(to15sf(mixed / total_population * 100));
                    eachWardNr6.push(to15sf(indian / total_population * 100));
                    eachWardNr7.push(to15sf(pakistani / total_population * 100));
                    eachWardNr8.push(to15sf(bangladeshi / total_population * 100));
                    eachWardNr9.push(to15sf(chinese / total_population * 100));
                    eachWardNr10.push(to15sf(other_asian / total_population * 100));
                    eachWardNr11.push(to15sf(black_african / total_population * 100));
                    eachWardNr12.push(to15sf(black_caribbean / total_population * 100));
                    eachWardNr13.push(to15sf(other_black / total_population * 100));
                    eachWardNr14.push(to15sf(arab / total_population * 100));
                    eachWardNr15.push(to15sf(other_ethnic_group / total_population * 100));
                    eachWardNr16.push(black_and_minority_ethnic_group);
            
                  }


            });
            
            var standard = standardDeviation(mean,countWard,eachWardNr);
            var standard2 = standardDeviation(mean2,countWard,eachWardNr2);
            var standard3 = standardDeviation(mean3,countWard,eachWardNr3);
            var standard4 = standardDeviation(mean4,countWard,eachWardNr4);
            var standard5 = standardDeviation(mean5,countWard,eachWardNr5);
            var standard6 = standardDeviation(mean6,countWard,eachWardNr6);
            var standard7 = standardDeviation(mean7,countWard,eachWardNr7);
            var standard8 = standardDeviation(mean8,countWard,eachWardNr8);
            var standard9 = standardDeviation(mean9,countWard,eachWardNr9);
            var standard10 = standardDeviation(mean10,countWard,eachWardNr10);
            var standard11 = standardDeviation(mean11,countWard,eachWardNr11);
            var standard12 = standardDeviation(mean12,countWard,eachWardNr12);
            var standard13 = standardDeviation(mean13,countWard,eachWardNr13);
            var standard14 = standardDeviation(mean14,countWard,eachWardNr14);
            var standard15 = standardDeviation(mean15,countWard,eachWardNr15);
            var standard16 = standardDeviation(mean16,countWard,eachWardNr16);
         

            var clrs = colStd(fileName,tempWard[0],tempBrs[0],standard);
            var clrs2 = colStd(fileName,tempWard[1],tempBrs[1],standard2);
            var clrs3 = colStd(fileName,tempWard[2],tempBrs[2],standard3);
            var clrs4 = colStd(fileName,tempWard[3],tempBrs[3],standard4);
            var clrs5 = colStd(fileName,tempWard[4],tempBrs[4],standard5);
            var clrs6 = colStd(fileName,tempWard[5],tempBrs[5],standard6);
            var clrs7 = colStd(fileName,tempWard[6],tempBrs[6],standard7);
            var clrs8 = colStd(fileName,tempWard[7],tempBrs[7],standard8);
            var clrs9 = colStd(fileName,tempWard[8],tempBrs[8],standard9);
            var clrs10 = colStd(fileName,tempWard[9],tempBrs[9],standard10);
            var clrs11 = colStd(fileName,tempWard[10],tempBrs[10],standard11);
            var clrs12 = colStd(fileName,tempWard[11],tempBrs[11],standard12);
            var clrs13 = colStd(fileName,tempWard[12],tempBrs[12],standard13);
            var clrs14 = colStd(fileName,tempWard[13],tempBrs[13],standard14);
            var clrs15 = colStd(fileName,tempWard[14],tempBrs[14],standard15);
            var clrs16 = colStd(fileName,tempWard[15],tempBrs[15],standard16);

            var clrArray = [clrs,clrs2,clrs3,clrs4,clrs5,clrs6,clrs7,clrs8,clrs9,clrs10,clrs11,clrs12,clrs13,clrs14,clrs15,clrs16];
       



            var ethnicObj = new Object();

            ethnicObj.group = "White British";
            ethnicObj.currWard  = tempWard[0];
            ethnicObj.bristol = tempBrs[0];
            ethnicH.push(ethnicObj);

            var ethnicObj2 = new Object();

            ethnicObj2.group = "White Irish";
            ethnicObj2.currWard  = tempWard[1];
            ethnicObj2.bristol = tempBrs[1];
            ethnicH.push(ethnicObj2);

            var ethnicObj3 = new Object();

            ethnicObj3.group = "White Gypsy or Irish Traveller";
            ethnicObj3.currWard  = tempWard[2];
            ethnicObj3.bristol = tempBrs[2];
            ethnicH.push(ethnicObj3);
            
            
            var ethnicObj4 = new Object();

            ethnicObj4.group = "Other White";
            ethnicObj4.currWard  = tempWard[3];
            ethnicObj4.bristol = tempBrs[3];
            ethnicH.push(ethnicObj4);
            

            var ethnicObj5 = new Object();

            ethnicObj5.group = "Mixed";
            ethnicObj5.currWard  = tempWard[4];
            ethnicObj5.bristol = tempBrs[4];
            ethnicH.push(ethnicObj5);
            


            
            var ethnicObj6 = new Object();

            ethnicObj6.group = "Indian";
            ethnicObj6.currWard  = tempWard[5];
            ethnicObj6.bristol = tempBrs[5];
            ethnicH.push(ethnicObj6);
            


            
            var ethnicObj7 = new Object();

            ethnicObj7.group = "Pakistani";
            ethnicObj7.currWard  = tempWard[6];
            ethnicObj7.bristol = tempBrs[6];
            ethnicH.push(ethnicObj7);
            
              
            var ethnicObj8 = new Object();

            ethnicObj8.group = "Bangladeshi";
            ethnicObj8.currWard  = tempWard[7];
            ethnicObj8.bristol = tempBrs[7];
            ethnicH.push(ethnicObj8);
            
              
            var ethnicObj9 = new Object();

            ethnicObj9.group = "Chinese";
            ethnicObj9.currWard  = tempWard[8];
            ethnicObj9.bristol = tempBrs[8];
            ethnicH.push(ethnicObj9);
            
            var ethnicObj10 = new Object();

            ethnicObj10.group = "Other Asian";
            ethnicObj10.currWard  = tempWard[9];
            ethnicObj10.bristol = tempBrs[9];
            ethnicH.push(ethnicObj10);

            var ethnicObj11 = new Object();

            ethnicObj11.group = "Black African";
            ethnicObj11.currWard  = tempWard[10];
            ethnicObj11.bristol = tempBrs[10];
            ethnicH.push(ethnicObj11);
            
            var ethnicObj12 = new Object();

            ethnicObj12.group = "Black Caribbean";
            ethnicObj12.currWard  = tempWard[11];
            ethnicObj12.bristol = tempBrs[11];
            ethnicH.push(ethnicObj12);

            var ethnicObj13 = new Object();

            ethnicObj13.group = "Other Black";
            ethnicObj13.currWard  = tempWard[12];
            ethnicObj13.bristol = tempBrs[12];
            ethnicH.push(ethnicObj13);

            var ethnicObj14 = new Object();

            ethnicObj14.group = "Arab";
            ethnicObj14.currWard  = tempWard[13];
            ethnicObj14.bristol = tempBrs[13];
            ethnicH.push(ethnicObj14);

            var ethnicObj15 = new Object();

            ethnicObj15.group = "Other ethnic group";
            ethnicObj15.currWard  = tempWard[14];
            ethnicObj15.bristol = tempBrs[14];
            ethnicH.push(ethnicObj15);

            
            var ethnicObj16 = new Object();

            ethnicObj16.group = "Black and Minority Ethnic Group Total";
            ethnicObj16.currWard  = tempWard[15];
            ethnicObj16.bristol = tempBrs[15];
            ethnicH.push(ethnicObj16);

            
            for (var i=0; i < 8;i++){
                $(".ethnicH").find(".ethnicNumb").append("<div class='ethField'><span class='dotz "+clrArray[i]+"'></span><div class='wardPart'><div class='wardProc'><div class='procNr'>"+oneDcm(ethnicH[i].currWard)+"%</div></div></div><div class='brsPart'><div class='brsProc'><div class='procNr'>"+oneDcm(ethnicH[i].bristol)+"%</div></div></div><p class='ethText'>"+ethnicH[i].group+"</p></div>");
            }
            for (var i=8; i < 16;i++){
                $(".ethnicH2").find(".ethnicNumb").append("<div class='ethField'><span class='dotz "+clrArray[i]+"'></span><div class='wardPart'><div class='wardProc'><div class='procNr'>"+oneDcm(ethnicH[i].currWard)+"%</div></div></div><div class='brsPart'><div class='brsProc'><div class='procNr'>"+oneDcm(ethnicH[i].bristol)+"%</div></div></div><p class='ethText'>"+ethnicH[i].group+"</p></div>");
            } 

            $(".ethnicH").find(".ethnicNumb").prepend("<div class='ethField'><div class='wardPart'><div class='first'>"+ ward +"</div></div><div class='brsPart'><div class='first'>Bristol</div></div></div>");
            
            $(".ethnicH2").find(".ethnicNumb").prepend("<div class='ethField'><div class='wardPart'><div class='first'>"+ ward +"</div></div><div class='brsPart'><div class='first'>Bristol</div></div></div>");


            sampleBar(".ethBar",barJson,{addedToMouseOverUnits: '%'}); 
        });
    };//end of func

    ethnicity("/population/ethnicity.json","&");
    
});    