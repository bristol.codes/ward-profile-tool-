
function donut2(dataz,div,classCol){

  $(".donutHolder "+div+" svg path").addClass(classCol);
  
  
  var newW = $(".brC").width();
  
  var setToParentWidth = $(div).width();
  
  var donutChart;
  (function() {
    var w = setToParentWidth,
     h = 100,
     r = 50,
     innerRadius = 30,
     transitionsDuration = 1000,
     transitionsDelay = 250,
     percentageTextSize = '1.0rem';
     
  
    // This is the scale to avoid using gradiant for the angles.
    var rScale = d3.scale.linear().domain([0, 100]).range([0, 2 * Math.PI]);
  
    // Here we use the helper function of d3 to draw arcs easier
    var arc = d3.svg.arc()
      .outerRadius(r + 0.8)
      .innerRadius(innerRadius);
  
    // Another helper function of d3 to bind the data to the arcs
    var pie = d3.layout.pie()
      .value(function(d) {
        return d.value;
      });
  
    donutChart = {
      /**
       * A d3 function that draws a donut chart.
       */
      draw: function(container, data) {
  
        var svg = d3.select(container)
          .append('svg')
          .attr('width', '100%')
          //.attr('viewBox', '0 0 ' + 300 + ' ' + 250);
  
        createBigCircle(svg);
        var vis = createChartContainer(svg, data);
        drawChartArcs(vis, data);
        createSmallCircle(vis);
        drawPercentageText(vis, data);
        //drawInformativeText(vis, data);
  
      }
    };
  
    // Here we create the big circle (the outer one)
    function createBigCircle(svg) {
      svg.append('circle')
      .attr("align","center")
        .attr('r', r)
        //.attr('transform', 'translate(' + 150 + ',' + r + ')')
        .attr('transform', 'translate(' + w/2 +  ',' + h/2 +')')
        .attr('class', 'pie-graph-big-circle');
  
        
    }
  
    // Here we give dimensions to the svg and create a g container
    function createChartContainer(svg, data) {
      return svg
        .data([data])
        .attr('width', w)
        .attr('height', h)
        .append('g')
        //.attr('transform', 'translate(' + 150 + ',' + r + ')');
        .attr('transform', 'translate(' + w/2 +  ',' + h/2 +')');
    }
  
    // We draw the arc in here, give it an smooth transition and the correct color depending on the data.
    function drawChartArcs(visualization, data) {
      var arcs = visualization.selectAll('g')
        .data(pie)
        .enter()
        .append('g');
        
  
      arcs.append('path')
      
      .attr("class", classCol)
     
        .each(function(d) {
          d.endAngle = 0;
        })
        .attr('d', arc)
        .transition()
        .duration(transitionsDuration)
        .delay(transitionsDelay)
        .ease('elastic')
        .call(arcTween, this);
    }
  
    // This help us achieve the arcs transitions.
    function arcTween(transition, newAngle) {
  
      transition.attrTween("d", function(d) {
  
        var interpolate = d3.interpolate(0, 360 * (d.value / 100) * Math.PI / 180);
  
        return function(t) {
  
          d.endAngle = interpolate(t);
  
          return arc(d);
        };
      });
    }
  
    // This is the small circle, the one with the text in the middle.
    function createSmallCircle(visualization) {
      visualization.append('circle')
        .attr('cx', 0)
        .attr('cy', 0)
        .attr('r', innerRadius)
        .attr('class', 'pie-graph-small-circle');
    }
  
    // This is the percentage text, it appears with the same transition as the path/arcs
    function drawPercentageText(visualization, data) {
      visualization.append('text')
        .data(data)
        .attr("font-family", "Arial")
        .attr("font-size", "0px")
          .attr("fill", colourScheme.reusedColours.black)
        .attr('text-anchor', 'middle')
        .attr('y', '5px')
        .text(function(d) {
          return d.value + "%";
        })
        .transition()
        .attr('font-size', percentageTextSize)
        .duration(transitionsDuration)
        .delay(transitionsDelay)
        .ease('elastic');
  
        
    }
    
  })();
  
    donutChart.draw(div, [{
        value: dataz, color: colourScheme.reusedColours.green, middleText: 'TARGET'}] )
  
  }