
      function makeLineChart(dataset, xName, yObjs, axisLables,index) {
       
      
        const xScaleOrdinal=true;
        var chartObj = {};
        function testCol(name){

            if (name == "Ward"){
                return colourScheme.reusedColours.darkBlue;
            } else {
                return colourScheme.reusedColours.darkOrange;
            }
        }

        chartObj.xAxisLable = axisLables.xAxis;
        chartObj.yAxisLable = axisLables.yAxis;
      
        chartObj.data = dataset;
        chartObj.margin = {top: 15, right: 60, bottom: 30, left: 50};
        chartObj.width = 750 - chartObj.margin.left - chartObj.margin.right;
        chartObj.height = 472 - chartObj.margin.top - chartObj.margin.bottom;
    
    // So we can pass the x and y as strings when creating the function
        chartObj.xFunct = function(d){return d[xName]};
        
    
    // For each yObjs argument, create a yFunction
        function getYFn(column) {
            return function (d) {
                return d[column];
            };
        }
    
    // Object instead of array
        chartObj.yFuncts = [];
        for (var y  in yObjs) {
            yObjs[y].name = y;
            yObjs[y].yFunct = getYFn(yObjs[y].column); //Need this  list for the ymax function
            chartObj.yFuncts.push(yObjs[y].yFunct);
        }
    
    //Formatter functions for the axes
    
        chartObj.formatAsYear = d3.format("%y");
        chartObj.formatAsNumber = d3.format(".0f");
        chartObj.formatAsDecimal = d3.format(".2f");
        chartObj.formatAsCurrency = d3.format("$.2f");
        chartObj.formatAsFloat = function (d) {
            if (d % 1 !== 0) {
                return d3.format(".2f")(d);
            } else {
                return d3.format(".0f")(d);
            }
            
        };
    // 
        chartObj.xFormatter = chartObj.formatAsNumber;
        chartObj.yFormatter = chartObj.formatAsFloat;
    
        chartObj.bisectYear = d3.bisector(chartObj.xFunct).left; //< Can be overridden in definition
    

          //Create scale functions
          if (!xScaleOrdinal) {
              chartObj.xScale = d3.scale
                  .linear()
                  .range([0, chartObj.width])
                  .domain(
                      d3.extent(chartObj.data, chartObj.xFunct)
                  );
          }
          else {
              chartObj.xScale = d3.scale
                  .ordinal()
                  .domain(chartObj.data.map(x => x.year))
                  .rangeBands([0, chartObj.width]);
          } 
    
    // Get the max of every yFunct
        chartObj.max = function (fn) {
            return d3.max(chartObj.data, fn);
        };
        chartObj.minZeroOrBelow=function(fn){
            var minZ = d3.min(chartObj.data,fn);
            return minZ>0 ? 0 : minZ;
        }
        chartObj.yScale = d3.scale.linear()
            .range([chartObj.height, 0])
            .domain([ d3.min(chartObj.yFuncts.map(chartObj.minZeroOrBelow)), d3.max([0, d3.max(chartObj.yFuncts.map(chartObj.max))]) ]);

        chartObj.xAxis = d3.svg.axis().scale(chartObj.xScale).orient("bottom").ticks(index).tickFormat(function(d, i) {
             if (i < index){
              return chartObj.data[i].label;
             }
            })
        
        chartObj.yAxis = d3.svg.axis().scale(chartObj.yScale).orient("left").tickFormat(chartObj.yFormatter); //< Can be overridden in definition
    
    
    // Build line building functions
        function getYScaleFn(yObj) {
            return function (d) {
                return chartObj.yScale(yObjs[yObj].yFunct(d));
            };
        }

        for (let newYObj in yObjs) {
            let forY = newYObj; // todo: resolve this closure mess
            // add .interpolate("cardinal") after line() for smooth line
            yObjs[newYObj].line = 
                d3.svg.line().defined(d =>{ let newY = forY; return !isNaN(getYScaleFn(newY)(d)); })
                    .x(function (d) {return chartObj.xScale(chartObj.xFunct(d)) + getHalfBandWidthX()+chartObj.margin.left;})
                    .y(getYScaleFn(forY));
        }
        
        chartObj.bind = function (selector) {
            chartObj.mainDiv = d3.select(selector);
            // Add all the divs to make it centered and responsive
            chartObj.mainDiv
                .append("div").attr("class", "inner-wrapper svgs3 padBot")
                    .append("div").attr("class", "outer-box")
                        .append("div").attr("class", "inner-box");
            var chartSelector = selector + " .inner-box";
            chartObj.chartDiv = d3.select(chartSelector);
            return chartObj;
        };
    
    // Render the chart
        chartObj.render = function () {
            //Create SVG element
            chartObj.svg = chartObj.chartDiv.append("svg")
                .attr("class", "chart-area")
                .attr("viewBox","0 0 900 300")
                .attr("width","100%")
                .attr("height", chartObj.height+60 + (chartObj.margin.top + chartObj.margin.bottom))
                    .append("g").attr("transform", "translate(" + chartObj.margin.left + ",-60)");
    
            // Draw Lines
            for (var y  in yObjs) {
                yObjs[y].path = chartObj.svg.append("path")
                    .datum(chartObj.data)
                    .attr("class", "line")
                    .attr("d", yObjs[y].line)
                    .style("stroke", testCol(y))
                    .attr("data-series", y)
                    .on("mouseover", function () {
                        focus.style("display", null);
                    })
                    .on("mouseout", function () {
                        focus.transition().delay(700).style("display", "none");
                    })
                    .on("mousemove", mousemove);
            }
            
    
            // Draw Axis & label
            chartObj.svg.append("g")
                .attr("class", "x axis")
                .attr("transform", "translate(0," + chartObj.yScale(0)+ ")")
                .call(chartObj.xAxis)
                .append("text")
                    .attr("class", "label")
                    .attr("x", chartObj.width / 2)
                    .attr("y", 30)
                    .style("text-anchor", "middle")
                    .text(chartObj.xAxisLable);
    
            chartObj.svg.append("g")
            .attr("class", "y axis")
            .call(chartObj.yAxis)
            .append("text")
                .attr("class", "label")
                .attr("transform", "rotate(-90)")
                .attr("y", -50)
                .attr("x", -chartObj.height / 2)
                .attr("dy", ".71em")
                .style("text-anchor", "middle")
                .text(chartObj.yAxisLable);
    
            //Draw tooltips
            var focus = chartObj.svg.append("g").attr("class", "focus").style("display", "none");
    
            for (var y  in yObjs) {
                yObjs[y].tooltip = focus.append("g");
                yObjs[y].tooltip.append("circle").attr("r", 5).attr("class", yObjs[y].column);
                yObjs[y].tooltip.append("rect").attr("x", 8).attr("y","-1em").attr("width",44).attr("height",'2em');
                yObjs[y].tooltip.append("text").attr("x", 9).attr("dy", "0.35em");
                
            }

            // Focus line
            focus.append("line").attr("class", "focus line").attr("y1", 0).attr("y2", chartObj.height);
    
            
            //Draw legend
            var legend = chartObj.mainDiv.append('div').attr("class", "legend svgs3");
            for (var y  in yObjs) {
                var series = legend.append('div');
                series.append('div').attr("class", "series-marker").style("background-color", testCol(y));
                series.append('p').text(y);
                yObjs[y].legend = series;
            }
            
       
            // Overlay to capture hover
            chartObj.svg.append("rect").attr("class", "overlay").attr("width", chartObj.width).attr("height", chartObj.height).on("mouseover", function () {
                focus.style("display", null);
            }).on("mouseout", function () {
                focus.style("display", "none");
            }).on("mousemove", mousemove);
    
            return chartObj;

            function scaleBandInvert(scale) {
                var domain = scale.domain();
                var paddingOuter = 0;// scale(domain[0]);
                // var eachBand = scale.step();
                var eachBand = scale.rangeBand();
                return function (value) {
                  var index = Math.floor(((value - paddingOuter) / eachBand));
                  return domain[Math.max(0,Math.min(index, domain.length-1))];
                }
              }
            
            function mousemove() {
                var x0 = 0,i=0,d0=0,d1=0;
                if(!xScaleOrdinal){
                    x0= chartObj.xScale.invert(d3.mouse(this)[0]), 
                    i = chartObj.bisectYear(dataset, x0, 1),
                    d0 = chartObj.data[i - 1],
                    d1 = chartObj.data[i];
                }else{
                    x0= scaleBandInvert( chartObj.xScale)(d3.mouse(this)[0]), 
                    i = chartObj.bisectYear(dataset, x0),
                    d0 = chartObj.data[i - 1],
                    d1 = chartObj.data[i];
                }
                var d=d0 || d1;
                try {
                    if(!xScaleOrdinal)  d = x0 - chartObj.xFunct(d0) > chartObj.xFunct(d1) - x0 ? d1 : d0;
                    else  d = d3.mouse(this)[0] - chartObj.xScale( chartObj.xFunct(d0) ) > chartObj.xScale(chartObj.xFunct(d1)) - d3.mouse(this)[0] ? d1 : d0;
                } catch (e) {
                     //return;
                }
                var minY = chartObj.height;
                
                var safeYs=[];
                var ys = [];

                for (var y  in yObjs) {
                    ys.push(chartObj.yScale( yObjs[y].yFunct(d) ));
                }
                
                ys=Array.prototype.sort.call(ys,(a,b)=>a-b);
                var topY=d3.min(ys);
                var bottomY=d3.max(ys);
                var maxY = bottomY;

                // Space is 0 at top
                
                if((bottomY-topY)/(ys.length-1) < 15){
                    safeYs=$.map(ys,(x,i)=>{return new Object({o:x,n: ys[0]+(i*15) });} );
                }else{
                    safeYs=$.map(ys,(x)=>new Object({o:x,n:x}));
                }

                for (var y  in yObjs) {
                    yObjs[y].tooltip
                        .attr(
                            "transform", 
                            "translate(" + 
                               ( chartObj.xScale(chartObj.xFunct(d)) +getHalfBandWidthX()+chartObj.margin.left)+ 
                                "," + 
                                $.grep(
                                    safeYs,
                                    x=>x.o== chartObj.yScale(yObjs[y].yFunct(d)) 
                                )[0].n 
                            + ")"
                            );
                    yObjs[y].tooltip
                        .select("text")
                        .attr("class","textPoints")
                        .text(chartObj.yFormatter(yObjs[y].yFunct(d)));
                    
                    yObjs[y].tooltip
                        .select("rect")
                        .attr("width",((""+chartObj.yFormatter(yObjs[y].yFunct(d))).length-1) * 11);

                    minY = Math.min(minY, chartObj.yScale(yObjs[y].yFunct(d)));
                    maxY = Math.max(maxY,chartObj.yScale(yObjs[y].yFunct(d)));
                }

                /*
                Fixed focus line/trend highlight line to support all 3 situations:
                    negative->negative [ lowestY -> y(0) ]
                    positive->positive[ highestY -> y(0) ]
                    negative->postiive [lowestY -> highestY]
                */
                let y1=0, y2=0,zero=chartObj.yScale(0);
                // if minY is lower down graph than zero axis
                if (minY > zero) {
                    y1 = zero;
                    y2 = maxY;
                } else {
                    // if maxY is lower down graph than zero axis but minY is above axis
                    if (maxY > zero) {
                        y1 = minY, y2 = maxY;
                    } else {
                        y1 = zero, y2 = minY;
                    }
                }
                focus.select(".focus.line")
                    .attr("transform", "translate(" + (chartObj.xScale(chartObj.xFunct(d)) + getHalfBandWidthX()+chartObj.margin.left)+ ")")
                    .attr("y1", y1)
                    .attr("y2", y2);
            }

        };
  
        return chartObj;
       

          function getHalfBandWidthX() {
              return (chartObj.xScale.rangeBand() / (chartObj.xScale.domain().length + 1));
          }

    }