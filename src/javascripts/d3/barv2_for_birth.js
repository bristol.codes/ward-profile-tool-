function sampleBarv2(div,data, options){
    if(options=="undefined" || !options){
      options={ yAxisLabel: "People"};
    }
    var margin = {top: 40, right: 20, bottom: 150, left: 40},
    width = 300 - margin.left - margin.right,
    height = 500 - margin.top - margin.bottom;
    
    // Parse the date / time
    data.sort(function(a, b) { return b.people - a.people; });

    var x = d3.scale.ordinal().rangeRoundBands([0, width], 0.29);
    
    var y = d3.scale.linear().range([height, 0]);
    
    var xAxis = d3.svg.axis()
    .scale(x)
    .orient("bottom")
    
    var yAxis = d3.svg.axis()
    .scale(y)
    .orient("left")
    .ticks(6);
    
    var x2 = d3.scale.ordinal()
    .rangeRoundBands([0,width],0);	
    
    //var svg = d3.select("#test").append("svg")
    var svg = d3.select(div).append("svg")
    
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
    .attr("class", "svgs2 see hidden")
    .append("g")
        .style("fill", colourScheme.reusedColours.white)   
    .attr("transform", 
          "translate(" + margin.left + "," + margin.top + ")");
          
    var tooltip = d3.select(div).append("div")   
        .attr("class", "tooltip")               
        .style("opacity", 0);
        
        
    /*Label*/
    /*
    svg.append("text")
        .attr("x", (width / 2))             
        .attr("y", 0 - (margin.top / 2))
        .attr("text-anchor", "middle")  
        .style("font-size", "1.5em")
        .style("color",colourScheme.reusedColours.white)
        //.style("text-decoration", "underline")  
        .text("Top 3 countries of birth outside of the UK");
    /*
    data.forEach(function(d) {
        d.date = d.label;
        d.value = d.r;
    });
    */	
    x.domain(data.map(function(d) { return d.country; }));
    y.domain([0, d3.max(data, function(d) { return d.people+200; })]);

    
    svg.append("g")
      .attr("class", "x axis")
      .attr("transform", "translate(0," + height + ")")
      .call(xAxis)
    .selectAll("text")
      .style("text-anchor", "middle")
      
      .attr("dx", "-.2em")
      .attr("dy", function(v,i){
        let text = "";
        let w = $(this).text().split(" ") ;
        for (let index = 0; index < w.length; index++) {
          if(index % 2==0){
            text+="<tspan x=\"0\" dy=\"1.1em\">";
          }
          text += w[index] + " ";
          if(index % 2==1){
            text+= "</tspan>"
          }
        }
        var lastIndex = String.prototype.lastIndexOf.call(text,"</tspan>");
        if(!lastIndex || !(text.length-lastIndex==8)){
          text+= "</tspan>"
        }
        $(this).html(text);
        return "1.1em"; 
      }) // "1.1em")
      .attr("transform", "rotate(0)" );
    
    svg.append("g")
      .attr("class", "y axis")
      .call(yAxis)
    .append("text")
      .attr("transform", "rotate(-90)")
      .style("text-anchor", "end");
    
    if(Object.prototype.hasOwnProperty.call(options,"yAxisLabel") && (""+options.yAxisLabel).length >0){
        svg.append("g")
        .append("text")
        .attr("class", "label")
        .attr("transform", "rotate(-90)")
        .attr("y", -42)
        .attr("x", -height / 2)
        .attr("dy", "1em")
        .style("text-anchor", "middle")
        .text(options.yAxisLabel);
    }
      
    svg.selectAll("bar")
      .data(data)
    .enter().append("rect")
        .style("fill", colourScheme.reusedColours.white)      
      .attr("x", function(d) { return x(d.country); })
      .attr("width", x.rangeBand())
      .attr("y", function(d) { return y(d.people); })
      .attr("height", function(d) { return height - y(d.people); })
      
       .on("mouseover", function(d) {
            tooltip.text(d.country + ", " + Math.round(d.people))
            .style("opacity", 0.8)
                    .style("left", (d3.event.pageX)+0 + "px") 
                    .style("top", (d3.event.pageY)-0 + "px");
        })
        
        
        .on("mouseout", function(d) {
          tooltip.style("opacity", 0);
  
      });
        svg.selectAll("bar")
        .data(data)
            .enter().append("text")
                .attr("class", "bar")
                .attr("x", function(d) { return x(d.country); })
                .attr("y", function(d) { return y(d.people); })
                .attr("dx", "1.2em")
                .attr("dy", "1em")
                .attr("text-anchor", "middle")
                .attr("font-size", "2em")
            .attr("fill", colourScheme.reusedColours.orange)
                .attr("transform", function(d) { return "translate(0, 0)"; })
                .text(function(d) { return d.people; }); 

        };
    
    
    
    