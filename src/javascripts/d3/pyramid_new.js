function pyramidBuilder(data, target, options,colorLeft,colorRight) {
    
    var Legendleft = colorLeft;
    var Legendright = colorRight;

    var w = typeof options.width === 'undefined' ? 400  : options.width,
        h = typeof options.height === 'undefined' ? 400  : options.height,
        w_full = w,
        h_full = h;

    if (w > $( window ).width()) {
      w = $( window ).width();
    }

    var margin = {
            top: 50,
            right: 10,
            bottom: 20,
            left: 10,
            middle: 20
        },
        sectorWidth = (w / 2) - margin.middle,
        leftBegin = sectorWidth - margin.left,
        rightBegin = w - margin.right - sectorWidth;

    w = (w- (margin.left + margin.right) );
    h = (h - (margin.top + margin.bottom));
    
  

    var totalPopulation = d3.sum(data, function(d) {
            return d.male + d.female;
        })
        
        var percentage = function(d) {
            return d / 100;
        };
    
      
    var region = d3.select(target).append('svg')
        .attr('width', w_full)
        .attr('height', h_full)
        .attr('class','mid');


    var legend = region.append('g')
        .attr('class', 'legend');

        // TODO: fix these margin calculations -- consider margin.middle == 0 -- what calculations for padding would be necessary?
    legend.append('rect')
        .attr('class', 'bar Legendleft')
        .attr('x', (w / 2) - (margin.middle * 3))
        .attr('y', 12)
        .attr('width', 12)
        .attr('fill',colorLeft)
        .attr('height', 12);

    legend.append('text')
        .attr('fill', colourScheme.reusedColours.black)
        .attr('x', (w / 2) - (margin.middle * 2))
        .attr('y', 18)
        .attr('dy', '0.32em')
        .text('Males');

    legend.append('rect')
        .attr('class', 'bar Legendright')
        .attr('x', (w / 2) + (margin.middle * 2))
        .attr('y', 12)
        .attr('width', 12)
        .attr('fill',colorRight)
        .attr('height', 12);

    legend.append('text')
        .attr('fill', colourScheme.reusedColours.black)
        .attr('x', (w / 2) + (margin.middle * 3))
        .attr('y', 18)
        .attr('dy', '0.32em')
        .text('Females');

        legend.append('text')
        .style('text-anchor', 'middle')
        .attr('x', 201)
        .attr('y', 39)
        .attr('dy', '0.32em')
        .text('Age');    


    var tooltipDiv = d3.select("body").append("div")
        .attr("class", "tooltip")
        .style("opacity", 0);

    var pyramid = region.append('g')
        .attr('class', 'inner-region')
        .attr('transform', translation(margin.left, margin.top));

    // find the maximum data value for whole dataset
    // and rounds up to nearest 5%
    //  since this will be shared by both of the x-axes
    var maxValue = Math.ceil(Math.max(
        
        d3.max(data, function(d) {
            return d.male;
        }),

        
        d3.max(data, function(d) {
            return d.female;
        })
        
      

    )/0.05)*0.05;

    
    // SET UP SCALES

    // the xScale goes from 0 to the width of a region
    //  it will be reversed for the left x-axis
    var xScale = d3.scaleLinear()
        .domain([0, maxValue])
        .range([0, (sectorWidth-margin.middle)])
        .nice();

    var xScaleLeft = d3.scaleLinear()
        .domain([0, maxValue])
        .range([sectorWidth, 0]);

    var xScaleRight = d3.scaleLinear()
        .domain([0, maxValue])
        .range([0, sectorWidth]);

    var yScale = d3.scaleBand()
        .domain(data.map(function(d) {
            return d.age;
        }))
        .range([h, 0], 0.1);


    // SET UP AXES
    var yAxisLeft = d3.axisRight()
        .scale(yScale)
        .tickSize(4, 0)
        .tickPadding(margin.middle - 4);

    var yAxisRight = d3.axisLeft()
        .scale(yScale)
        .tickSize(4, 0)
        .tickFormat('');

    var xAxisRight = d3.axisBottom()
        .scale(xScale)
        .ticks(4,0)
        .tickFormat(function(d, i) {
            return d + "% ";
          });
        //.tickFormat(d3.format('.0s'));

    var xAxisLeft = d3.axisBottom()
        // REVERSE THE X-AXIS SCALE ON THE LEFT SIDE BY REVERSING THE RANGE
        .scale(xScale.copy().range([leftBegin, 0]))
        .ticks(4)
        .tickFormat(function(d, i) {
            return d + "% ";
          });
        
        //.tickFormat(d3.format('.0s'));

    // MAKE GROUPS FOR EACH SIDE OF CHART
    // scale(-1,1) is used to reverse the left side so the bars grow left instead of right
    var leftBarGroup = pyramid.append('g')
        .attr('transform', translation(leftBegin, 0) + 'scale(-1,1)');
    var rightBarGroup = pyramid.append('g')
        .attr('transform', translation(rightBegin, 0));

    // DRAW AXES
    pyramid.append('g')
        .attr('class', 'axis y left')
        .attr('transform', translation(leftBegin, 0))
        .call(yAxisLeft)
        .selectAll('text')
        .attr("class","pyramidUniqueClass")
        .style('text-anchor', 'middle');

    pyramid.append('g')
        .attr('class', 'axis y right')
        .attr('transform', translation(rightBegin, 0))
        .call(yAxisRight);

    pyramid.append('g')
        .attr('class', 'axis x left')
        .attr('transform', translation(0, h))
        .call(xAxisLeft);

    pyramid.append('g')
        .attr('class', 'axis x right')
        .attr('transform', translation(rightBegin, h))
        .call(xAxisRight);

    // DRAW BARS
    leftBarGroup.selectAll('.bar.left')
        .data(data)
        .enter().append('rect')
        .attr('class', 'bar left')
        .attr('x', 0)
        .attr("fill",colorLeft)
        .attr('y', function(d) {
            return yScale(d.age) + margin.middle / 4;
        })
        .attr('width', function(d) {
            return xScale(d.male);
        })
        .attr('height', (yScale.range()[0] / data.length) - margin.middle / 2)
        .on("mouseover", function(d) {
            tooltipDiv.transition()
                .duration(200)
                .style("opacity", 0.9);
            tooltipDiv.html("<strong>Males Age " + d.age + "</strong>" +
                    "<br />% of total male population: " + prettyFormat(d.male))
                .style("left", (d3.event.pageX) + "px")
                .style("top", (d3.event.pageY - 28) + "px");
        })
        .on("mouseout", function(d) {
            tooltipDiv.transition()
                .duration(500)
                .style("opacity", 0);
        });

    rightBarGroup.selectAll('.bar.right')
        .data(data)
        .enter().append('rect')
        .attr('class', 'bar right')
        .attr('x', 0)
        .attr("fill",colorRight)
        .attr('y', function(d) {
            return yScale(d.age) + margin.middle / 4;
        })
        .attr('width', function(d) {
            return xScale(d.female);
        })
        .attr('height', (yScale.range()[0] / data.length) - margin.middle / 2)
        .on("mouseover", function(d) {
            tooltipDiv.transition()
                .duration(200)
                .style("opacity", 0.9);
            tooltipDiv.html("<strong> Females Age " + d.age + "</strong>" +
                    "<br />% of total female  population: " + prettyFormat(d.female))
                .style("left", (d3.event.pageX) + "px")
                .style("top", (d3.event.pageY - 28) + "px");
        })
        .on("mouseout", function(d) {
            tooltipDiv.transition()
                .duration(500)
                .style("opacity", 0);
        });

    /* HELPER FUNCTIONS */

    // string concat for translate
    function translation(x, y) {
        return 'translate(' + x + ',' + y + ')';
    }

    // numbers with commas
    function prettyFormat(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

   

    $(".pyramidUniqueClass").each(function( index ) {
        
        var newText = $(this).text();

        newText=newText.replace(/_/g,"-");

        $(this).text(newText);


    });


};