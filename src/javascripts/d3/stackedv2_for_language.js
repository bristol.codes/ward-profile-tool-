function stackedv2(div,data){
  // Setup svg using Bostock's margin convention
  
  var margin = {top: 20, right: 160, bottom: 200, left: 30};
  
  var width = 400 - margin.left - margin.right,
      height = 550 - margin.top - margin.bottom;
  
  var svg = d3.select(div)
    .append("svg")
    .attr("width", "100%")
    .attr("height", height + margin.top + margin.bottom)
    .attr("preserveAspectRatio", "xMinYMin meet")
    .attr("viewBox", "0 0 350 250")
    .append("g")
    .attr("transform", "translate(" + "90" + "," + margin.top + ")");
  //.attr("transform", "translate(" + margin.left + "," + margin.top + ")");
  
    data.sort(function(a, b) { return b.space - a.space; });
  /* Data in strings like it would be if imported from a csv */
  
  
  // Transpose the data into layers
  var dataset = d3.layout.stack()(["main","notMain"].map(function(country) {
      return data.map(function(d) {
        return {x:  d.ward, y: +d[country]};
      });
    }));
    
  
  // Set x, y and colors
  var x = d3.scale.ordinal()
    .domain(dataset[0].map(function(d) { return d.x; }))
    .rangeRoundBands([10, width-10], 0.2);
  
  var y = d3.scale.linear()
    //.domain([0, d3.max(dataset, function(d) {  return d3.max(d, function(d) {  return d.space; });  })])
    .domain([0, d3.max(data, function(d) { return 100; })])
    .range([height, 0]);
  
    var colorsOrange = [colourScheme.reusedColours.lightOrange, colourScheme.reusedColours.darkOrange];

    var colorsBlue = [colourScheme.reusedColours.lightBlue, colourScheme.reusedColours.darkBlue];
  
  // Define and draw axes
  var yAxis = d3.svg.axis()
    .scale(y)
    .orient("left")
    .tickSize(-width, 0, 0)
    .tickFormat( function(d) { return d } );
    
  
  var xAxis = d3.svg.axis()
    .scale(x)
    .orient("bottom")
    
  
  svg.append("g")
    .attr("class", "y axis")
    .call((yAxis).ticks(10));
  
  svg.append("g")
   .attr("class", "x axis")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis)
        .selectAll("text")
        .style("text-anchor", "end")
        .attr("dx", ".8em")
        .attr("dy", "+.1em")
        .attr("transform", "rotate(0)" );
  
    
  var groups = svg.selectAll("g.cost")
    .data(dataset)
    .enter().append("g")
    .attr("class", "cost")
    
  var rect = groups.selectAll("rect")
    .data(function(d) { return d; })
    .enter()
    .append("rect")
    .attr("x", function(d) { return x(d.x); })
    .attr("y", function(d) { return y(d.y0 + d.y); })
    .attr("height", function(d) { return y(d.y0) - y(d.y0 + d.y); })
    .attr("width", x.rangeBand())
    
    .attr("fill", function(d, i, j) { // uses colourScheme
      if (d.x == ward) {
         return colorsBlue [(j*1)+i];
      }else{
        return colorsOrange[(j-1*1)+i];
      }
      
     })

    .on("mouseover", function() { tooltip.style("display", null); })
    .on("mouseout", function() { tooltip.style("display", "none"); })
    .on("mousemove", function(d) {
      var xPosition = d3.mouse(this)[0] - 15;
      var yPosition = d3.mouse(this)[1] - 25;
      tooltip.attr("transform", "translate(" + xPosition + "," + yPosition + ")");
      tooltip.select("text").text(d.y + "%");
    });


  groups.selectAll(".text")
    .data(data)
    .enter()
    .append("text")
    .attr("class", "label")
    .attr("font-size", "1em")
    .attr("font-weight", "bold")
      .attr("fill", colourScheme.reusedColours.black)
    .attr("x", (function (d) { return x(d.ward) + x.rangeBand() / 2; }))
    .attr("y", function (d) { return y(d.main) + 1; })
    .attr("dy", "1em")
    .attr("dx", "-1em")
    .text(function (d) { return d.main + " %"; });

  groups.selectAll(".text")
    .data(data)
    .enter()
    .append("text")
    .attr("class", "label")
    .attr("font-size", "1em")
    .attr("font-weight", "bold")
      .attr("fill", colourScheme.reusedColours.black)
    .attr("x", (function (d) { return x(d.ward) + x.rangeBand() / 2; }))
    .attr("y", function (d, i) { return y(d.space) + 1; })
    .attr("dy", "-.25em")
    .attr("dx", "-1em")
    .text(function (d) { return d.notMain + " %"; });
  
  // Prep the tooltip bits, initial display is hidden
  var tooltip = svg.append("g")
    .attr("class", "tooltip")
    .style("display", "none");
      
  tooltip.append("rect")
    .attr("width", 40)
    .attr("height", 20)
      .attr("fill", colourScheme.reusedColours.white)
    .style("opacity", 0.6);
  
  tooltip.append("text")
    .attr("x", 20)
    .attr("dy", "1.2em")
    .style("text-anchor", "middle")
    .attr("font-size", "12px")
    .attr("font-weight", "bold");
  

  $(".wEng").css("background-color",colorsOrange[0]);

  $(".bEng").css("background-color",colorsBlue[0]);

  $(".wNEng").css("background-color",colorsOrange[1]);

  $(".bNEng").css("background-color",colorsBlue[1]);
  
  
  
  }