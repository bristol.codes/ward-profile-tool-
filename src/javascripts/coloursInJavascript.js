const colourScheme = {
    reusedColours: {
        black: "black",
        white: "white",
        orange: "orange",
        yellow: "yellow", // used by age at least
        offyellow: "rgb(242, 228, 66)", // used by sampleCirc and test.js as amber / yellow
        red: "red",
        green: "#0fff01",
        gray: "grey",
        grey: "grey",
        pink: "rgb(230, 187, 187)", //currently #e6bbbb in css, mauve doesnt exist in css, palevioletred is closest, a pale pink
        purple: "rgb(167,79,171)",/* was 204,121,167)", */
        darkOrange: "rgb(230, 159, 0)",
        darkBlue: "rgb(0, 114, 178)",
        lightOrange: "rgb(255,219,153)",// rgba(255, 165, 0, 0.40) on white bg = rgb(255,219,153)
        lightBlue: "rgb(167,200,227)", // rgba(34, 118, 186, 0.40) on white = rgb(167,200,227)
    }

};
colourScheme.reusedColours.mauve=colourScheme.reusedColours.pink;

colourScheme.stack = {
    darkOrange: colourScheme.reusedColours.darkOrange,
    midOrange: "rgb(255,177,33)",//"#ffa500de" rgba(255, 165, 0, 0.87)
    lightOrange: colourScheme.reusedColours.lightOrange, //#ffa500ab -- too similar
}

colourScheme.age = {
    yellow: colourScheme.reusedColours.yellow,
    pink: colourScheme.reusedColours.pink,
    purple: colourScheme.reusedColours.purple,
};

// This is also 4 confidenceLevels
colourScheme.sampleCircle = {
    yellow: colourScheme.reusedColours.offyellow, //"rgb(242, 228, 66)",// "#F2E442"
    amber: colourScheme.reusedColours.offyellow, //"rgb(242, 228, 66)",// "#F2E442"
    grey: colourScheme.reusedColours.grey,
    red: "rgb(255, 1, 1)", // #ff0101
    green: "rgb(15, 255, 1)", //0fff01
};


colourScheme.pyramid = {
    darkOrange: colourScheme.reusedColours.darkOrange, // dark orange
    lightOrange: colourScheme.reusedColours.lightOrange,  // lightOrange
    darkBlue: colourScheme.reusedColours.darkBlue,   //dark blue
    lightBlue: colourScheme.reusedColours.lightBlue,   //lightblue
};