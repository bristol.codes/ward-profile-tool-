/*
Education Category Page

On this page each function is called when the option is changed.


*/



$(function(){


var n = 0;


$('#educationOptions').on('change', function() {

  
$(".svgs").remove();
$(".svgs2").remove();
$(".svgs3").remove();
$("#Trend").show();

var selectedText = $("#educationOptions option:selected").html();


selectedText = makeAMP(selectedText);

$(".labelIndicator").text(selectedText);
 /*
    check to see what option is clicked
    Note you have to make sure that you type in the folliwng to the function:
    -Json file name and its location 
    - the indicator that we want to target withing that file
    - the name of the indicator where the year is held
    - 3 dates to make sure that the trend line displays correcrlty
    - & or and depeding on what is used withing the Json file to make sure wards such as Avonmouth (and or &) Lawrence Weston work.  
    /for Quality of life the trend line is called as seperate function due to different naming conventions./
    */

      //each value matches the value on the select field in education.pug
  if ( this.value == 'meals')
  {

    sampleFunc("free-school-meals-in-bristol.json",
"free_school_meals_of_all_pupils","year",["2019","2018","2017","2016"],"&",{ addedToMouseOverUnits:'%'});

  }
  else if ( this.value == 'diasdv')
  {
    sampleFunc("pupils-classed-as-disadvantaged-in-bristol.json",
"disadvantaged_of_all_pupils","year",["2019", "2018","2017","2016"],"&",{ addedToMouseOverUnits:'%'});
    
  }
  else if ( this.value == 'spec')
  {
    sampleFunc("pupils-with-special-educational-needs-in-bristol.json",
"of_sen_pupils","year",["2019","2018","2017"],"&",{ addedToMouseOverUnits:'%'});
   
  }
  else if ( this.value == 'eng')
  {
    sampleFunc("pupils-with-english-as-an-additional-language-in-bristol-by-ward.json",
"english_as_an_additional_language_of_all_pupils","year",["2019","2018","2017"],"&",{ addedToMouseOverUnits:'%'});
    
  }
  else if ( this.value == 'avg')
  {
    //$(".inner-wrapper.svgs3").remove();
    sampleFunc("progress-8-and-attainment-8-scores-in-bristol.json",
"average_progress_8_score","time_period",["2017/2018","2016/2017","2015/2016"],"and", {decimals:2, addedToMouseOverUnits:''});
    // $("#Trend").hide();
  }
  else if ( this.value == 'early')
  {
    sampleFunc("early-years-pupils-achieving-a-good-level-of-development-in-bristol.json",
"achieving_a_good_level_of_development","time_period",["2018/2019", "2017/2018","2016/2017","2015/2016"],"and",{ addedToMouseOverUnits:'%'});

  }
  else if ( this.value == 'ks2')
  {
    sampleFunc("key-stage-2-pupils-level-4-in-reading-writing-maths-combined.json",
"percentage_of_pupils_reaching_the_expected_standard_in_reading_writing_maths","academic_year",["2018/2019", "2017/18","2016/17","2015/16"],"&",{ addedToMouseOverUnits:'%'});

  }
  else if ( this.value == 'absence')
  {
    
    sampleFunc("pupil-absence-in-bristol.json",
"absence","time_period",["2017/18","2016/17"],"and",{ addedToMouseOverUnits:'%'});
 
  } else if ( this.value == 'att')
  {
    sampleFunc("progress-8-and-attainment-8-scores-in-bristol.json",
"average_attainment_8_score","time_period",["2017/2018","2016/2017","2015/2016"],"and"); 
  }
 
  else
  {
    console.log("Indicator not found:",this.value);
  }

$(".compare").removeClass("see");


});//end of if statement


$('#educationOptions').val("meals").trigger('change');


});//end of script