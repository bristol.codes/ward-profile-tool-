

$(function(){

  
$('#healthyLOptions').on('change', function() {
  
    $(".svgs").remove();
    $(".svgs2").remove();
    $(".svgs3").remove();
    $("#Trend").removeClass("hidden");
    
    
    var selectedText = $("#healthyLOptions option:selected").html();
    
    
    selectedText = makeAMP(selectedText).toLowerCase();

    $(".labelIndicator").text("% "+selectedText);
    $(".subtit").html("Quality of Life Survey 2018-19").removeClass("noPadding");
    
      if ( this.value == 'gHealth')
      {
    
        sampleFuncForQofL("/q_of_life/qol-indicator-data-rag-ratings.json","% in good health","&"); 
        trend("/q_of_life/qol-indicators-trend-ward-profile-2019.json","% in good health","&",["2018","2017","2015"]); 
      
      }
      else if ( this.value == 'limits')
      {
         sampleFuncForQofL("/q_of_life/qol-indicator-data-rag-ratings.json","% with illness or health condition which limits day-to-day activities","&"); 
         trend("/q_of_life/qol-indicators-trend-ward-profile-2019.json","% with illness or health condition which limits day-to-day activities","&",["2018","2017","2015"]); 
      
      }
      else if ( this.value == 'wellbeing')
      {
        sampleFuncForQofL("/q_of_life/qol-indicator-data-rag-ratings.json","% above average mental wellbeing","&"); 
        trend("/q_of_life/qol-indicators-trend-ward-profile-2019.json","% above average mental wellbeing","&",["2018","2017","2015"]); 
      
      }
      else if ( this.value == 'households')
      {
        sampleFuncForQofL("/q_of_life/qol-indicator-data-rag-ratings.json","% households where someone smokes regularly within the home","&"); 
        trend("/q_of_life/qol-indicators-trend-ward-profile-2019.json","% households where someone smokes regularly within the home","&",["2018","2017","2015"]); 
      

      }
      else if ( this.value == 'sugar')
      {
        sampleFuncForQofL("/q_of_life/qol-indicator-data-rag-ratings.json","% who consume above recommended amount of sugar","&"); 
        trend("/q_of_life/qol-indicators-trend-ward-profile-2019.json","% who consume above recommended amount of sugar","&",["2018","2017","2015"]); 
      
      
      }
      else if ( this.value == 'eExercise')
      {
         sampleFuncForQofL("/q_of_life/qol-indicator-data-rag-ratings.json","% who do enough regular exercise each week (at least 150 mins moderate or 75 mins vigorous exercise)","&"); 
         trend("/q_of_life/qol-indicators-trend-ward-profile-2019.json","% who do enough regular exercise each week (at least 150 mins moderate or 75 mins vigorous exercise)","&",["2018","2017","2015"]); 
      
    
      }
      else if ( this.value == 'overweight')
      {
         sampleFuncForQofL("/q_of_life/qol-indicator-data-rag-ratings.json","% overweight or obese","&"); 
         trend("/q_of_life/qol-indicators-trend-ward-profile-2019.json","% overweight or obese","&",["2018","2017","2015"]); 
      
    
      }
      else if ( this.value == 'childrenRec')
      {
        
        sampleFuncConf("/health_and_wellbeing/reception-children-with-excess-weight-in-bristol.json","reception_children_with_excess_weight","time_period_3_year_average",["2015/16 to 2017/18","2014/15 to 2016/17","2013/14 to 2015/16"],"&",{addedToMouseOverUnits:'%'});
         $(".subtit").html("Public Health National Child Measurement Programme 2015/16 - 2017/18").addClass("noPadding"); 
      } else if ( this.value == 'childrenYear6')
      {
        sampleFuncConf("/health_and_wellbeing/year-6-children-age-10-11-years-with-excess-weight-in-bristol.json","year_6_children_with_excess_weight","time_period_3_year_average",["2015/16 to 2017/18","2014/15 to 2016/17","2013/14 to 2015/16"],"&", {addedToMouseOverUnits:'%'});
        $(".subtit").html("Public Health National Child Measurement Programme 2015/16 - 2017/18").addClass("noPadding"); 
      }
      else
      {
        console.log("Indicator not found:",this.value);
      }
      
   
    });//end of if statement
    
    $('#healthyLOptions').val("gHealth").trigger('change');
    
    
    });//end of script