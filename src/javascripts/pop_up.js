$(document).ready(function(){

/*Used for closing and opening tabs */
 $(".holder li a" ).click(function() {

    $(this).toggleClass("mainBlueBck");

    $(this).parent().parent().find('.popUp').slideToggle("fast");

    if($(this).hasClass("mainBlueBck")){

      $(this).find(".arrw").empty();
      $(this).find(".arrw").append("▼");

    }else {
      $(this).find(".arrw").empty();
      $(this).find(".arrw").append("►")

    }
    
    

  });


  if ((window.location.pathname == "/wards/" + ward+"/child_poverty") || (window.location.pathname == "/wards/" + ward+"/deprivation") ){

    $(".cat").remove();
  };
    
  $(function(){
    //Hide the footer and title on pages such as the front page of each ward
    if ((window.location.pathname == "/wards/" + ward) || (window.location.pathname == "/wards/" + ward+"/") || (window.location.pathname == "/wards/" + ward.toLowerCase()+"/")){
          $('#footer').addClass('hide');
          $(".titH").remove();
    } else {
          $('#footer').removeClass('hide');
         
    }
     
});


$(function(){
  //remove titles on further information pages
  if ((window.location.pathname == "/mapping_tools") ||
  (window.location.pathname == "/background_and_help") ||
  (window.location.pathname == "/sources") ||
  (window.location.pathname == "/ward_map")){
       
        //$('.titH').addClass('hide');
        $(".titH").remove();
  } else {
        
        //$('.titH').removeClass('hide');
  }
   
});

//used to fix a styling issue where the category name is very long
if (window.location.pathname == "/wards/"+ward+"/population_by_country_of_birth_and_english_as_main_language") {
  $('.subtit').css("padding-left","0px");
  
} 
  
});