

$(function(){
    var n = 0;
    
    
    $('#preOptions').on('change', function() {
      
    $(".svgs").remove();
    $(".svgs2").remove();
    $(".svgs3").remove();
    
    var selectedText = $("#preOptions option:selected").html();
    
    
    $(".labelIndicator").text(selectedText);
    
      if ( this.value == 'all')
      {
    
        sampleFuncConf("/health_and_wellbeing/premature-mortality-all-causes-in-bristol.json","all_cause_prem_mortality_asr_persons","year",["2015-2017","2014-2016","2013-2015"],"&",{addedToMouseOverUnits:''});
    
      }
      else if ( this.value == 'cancer')
      {
        sampleFuncConf("/health_and_wellbeing/premature-mortality-by-selected-cause-in-bristol.json","cancer_premature_mortality_asr_persons","year",["2015-2017","2014-2016","2013-2015"],"&",{addedToMouseOverUnits:''});
        
      }
      else if ( this.value == 'cardio')
      {
        sampleFuncConf("/health_and_wellbeing/premature-mortality-by-selected-cause-in-bristol.json","cardiovascular_disease_premature_mortality_asr_persons","year",["2015-2017","2014-2016","2013-2015"],"&",{addedToMouseOverUnits:''});
      }
      else if ( this.value == 'respi')
      {
        sampleFuncConf("/health_and_wellbeing/premature-mortality-by-selected-cause-in-bristol.json","respiratory_disease_premature_mortality_asr_persons","year",["2015-2017","2014-2016","2013-2015"],"&",{addedToMouseOverUnits:''});
        
      }
      else
      {
        console.log("Indicator not found:",this.value);
      }
    
    $(".compare").removeClass("see");
    
    
    });//end of if statement
    
    $('#preOptions').val("all").trigger('change');
    
    
    });//end of script