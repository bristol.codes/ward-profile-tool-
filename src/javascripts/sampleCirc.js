function sampleCirc(div,colour,data,orient,fixed, suppliedOptions){
  const defaultOptions={
    decimals: 1
  };
  let options = {...defaultOptions, ...suppliedOptions}
    if (colour == "yellow"){ //uses colourScheme.sampleCircle
      colour = colourScheme.sampleCircle.yellow;
    }
    var n =0;
    var width = 960,
        height = 200;
  
      var svg = d3.select(div).append("svg")
      .attr("width", width)
      .attr("height", height)
      .attr("align","center")
      .attr("viewBox","0 0 800 300")
      .attr("class", "svgs see hidden "+ orient)
      
      
      var elem = svg.selectAll("g myCircleText")
          .data(data)
    
      /*Create and place the "blocks" containing the circle and the text */  
      var elemEnter = elem.enter()
        .append("g")
        //.attr("transform", function(d){return "translate("+d.x+",80)"})
        .style('transform', 'translate(50%, 50%)')
    
      /*Create the circle for each block */
      var circle = elemEnter.append("circle")
          .attr("r", fixed + "%")
      .style("fill", function(d){return ""+colour+""; });
    
      /* Create the text for each block */
      elemEnter.append("text")
      .attr("text-anchor", "middle")
      .attr("font-size","3em")
        //.attr("dx", function(d){return})
        .text(function(d){return (+( d.info)).toFixed(options.decimals); })
        .attr("class", "txt")
      //r for radius
      //l label
    
      n++;
    
    };