$(document).ready(function(){


var width = 960,
    height = 200;
 
var svg = d3.select("#test2").append("svg")
    .attr("width", width)
    .attr("height", height)
    
d3.json("../../Json/crime2.json", function(json) {

    /* Define the data for the circles */


    var elem = svg.selectAll("g myCircleText")
        .data(json)
  
    /*Create and place the "blocks" containing the circle and the text */  
    var elemEnter = elem.enter()
	    .append("g")
	    .attr("transform", function(d){return "translate("+d.x+",80)"})
 
    /*Create the circle for each block */
    var circle = elemEnter.append("circle")
	    .attr("r", function(d){return d.r} )
		.style("fill", function(d){ return colourScheme.reusedColours.red; });
 
    /* Create the text for each block */
    elemEnter.append("text")
	    .attr("dx", function(d){return -10})
	    .text(function(d){return d.r})
		//r for radius
		//l label
		
});

});