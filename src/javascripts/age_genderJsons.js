
$(function(){

function ageGender(fileName,midYear){
    var tempArray = [];
    $.getJSON( "../../Json/"+fileName, function( data ) {
         
        $.each(data, function(i, obj) {

           var wardN = data[i].fields["2016_ward_name"];
           var children = data[i].fields.children_0_15_year_olds;
           var workingAge = data[i].fields.working_age_16_64_year_olds;
           var olderPeople = data[i].fields.older_people_65_years_and_over; 
           var year = data[i].fields.mid_year;
           var totP = data[i].fields.total_population_all_ages; 
              
            if (midYear == year) {
                var someData = { name: wardN, children: children, workingAge: workingAge, olderPeople: olderPeople, total: totP };
                tempArray.push(someData);
            } 
       
        });
     
        stackedv2(tempArray,ward);

    });    

};


function calcPyramid(fileName, sign, suppliedOptions){
    const defaultOptions={
        decimals: 1,
        height: 400,
        width: 400
      };
    let options = {...defaultOptions, ...suppliedOptions}
    var testing = changeName(ward,sign);

    ward = testing;

    var females =[];
    var males =[];
    var someData = {};
    var combined =[];
    var combined2 =[];
    var someData2 = {};
    var someData3 = {};
    var ageGrp =0;
    var tempArray = [];
    var tempArray2 = [];
    var tempArray3 =[];
    var tempArray4 =[];
    var a=0;
    var b=4;

    var wardTotF =0;
    var wardTotm =0;
    var bristolTot =0;
    var bristolTot2 =0;

    var  bristol_total_age_0_4 = 0;
    var  bristol_total_age_5_9 =0;
    var  bristol_total_age_10_14 = 0;
    var  bristol_total_age_15 = 0;
    var  bristol_total_age_16_19 = 0;
    var  bristol_total_age_20_24 = 0;
    var  bristol_total_age_25_29 =0;
    var  bristol_total_age_30_34 = 0;
    var  bristol_total_age_35_39 =0;
    var  bristol_total_age_40_44 = 0;
    var  bristol_total_age_45_49 = 0;
    var  bristol_total_age_50_54 = 0;
    var  bristol_total_age_55_59 = 0;
    var  bristol_total_age_60_64 = 0;
    var  bristol_total_age_65_69 =0;
    var  bristol_total_age_70_74 =0;
    var  bristol_total_age_75_79 = 0;
    var  bristol_total_age_80_84 = 0;
    var  bristol_total_age_85_89 =0;
    var  bristol_total_age_90 =0;

    var  bristol_total_age_0_4v2 = 0;
    var  bristol_total_age_5_9v2 =0;
    var  bristol_total_age_10_14v2 = 0;
    var  bristol_total_age_15v2 = 0;
    var  bristol_total_age_16_19v2 = 0;
    var  bristol_total_age_20_24v2 = 0;
    var  bristol_total_age_25_29v2 =0;
    var  bristol_total_age_30_34v2 = 0;
    var  bristol_total_age_35_39v2 =0;
    var  bristol_total_age_40_44v2 = 0;
    var  bristol_total_age_45_49v2 = 0;
    var  bristol_total_age_50_54v2 = 0;
    var  bristol_total_age_55_59v2 = 0;
    var  bristol_total_age_60_64v2 = 0;
    var  bristol_total_age_65_69v2 =0;
    var  bristol_total_age_70_74v2 =0;
    var  bristol_total_age_75_79v2 = 0;
    var  bristol_total_age_80_84v2 = 0;
    var  bristol_total_age_85_89v2 =0;
    var  bristol_total_age_90v2 =0;


    var precentwardM =[];
    var precentwardF =[];
    var precentbrstM=[];
    var precentbrstf=[];

    $.getJSON( "../../Json/"+fileName, function( data ) {
        
        $.each(data, function(i, obj) {

           

            var age_0_4 = data[i].fields.age_0_4;
            var age_5_9 = data[i].fields.age_5_9;
            var age_10_14 = data[i].fields.age_10_14;
            //var age_15 = data[i].fields.age_15;
            var age_16_19 = data[i].fields.age_16_19 + data[i].fields.age_15;
            var age_20_24 = data[i].fields.age_20_24;
            var age_25_29 = data[i].fields.age_25_29;
            var age_30_34 = data[i].fields.age_30_34;
            var age_35_39 = data[i].fields.age_35_39;
            var age_40_44 = data[i].fields.age_40_44;
            var age_45_49 = data[i].fields.age_45_49;
            var age_50_54 = data[i].fields.age_50_54;
            var age_55_59 = data[i].fields.age_55_59;
            var age_60_64 = data[i].fields.age_60_64;
            var age_65_69 = data[i].fields. age_65_69;
            var age_70_74 = data[i].fields.age_70_74;
            var age_75_79 = data[i].fields.age_75_79;
            var age_80_84 = data[i].fields.age_80_84;
            var age_85_89 = data[i].fields.age_85_89;
            var age_90 = data[i].fields.age_90;
            var allAge = data[i].fields.all_ages;
            var wardName = data[i].fields["2016_ward_name"];
            var sex = data[i].fields.sex;


            if(sex == "Male"){

                bristolTot = bristolTot + allAge;    
                bristol_total_age_0_4 = bristol_total_age_0_4 + age_0_4;
                bristol_total_age_5_9 = bristol_total_age_5_9 + age_5_9;
                bristol_total_age_10_14 = bristol_total_age_10_14 + age_10_14;
                //bristol_total_age_15 = bristol_total_age_15 + age_15;
                bristol_total_age_16_19 =  bristol_total_age_16_19 + age_16_19;
                bristol_total_age_20_24 = bristol_total_age_20_24 + age_20_24;
                bristol_total_age_25_29 = bristol_total_age_25_29 + age_25_29;
                bristol_total_age_30_34 =  bristol_total_age_30_34 + age_30_34;
                bristol_total_age_35_39 = bristol_total_age_35_39 + age_35_39;
                bristol_total_age_40_44 = bristol_total_age_40_44 + age_40_44;
                bristol_total_age_45_49 = bristol_total_age_45_49 + age_45_49;
                bristol_total_age_50_54 = bristol_total_age_50_54 + age_50_54;
                bristol_total_age_55_59 = bristol_total_age_55_59 + age_55_59;
                bristol_total_age_60_64 = bristol_total_age_60_64 + age_60_64;
                bristol_total_age_65_69 = bristol_total_age_65_69 + age_65_69;
                bristol_total_age_70_74 = bristol_total_age_70_74 + age_70_74;
                bristol_total_age_75_79 = bristol_total_age_75_79 + age_75_79;
                bristol_total_age_80_84 = bristol_total_age_80_84 + age_80_84;
                bristol_total_age_85_89 = bristol_total_age_85_89 + age_85_89;
                bristol_total_age_90 =  bristol_total_age_90 + age_90;

                


            }
             if(sex == "Female"){

                bristolTot2 = bristolTot2 + allAge;

                bristol_total_age_0_4v2 = bristol_total_age_0_4v2 + age_0_4;
                bristol_total_age_5_9v2 = bristol_total_age_5_9v2 + age_5_9;
                bristol_total_age_10_14v2 = bristol_total_age_10_14v2 + age_10_14;
               // bristol_total_age_15v2 = bristol_total_age_15v2 + age_15;
                bristol_total_age_16_19v2 =  bristol_total_age_16_19v2 + age_16_19;
                bristol_total_age_20_24v2 = bristol_total_age_20_24v2 + age_20_24;
                bristol_total_age_25_29v2 = bristol_total_age_25_29v2 + age_25_29;
                bristol_total_age_30_34v2 =  bristol_total_age_30_34v2 + age_30_34;
                bristol_total_age_35_39v2 = bristol_total_age_35_39v2 + age_35_39;
                bristol_total_age_40_44v2 = bristol_total_age_40_44v2 + age_40_44;
                bristol_total_age_45_49v2 = bristol_total_age_45_49v2 + age_45_49;
                bristol_total_age_50_54v2 = bristol_total_age_50_54v2 + age_50_54;
                bristol_total_age_55_59v2 = bristol_total_age_55_59v2 + age_55_59;
                bristol_total_age_60_64v2 = bristol_total_age_60_64v2 + age_60_64;
                bristol_total_age_65_69v2 = bristol_total_age_65_69v2 + age_65_69;
                bristol_total_age_70_74v2 = bristol_total_age_70_74v2 + age_70_74;
                bristol_total_age_75_79v2 = bristol_total_age_75_79v2 + age_75_79;
                bristol_total_age_80_84v2 = bristol_total_age_80_84v2 + age_80_84;
                bristol_total_age_85_89v2 = bristol_total_age_85_89v2 + age_85_89;
                bristol_total_age_90v2 =  bristol_total_age_90v2 + age_90;

                          
                
            }  

        if (wardName == ward){    

            if(sex == "Male"){

               
                wardTotm = allAge; 
                
                tempArray.push(age_0_4,age_5_9,age_10_14,age_16_19,age_20_24,age_25_29,age_30_34,age_35_39,age_40_44,age_45_49,age_50_54,age_55_59,age_60_64,age_65_69,age_70_74,age_75_79,age_80_84,age_85_89,age_90);
                

            }else if (sex =="Female"){
                
                wardTotF = allAge;
              
             
               tempArray2.push(age_0_4,age_5_9,age_10_14,age_16_19,age_20_24,age_25_29,age_30_34,age_35_39,age_40_44,age_45_49,age_50_54,age_55_59,age_60_64,age_65_69,age_70_74,age_75_79,age_80_84,age_85_89,age_90);


            }



        }    
        
        });

        


        tempArray3.push(bristol_total_age_0_4,bristol_total_age_5_9,bristol_total_age_10_14,bristol_total_age_16_19,bristol_total_age_20_24,bristol_total_age_25_29,bristol_total_age_30_34,bristol_total_age_35_39,bristol_total_age_40_44,bristol_total_age_45_49,bristol_total_age_50_54,bristol_total_age_55_59,bristol_total_age_60_64,bristol_total_age_65_69,bristol_total_age_70_74,bristol_total_age_75_79,bristol_total_age_80_84,bristol_total_age_85_89,bristol_total_age_90);
        tempArray4.push(bristol_total_age_0_4v2,bristol_total_age_5_9v2,bristol_total_age_10_14v2,bristol_total_age_16_19v2,bristol_total_age_20_24v2,bristol_total_age_25_29v2,bristol_total_age_30_34v2,bristol_total_age_35_39v2,bristol_total_age_40_44v2,bristol_total_age_45_49v2,bristol_total_age_50_54v2,bristol_total_age_55_59v2,bristol_total_age_60_64v2,bristol_total_age_65_69v2,bristol_total_age_70_74v2,bristol_total_age_75_79v2,bristol_total_age_80_84v2,bristol_total_age_85_89v2,bristol_total_age_90v2);
     
        functionChangArr(tempArray,wardTotm,precentwardM);
        functionChangArr(tempArray2,wardTotF,precentwardF);
        functionChangArr(tempArray3,bristolTot,precentbrstM);
        functionChangArr(tempArray4,bristolTot2,precentbrstf);

        //makeNeg(precentwardF);
        //makeNeg(precentbrstf);

    
        for(var i=0;i< precentwardM.length;i++){


/*
            if (b > 89){
                someData2 = {ageGroup:a+"+",malesWard:precentwardM[i],femalesWard:precentwardF[i],bristolMales:precentbrstM[i],bristolFemales:precentbrstf[i]};

                combined.push(someData2);

                b = 0;
                a = a+5;

            }else{
                someData2 = {ageGroup:a+"_"+b,malesWard:precentwardM[i],femalesWard:precentwardF[i],bristolMales:precentbrstM[i],bristolFemales:precentbrstf[i]};

                combined.push(someData2);

                a = a+5;
                b = b+5;
            }
*/
            if (b > 89){

                someData3 = {age:a+"+",male:precentbrstM[i],female:precentbrstf[i]};

                combined2.push(someData3);


                someData2 = {age:a+"+",male:precentwardM[i],female:precentwardF[i]};

                combined.push(someData2);

                b = 0;
                a = a+5;

            }else{

                someData2 = {age:a+"_"+b,male:precentwardM[i],female:precentwardF[i]};

                combined.push(someData2);

                someData3 = {age:a+"_"+b,male:precentbrstM[i],female:precentbrstf[i]};

                combined2.push(someData3);

                a = a+5;
                b = b+5;
            }

        }

        pyramidBuilder(combined, '.populationPyramid2', options, colourScheme.pyramid.darkBlue, colourScheme.pyramid.lightBlue);
        pyramidBuilder(combined2, '.populationPyramid', options, colourScheme.pyramid.darkOrange, colourScheme.pyramid.lightOrange);
     
    });//end of json       

};//end of func


ageGender("/population/population-mid-2012-to-mid-2017-by-broad-age-band-2016-ward.json","2018");
populationFunc("/population/population-estimates-mid-2017-by-five-year-age-band-and-sex-2016-ward.json","and");
calcPyramid("/population/population-estimates-mid-2017-by-five-year-age-band-and-sex-2016-ward.json","and");

});    