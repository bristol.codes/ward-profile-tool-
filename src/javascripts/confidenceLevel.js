function confidenceLevels(fileName, wardUpper, wardLower, bristolUpper, bristolLower) {
    //returns direct colour values / css compatible colour values for use in fill/background-color etc

    var clrs = colourScheme.reusedColours.grey;

    if (fileName === "health_and_wellbeing/life-expectancy-in-bristol.json") {
        //high is good 
        if (wardLower > bristolUpper) {
            //it is high green
            // console.log("confidence high");
            clrs = colourScheme.sampleCircle.green;

        } else if (wardUpper < bristolLower) {
            //low red
            // console.log("confidence sig Better");
            clrs = colourScheme.sampleCircle.red;

        } else {//if(wardUpper > bristolLower){
            //average amber
            // console.log("confidence not sig different");

            clrs = colourScheme.sampleCircle.yellow;
        }


    } else if ((fileName === "/health_and_wellbeing/premature-mortality-all-causes-in-bristol.json") ||
        (fileName === "/health_and_wellbeing/premature-mortality-by-selected-cause-in-bristol.json")
    ) {
        //high is bad
        if (wardLower > bristolUpper) {
            //it is high red
            // console.log("confidence high");
            clrs = colourScheme.sampleCircle.red;

        } else if (wardUpper < bristolLower) {
            //low grenn
            // console.log("confidence sig better");
            clrs = colourScheme.sampleCircle.green;

        } else {
            //average amber
            // console.log("confidence not sig different");
            clrs = colourScheme.sampleCircle.yellow;

        }
    } else if ((fileName === "/health_and_wellbeing/year-6-children-age-10-11-years-with-excess-weight-in-bristol.json") ||
        (fileName === "/health_and_wellbeing/reception-children-with-excess-weight-in-bristol.json")
    ) {
        //high is bad
        if (wardLower > bristolUpper) {
            //it is high red
            clrs = colourScheme.sampleCircle.red;

        } else if (wardUpper < bristolLower) {
            //low grenn
            clrs = colourScheme.sampleCircle.green;

        } else {
            //average amber
            clrs = colourScheme.sampleCircle.yellow;

        }
    }

    return clrs;
}