function sampleBar(div,data, suppliedOptions){
      let defaultOptions ={ 
        tooltipDecimals: 1,
        bristolAverageDecimals: 1,
        isBristolAverageFromJsonIfAvailable: true,
        displayBristolBar: false,
        addedToYaxisUnits: '', // unused
        addedToMouseOverUnits: '',
      };
    let options = { ...defaultOptions, ...suppliedOptions };
  
    var margin = {top: 20, right: 20, bottom: 150, left: 25},
    width = 800 - margin.left - margin.right,
    height = 340 - margin.top - margin.bottom;
    
    // Parse the date / time
    data.sort(function(a, b) { return b.info - a.info; });
    var filteredData = data;
    if (!options.displayBristolBar) {
        filteredData = filteredData.filter( function (item, index, arr) {
            return !(item.ward == "Bristol" || item.ward == '.Bristol Average');
        });
    }
    var x = d3.scale.ordinal().rangeBands([0, width], .40);
    
    var y = d3.scale.linear().range([height, 0]);
    
    var xAxis = d3.svg.axis()
    .scale(x)
    .orient("bottom")
    
    var yAxis = d3.svg.axis()
    .scale(y)
    .orient("left")
    .ticks(10);
    
    var x2 = d3.scale.ordinal()
    .rangeBands([0,width],0);	
    
    var svg = d3.select(div).append("svg")
    
    .attr("width", "100%")
    .attr("height", height + (2*margin.top) + margin.bottom)
    //.attr("min-width","600px")
    .attr("viewBox","0 0 800 360")
    .attr("class", "svgs2 see hidden")
    .append("g")
    .attr("transform", 
          "translate(" + margin.left + "," + margin.top + ")");
          
    var tooltip = d3.select(div).append("div")   
        .attr("class", "tooltip")               
        .style("opacity", 0);
        

    x.domain(filteredData.map(function(d) { return d.ward; }));
    y.domain([0, d3.max(filteredData, function(d) { return +d.info; })]);
    
    svg.append("g")
      .attr("class", "x axis")
      .attr("transform", "translate(0," + height + ")")
      .call(xAxis)
    .selectAll("text")
      .style("text-anchor", "end")
      .attr("dx", "-.8em")
      .attr("dy", "-.55em")
      .attr("transform", "rotate(-90)" );
    
    svg.append("g")
      .attr("class", "y axis")
      .call(yAxis)
    .append("text")
      .attr("transform", "rotate(-90)")
      .attr("y", 6)
      .attr("dy", ".71em")
      .style("text-anchor", "end")
      
    svg.selectAll("bar")
      .data(filteredData)
    .enter().append("rect")
        .style("fill", function (d) {          // uses colourScheme.reusedColours.
          if (d.ward == ward) { return colourScheme.reusedColours.darkBlue}  // <== Add these
          else { return colourScheme.reusedColours.darkOrange }             // <== Add these
        ;})      
      .attr("x", function(d) { return x(d.ward); })
      .attr("width", x.rangeBand())
      .attr("y", function(d) { return y(d.info); })
      .attr("height", function(d) { return height - y(d.info); })
        
      
      
       .on("mouseover", function(d) {
            tooltip.text(d.ward + ", " + (+d.info).toFixed(options.tooltipDecimals) + options.addedToMouseOverUnits)
            .style("opacity", 0.8)
                    .style("left", (d3.event.pageX)+0 + "px") 
                    .style("top", (d3.event.pageY)-0 + "px");
        })
        
        
        .on("mouseout", function(d) {
            tooltip.style("opacity", 0);
    
        });
    
    
    
    var setIncludesBristol = false;
    var bristolAverage = 0;
    var sum = d3.sum(data, function (d) {
        if (d.ward == 'Bristol' || d.ward == '.Bristol Average') { setIncludesBristol = true; bristolAverage = d.info; return 0; }
        return d.info;
    }); 
    var average =  sum / (setIncludesBristol ? data.length-1:data.length) ;

    bristolAverage =
        correctDp(
            options.isBristolAverageFromJsonIfAvailable && setIncludesBristol
                ? bristolAverage
                : average
        , options.bristolAverageDecimals)
    
    var line = d3.svg.line()
        .x(function(d, i) { return x(d.ward) + i; })
        .y(function(d, i) { return y(bristolAverage); }); 
    
    svg.append("path")
        .datum(filteredData)
        .attr("class", "mean")
        .attr("d", line);
    
    svg.append("text")
        .attr("transform", "translate(" + (width+3) + "," + y(bristolAverage) + ")")
        .attr("dy", "-1em")
        .attr("text-anchor", "end")
        .attr("font-weight","bold")
        .attr("border-top","1px dotted "+colourScheme.reusedColours.red) // was #f00
        .style("fill", colourScheme.reusedColours.black)
        .html("Bristol Average: " + bristolAverage + options.addedToMouseOverUnits);
    
    };
    
    
    
    