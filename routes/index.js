var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  //res.render('index', { title: 'Ward Profile Tool',name: 'Dawid', condition: true});
  res.redirect('../wards/Ashley');
  //res.render('wards', { category: 'List of wards',wardName: " "});
  
});



/* GET users listing. */
router.get('/wards', function(req, res, next) {
  res.redirect('../wards/Ashley');
  // res.render('wards', { category: 'List of wards',wardName: " "});
});

router.get('/background_and_help', function(req, res, next) {
  res.render('further/background_and_help', { title: 'Background and Help'});
});

router.get('/mapping_tools', function(req, res, next) {
  res.render('further/mapping_tools', { title: 'Mapping Tools'});
});

router.get('/sources', function(req, res, next) {
  res.render('further/sources', { title: 'Sources'});
});



router.get('/ward_map', function(req, res, next) {
  res.render('further/ward_map', { title: 'Ward Map'});
});






module.exports = router;
