var express = require('express'), app=express();
var router = express.Router();


/* GET users listing. */
router.get('/', function(req, res, next) {
  res.render('henbury_and_brentry',{ wardName: 'henbury_and_brentry',category: ""});
});


router.get('/deprivation', function(req, res, next) {

  res.render('population/deprivation',{ wardName: 'henbury_and_brentry',category: 'deprivation',mainPTit : 'Indices of Multiple Deprivation 2019'});
});


router.get('/ethnicity', function(req, res, next) {

  res.render('population/ethnicity',{ wardName: 'henbury_and_brentry',category: 'ethnicity',mainPTit : '2011 CENSUS'});
});

router.get('/population_by_religion', function(req, res, next) {

  res.render('population/population_by_religion',{ wardName: 'henbury_and_brentry',category: 'population_by_religion',mainPTit :'2011 CENSUS'});
});

router.get('/population_by_country_of_birth_and_english_as_main_language', function(req, res, next) {

  res.render('population/population_by_country_of_birth_and_english_as_main_language',{ wardName: 'henbury_and_brentry',category: 'population_by_country_of_birth_and_english_as_main_language',mainPTit : '2011 CENSUS'});
});


router.get('/age_and_gender', function(req, res, next) {

  res.render('age_and_gender',{ wardName: 'henbury_and_brentry',category: 'age_and_gender',mainPTit : 'Office for National Statistics 2018'});
});

router.get('/education', function(req, res, next) {

  res.render('education',{ wardName: 'henbury_and_brentry',category: 'education',mainPTit : 'Bristol City Council / Dept for Education 2018 and 2019'});
});

router.get('/healthy_lifestyles', function(req, res, next) {

  res.render('healthy_lifestyles',{ wardName: 'henbury_and_brentry',category: 'healthy_lifestyles',mainPTit : 'Quality of Life Survey 2018-19'});
});

router.get('/premature_mortality', function(req, res, next) {

  res.render('premature_mortality',{ wardName: 'henbury_and_brentry',category: 'premature_mortality',mainPTit : 'Public Health 2015-17'});
});

router.get('/social_care', function(req, res, next) {

  res.render('social_care',{ wardName: 'henbury_and_brentry',category: 'social_care',mainPTit : 'Adult Social Care 2019'});
});

router.get('/crime', function(req, res, next) {

  res.render('crime',{ wardName: 'henbury_and_brentry',category: 'crime',mainPTit : 'Crime and Policing 2018-19'});
});

router.get('/child_poverty', function(req, res, next) {

  res.render('child_poverty',{ wardName: 'henbury_and_brentry',category: 'child_poverty',mainPTit : 'Children living in low income families 2016',subText : "This is the percentage of children living in low income families, mapped by small areas (called “Lower super output areas” or LSOAs), with the new Bristol ward boundaries added on top.  This data is not currently available by the 2016 Bristol wards"});
});

router.get('/housing', function(req, res, next) {

  res.render('housing',{ wardName: 'henbury_and_brentry',category: 'housing',mainPTit : '2011 CENSUS'});
});


router.get('/life_expectancy', function(req, res, next) {

  res.render('health/life_expectancy',{ wardName: 'henbury_and_brentry',category: 'life_expectancy',mainPTit : 'Public Health 2015-2017'});
});

router.get('/household_size', function(req, res, next) {

  res.render('housing/household_size',{ wardName: 'henbury_and_brentry',category: 'household_size',mainPTit : '2011 CENSUS'});
});

router.get('/car_availability', function(req, res, next) {

  res.render('car_availability',{ wardName: 'henbury_and_brentry',category: 'car_availability',mainPTit : '2011 CENSUS'});
});


router.get('/quality_of_life1', function(req, res, next) {

  res.render('q_of_life/quality_of_life1',{ wardName: 'henbury_and_brentry',category: 'quality_of_life1', title: 'Quality of Life 1', mainPTit : 'Quality of Life Survey 2018-19'});
});

router.get('/quality_of_life2', function(req, res, next) {

  res.render('q_of_life/quality_of_life2',{ wardName: 'henbury_and_brentry',category: 'quality_of_life2', title: 'Quality of Life 2', mainPTit : 'Quality of Life Survey 2018-19'});
});

router.get('/quality_of_life3', function(req, res, next) {

  res.render('q_of_life/quality_of_life3',{ wardName: 'henbury_and_brentry',category: 'quality_of_life3', title: 'Quality of Life 3', mainPTit : 'Quality of Life Survey 2018-19'});
});


module.exports = router;
