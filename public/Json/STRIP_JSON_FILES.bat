@echo off
REM If you are reading this, you probably need to install jq first, see here: https://github.com/stedolan/jq/wiki/Installation
REM You may need to get IT Support to install this, tell them you need them to install jq to manipulate large json files.
for /f "tokens=*" %%G in ('dir ".\*.json" /b /s') do (
    echo Found %%G 
    del %%G.tmp
    move %%G %%G.tmp
    jq -r "walk(if type == \"object\" and ( has(\"geo_shape\") or has(\"geometry\") ) then del(.geo_shape , .geometry) else . end)" %%G.tmp > %%G
)