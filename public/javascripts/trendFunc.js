"use strict";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function trend(fileName, indicatorName, sign, timePeriods, suppliedOptions) {
  var defaultOptions = {
    decimals: 1,
    displayBristolBar: false
  };

  var options = _objectSpread({}, defaultOptions, {}, suppliedOptions);

  console.log("trend : " + options.decimals + "dp");
  var testing = changeName(ward, sign);
  ward = testing;
  $.getJSON("../../Json/" + fileName, function (data) {
    var wardValues = {},
        bristolValues = {};
    /*For trend Line*/

    var trendYears2 = [];
    $.each(data, function (i, obj) {
      var indicator = data[i].fields.indicator;
      var wardN = data[i].fields.ward_name;
      var dataz = +data[i].fields.statistic;
      var date = data[i].fields.year;

      if (indicatorName == indicator) {
        if (wardN === ward) {
          wardValues[date] = (Object.prototype.hasOwnProperty.call(wardValues, date) ? wardValues[date] : 0) + dataz;
        } else if (wardN === ".Bristol Average") {
          bristolValues[date] = (Object.prototype.hasOwnProperty.call(bristolValues, date) ? bristolValues[date] : 0) + dataz;
        }
      }
    }); //json
    // trend years need to be ascending so sorted first then datapoints collected in order.

    timePeriods.sort().forEach(function (yearElement) {
      if (true) {
        //!isNaN(wardValues[yearElement]) && !isNaN(bristolValues[yearElement])) {
        // years field must be a numerical value for d3's Linear scale, if ordinal then should be unaffected.
        var sanitisedLabel = stripAnythingAfterDash(stripAnythingAfterSlash(yearElement));
        trendYears2.push({
          "year": parseInt(sanitisedLabel),
          "ward": wardValues[yearElement],
          "bristol": bristolValues[yearElement],
          "label": yearElement
        });
      }
    });

    if (trendYears2.length - countFullMissingYears(trendYears2) <= 1) {
      $("#Trend").hide();
      console.log("one trend year, hiding trend graph");
    } else {
      $("#Trend").show();
    }

    trendYears2.forEach(function (d) {
      d.year = +d.year;
      d.ward = +d.ward;
      d.bristol = +d.bristol;
    });
    var chart = makeLineChart(trendYears2, 'year', {
      'Ward': {
        column: 'ward'
      },
      'Bristol': {
        column: 'bristol'
      }
    }, {
      xAxis: 'Years',
      yAxis: 'Amount'
    }, trendYears2.length);
    chart.bind("#chart-line1");
    chart.render();
  }); //end of json
} //end of func