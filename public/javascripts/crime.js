"use strict";

/*
Crime Category Page

On this page each function is called when the option is changed.


*/
$(function () {
  $('#crimeOptions').on('change', function () {
    //when you select a option delte the visuals
    $(".svgs").remove();
    $(".svgs2").remove();
    $(".svgs3").remove(); //select the text you clicked and display that tile on the screen

    var selectedText = $("#crimeOptions option:selected").html(); // add & 

    selectedText = makeAMP(selectedText);
    $(".labelIndicator").text(selectedText);
    $(".subtit").html("Crime and Policing 2018-19");

    if (this.value == 'allC') {
      sampleFunc("/crime/crime-recorded-by-police-by-selected-offence-groups-in-bristol-by-ward.json", "all_crimes_rate_per_1000_ward_population", "year", ["2018/19", "2017/18", "2016/17"], "&");
      $(".labelIndicator").text(selectedText + " (offence rate per 1,000 population) ");
    } else if (this.value == 'burglary') {
      sampleFunc("/crime/crime-recorded-by-police-by-selected-offence-groups-in-bristol-by-ward.json", "burglary_rate_per_1000_ward_population", "year", ["2018/19", "2017/18", "2016/17"], "&");
      $(".labelIndicator").text(selectedText + " (offence rate per 1,000 population) ");
    } else if (this.value == 'violent') {
      sampleFunc("/crime/crime-recorded-by-police-by-selected-offence-groups-in-bristol-by-ward.json", "violent_sexual_offences_rate_per_1000_ward_population", "year", ["2018/19", "2017/18", "2016/17"], "&");
      $(".labelIndicator").text(selectedText + " (offence rate per 1,000 population) ");
    } else if (this.value == 'anti') {
      sampleFunc("/crime/asb-incidents-reported-to-the-police-in-bristol.json", "total_anti_social_behaviour_incidents_rate_per_1000_ward_population", "year", ["2018/19", "2017/18", "2016/17"], "&");
      $(".labelIndicator").text(selectedText + " (offence rate per 1,000 population) ");
    } else if (this.value == 'yOffences') {
      sampleFunc("/crime/youth-offender-aged-10-17-years-in-bristol-by-ward.json", "offenders_per_ward_rate_per_1_000", "time_period", ["2018/19", "2017/18", "2016/17"], "and");
      $(".labelIndicator").text(selectedText + " Offenders (rate of offenders per 1,000 of 10 -17 year olds) ");
    } else if (this.value == 'anti-social') {
      sampleFuncForQofL("/q_of_life/qol-indicator-data-rag-ratings.json", "% who feel anti-social behaviour is a problem locally", "&");
      trend("/q_of_life/qol-indicators-trend-ward-profile-2019.json", "% who feel anti-social behaviour is a problem locally", "&", ["2018", "2017", "2015"]);
      $(".subtit").html("Quality of Life Survey 2018-19");
    } else if (this.value == 'safe') {
      sampleFuncForQofL("/q_of_life/qol-indicator-data-rag-ratings.json", "% who feel safe outdoors after dark", "&");
      trend("/q_of_life/qol-indicators-trend-ward-profile-2019.json", "% who feel safe outdoors after dark", "&", ["2018", "2017", "2015"]);
      $(".subtit").html("Quality of Life Survey 2018-19");
    } else if (this.value == 'victim') {
      sampleFuncForQofL("/q_of_life/qol-indicator-data-rag-ratings.json", "% who have been a victim of crime in the last 12 months", "&");
      trend("/q_of_life/qol-indicators-trend-ward-profile-2019.json", "% who have been a victim of crime in the last 12 months", "&", ["2018", "2017", "2015"]);
      $(".subtit").html("Quality of Life Survey 2018-19");
    } else {
      console.log("Indicator not found:", this.value);
    } //remove visibillity


    $(".compare").removeClass("see");
  }); //end of if statement
  //select the first option automatically 

  $('#crimeOptions').val("allC").trigger('change');
}); //end of script