"use strict";

$(function () {
  var housing = ["% satisfied overall with their current accommodation", "% satisfied with the state of repair of their home", "% satisfied with the cost of their rent or mortgage payments"];
  var sastainabillity_and_enviroment = ["% who think street litter is a problem locally", "% satisfied with the general household waste service", "% satisfied with the quality of parks and green spaces", "% who visit Bristol's parks and green spaces at least once a week", "% satisfied with the recycling service", "% concerned about climate change", "% who have changed the way they travel due to climate change concerns", "% who have reduced their household waste due to climate change concerns", "% who have reduced energy use at home due to climate change concerns"];
  var culture_and_leisure = ["% satisfied with the range and quality of outdoor events", "% satisfied with activities for children/young people", "% who participate in cultural activities at least once a month", "% satisfied with libraries", "% satisfied with leisure facilities/services"];
  ql(housing, sastainabillity_and_enviroment, culture_and_leisure);
});