"use strict";

$(function () {
  function car_availability(fileName, sign, yr) {
    var barJson = [];
    $.getJSON("../../Json/" + fileName, function (data) {
      var testing = changeName(ward, sign);
      ward = testing;
      var tempWardArr = [];
      var tempWardArrNr = [];
      var tempWardTotals = [];
      var tempWardAvgandTot = [];
      var bristolTotalCars = 0;
      var bristolAllHouses = 0;
      var bristolAvgCars = 0;
      var brs1Car = 0;
      var brs2Car = 0;
      var brs3Car = 0;
      var brs4Plus = 0;
      var brsNoCar = 0;
      var wardCount = 0;
      var countWard = 0;
      var eachWardNr = [];
      var eachWardNr2 = [];
      var eachWardNr3 = [];
      var eachWardNr4 = [];
      var eachWardNr5 = [];
      var eachWardNr6 = [];
      var eachWardNr7 = [];
      var mean = 0;
      var mean2 = 0;
      var mean3 = 0;
      var mean4 = 0;
      var mean5 = 0;
      var mean6 = 0;
      var mean7 = 0;
      $.each(data, function (i, obj) {
        var wardN = data[i].fields["2016_ward_name"];
        var avgCars = data[i].fields.average_number_of_cars_per_household;
        var numbofCarsinArea = data[i].fields.number_of_cars_or_vans_in_the_area;
        var wardz1CarNr = data[i].fields["1_car_or_van_in_household"];
        var wardz2CarsoNr = data[i].fields["2_cars_or_vans_in_household"];
        var wardz3CarsNr = data[i].fields["3_cars_or_vans_in_household"];
        var wardz4PlusCarsNr = data[i].fields["4_or_more_cars_or_vans_in_household"];
        var wardzNoCarNr = data[i].fields.no_cars_or_vans_in_household;
        var allHouses = data[i].fields.all_households;
        var wardz1Car = wardz1CarNr / allHouses * 100;
        var wardz2Carso = wardz2CarsoNr / allHouses * 100;
        var wardz3Cars = wardz3CarsNr / allHouses * 100;
        var wardz4PlusCars = wardz4PlusCarsNr / allHouses * 100;
        var wardzNoCar = wardzNoCarNr / allHouses * 100; // Accurate calculation instead of using ODS average.

        avgCars = numbofCarsinArea / allHouses;
        bristolTotalCars = bristolTotalCars + numbofCarsinArea;
        bristolAvgCars = bristolAvgCars + avgCars;
        brs1Car = brs1Car + wardz1CarNr;
        brs2Car = brs2Car + wardz2CarsoNr;
        brs3Car = brs3Car + wardz3CarsNr;
        brs4Plus = brs4Plus + wardz4PlusCarsNr;
        brsNoCar = brsNoCar + wardzNoCarNr;
        bristolAllHouses = bristolAllHouses + allHouses;
        var feed = new Object();
        feed.ward = wardN;
        feed.info = avgCars;
        barJson.push(feed);

        if (wardN !== "Bristol" && wardN !== "England & Wales") {
          mean = mean + wardzNoCar;
          mean2 = mean2 + wardz1Car;
          mean3 = mean3 + wardz2Carso;
          mean4 = mean4 + wardz3Cars;
          mean5 = mean5 + wardz4PlusCars;
          mean6 = mean6 + avgCars;
          mean7 = mean7 + numbofCarsinArea;
          countWard++;
          eachWardNr.push(wardzNoCar);
          eachWardNr2.push(wardz1Car);
          eachWardNr3.push(wardz2Carso);
          eachWardNr4.push(wardz3Cars);
          eachWardNr5.push(wardz4PlusCars);
          eachWardNr6.push(avgCars);
          eachWardNr7.push(numbofCarsinArea);
        }

        if (wardN == ward) {
          tempWardArr.push(wardzNoCar, wardz1Car, wardz2Carso, wardz3Cars, wardz4PlusCars);
          tempWardArrNr.push(wardzNoCarNr, wardz1CarNr, wardz2CarsoNr, wardz3CarsNr, wardz4PlusCarsNr);
          tempWardTotals.push(numberWithCommas(numbofCarsinArea), avgCars);
          tempWardAvgandTot.push(avgCars, oneDcm(numbofCarsinArea / bristolTotalCars * 100));
        }

        wardCount++;
      });
      var brs1CarAvg = oneDcm(brs1Car / bristolAllHouses * 100);
      var brs2CarAvg = oneDcm(brs2Car / bristolAllHouses * 100);
      var brs3CarAvg = oneDcm(brs3Car / bristolAllHouses * 100);
      var brs4PlusAvg = oneDcm(brs4Plus / bristolAllHouses * 100);
      var brsNoCarAvg = oneDcm(brsNoCar / bristolAllHouses * 100);
      var brsAvgCars = to15sf(bristolTotalCars / bristolAllHouses);
      bristolTotalCars = numberWithCommas(bristolTotalCars);
      barJson.push({
        ward: "Bristol",
        info: brsAvgCars
      });
      $(".noCar").find(".wardHolder").find(".textC").append("<p>" + numberWithCommas(tempWardArrNr[0]) + " Households</p>");
      $(".oneCar").find(".wardHolder").find(".textC").append("<p>" + numberWithCommas(tempWardArrNr[1]) + " Households</p>");
      $(".twoCars").find(".wardHolder").find(".textC").append("<p>" + numberWithCommas(tempWardArrNr[2]) + " Households</p>");
      $(".threeCars").find(".wardHolder").find(".textC").append("<p>" + numberWithCommas(tempWardArrNr[3]) + " Households</p>");
      $(".fourPlus").find(".wardHolder").find(".textC").append("<p>" + numberWithCommas(tempWardArrNr[4]) + " Households</p>");
      $(".carData").find(".totData").find(".wrd").append(tempWardTotals[0]);
      $(".carData").find(".avgData").find(".wrd").append(tempWardTotals[1].toFixed(2));
      $(".noCar").find(".brsHolder").find(".textC").append("<p>" + numberWithCommas(brsNoCar) + " Households</p>");
      $(".oneCar").find(".brsHolder").find(".textC").append("<p>" + numberWithCommas(brs1Car) + " Households</p>");
      $(".twoCars").find(".brsHolder").find(".textC").append("<p>" + numberWithCommas(brs2Car) + " Households</p>");
      $(".threeCars").find(".brsHolder").find(".textC").append("<p>" + numberWithCommas(brs3Car) + " Households</p>");
      $(".fourPlus").find(".brsHolder").find(".textC").append("<p>" + numberWithCommas(brs4Plus) + " Households</p>");
      $(".carData").find(".totData").find(".brs").append(bristolTotalCars);
      $(".carData").find(".avgData").find(".brs").append(correctDp(brsAvgCars, 2));
      var standard = standardDeviation(mean, countWard, eachWardNr);
      var standard2 = standardDeviation(mean2, countWard, eachWardNr2);
      var standard3 = standardDeviation(mean3, countWard, eachWardNr3);
      var standard4 = standardDeviation(mean4, countWard, eachWardNr4);
      var standard5 = standardDeviation(mean5, countWard, eachWardNr5);
      var standard6 = standardDeviation(mean6, countWard, eachWardNr6);
      var standard7 = standardDeviation(mean7, countWard, eachWardNr7);
      var clrs = colStd(fileName, tempWardArr[0], brsNoCarAvg, standard);
      var clrs2 = colStd(fileName, tempWardArr[1], brs1CarAvg, standard2);
      var clrs3 = colStd(fileName, tempWardArr[2], brs2CarAvg, standard3);
      var clrs4 = colStd(fileName, tempWardArr[3], brs3CarAvg, standard4);
      var clrs5 = colStd(fileName, tempWardArr[4], brs4PlusAvg, standard5);
      var clrs6 = colStd(fileName, tempWardAvgandTot[0], brsAvgCars, standard6);
      bristolTotalCars = bristolTotalCars.replace(/,/g, '');
      var wardTotalCarsDecimal = tempWardTotals[0].replace(/,/g, '');
      var bristolTotalDivided = parseInt(bristolTotalCars) / wardCount;
      var clrs7 = colStd(fileName, wardTotalCarsDecimal, bristolTotalDivided, standard7);
      var clrArray = [clrs, clrs2, clrs3, clrs4, clrs5, clrs6, clrs7];
      $(".carData").find(".totData").find(".wrd").addClass(clrArray[6]);
      $(".carData").find(".avgData").find(".wrd").addClass(clrArray[5]);
      donut2(oneDcm(tempWardArr[0]), ".wardNone", clrArray[0]);
      donut2(oneDcm(tempWardArr[1]), ".ward1", clrArray[1]);
      donut2(oneDcm(tempWardArr[2]), ".ward2", clrArray[2]);
      donut2(oneDcm(tempWardArr[3]), ".ward3", clrArray[3]);
      donut2(oneDcm(tempWardArr[4]), ".ward4", clrArray[4]);
      donut2(brsNoCarAvg, ".brsNone", colourScheme.reusedColours.grey);
      donut2(brs1CarAvg, ".brs1", colourScheme.reusedColours.grey);
      donut2(brs2CarAvg, ".brs2", colourScheme.reusedColours.grey);
      donut2(brs3CarAvg, ".brs3", colourScheme.reusedColours.grey);
      donut2(brs4PlusAvg, ".brs4", colourScheme.reusedColours.grey);
      var divderHeight = $(".divider").find(".wardHolder").height();
      $(".miniDiv").find(".wrDiv").height(divderHeight);
      $(".miniDiv").find(".brDiv").height(divderHeight);
      sampleBar(".carBar", barJson, {
        tooltipDecimals: 2,
        bristolAverageDecimals: 2,
        isBristolAverageFromJsonIfAvailable: true,
        displayBristolBar: false
      });
    });
  }

  ;
  car_availability("/housing/car-availability.json", "&");
});