"use strict";

$(function () {
  function religion(fileName, sign) {
    $.getJSON("../../Json/" + fileName, function (data) {
      var testing = changeName(ward, sign);
      ward = testing;
      var tempBrs = [];
      var tempWard = [];
      var relH = [];
      var barJson = [];
      var buddhistBrs = 0;
      var christianBrs = 0;
      var hinduBrs = 0;
      var jewishBrs = 0;
      var muslimBrs = 0;
      var no_religionBrs = 0;
      var other_religionBrs = 0;
      var religion_not_statedBrs = 0;
      var sikhBrs = 0;
      var totPopBrs = 0;
      var total_ward_religion = 0;
      var countWard = 0;
      var eachWardNr = [];
      var eachWardNr2 = [];
      var eachWardNr3 = [];
      var eachWardNr4 = [];
      var eachWardNr5 = [];
      var eachWardNr6 = [];
      var eachWardNr7 = [];
      var eachWardNr8 = [];
      var eachWardNr9 = [];
      var mean = 0;
      var mean2 = 0;
      var mean3 = 0;
      var mean4 = 0;
      var mean5 = 0;
      var mean6 = 0;
      var mean7 = 0;
      var mean8 = 0;
      var mean9 = 0;
      $.each(data, function (i, obj) {
        var buddhist = data[i].fields.buddhist;
        var christian = data[i].fields.christian;
        var hindu = data[i].fields.hindu;
        var jewish = data[i].fields.jewish;
        var muslim = data[i].fields.muslim;
        var wardN = data[i].fields.name;
        var no_religion = data[i].fields.no_religion;
        var other_religion = data[i].fields.other_religions;
        var religion_not_stated = data[i].fields.religion_not_stated;
        var sikh = data[i].fields.sikh;
        var totPop = data[i].fields.total_population;
        buddhistBrs = buddhistBrs + buddhist;
        christianBrs = christianBrs + christian;
        hinduBrs = hinduBrs + hindu;
        jewishBrs = jewishBrs + jewish;
        muslimBrs = muslimBrs + muslim;
        no_religionBrs = no_religionBrs + no_religion;
        other_religionBrs = other_religionBrs + other_religion;
        religion_not_statedBrs = religion_not_statedBrs + religion_not_stated;
        sikhBrs = sikhBrs + sikh;
        totPopBrs = totPopBrs + totPop;
        total_ward_religion = buddhist + christian + hindu + jewish + muslim + sikh + other_religion;
        total_ward_religion = to15sf(total_ward_religion / totPop * 100);
        var feed = new Object();
        feed.ward = wardN;
        feed.info = total_ward_religion;
        barJson.push(feed);

        if (wardN == ward) {
          var buddhistProc = to15sf(buddhist / totPop * 100);
          var christianProc = to15sf(christian / totPop * 100);
          var hinduProc = to15sf(hindu / totPop * 100);
          var jewishProc = to15sf(jewish / totPop * 100);
          var muslimProc = to15sf(muslim / totPop * 100);
          var no_religionProc = to15sf(no_religion / totPop * 100);
          var other_religionProc = to15sf(other_religion / totPop * 100);
          var religion_not_statedProc = to15sf(religion_not_stated / totPop * 100);
          var sikhProc = to15sf(sikh / totPop * 100);
          tempWard.push(christianProc, buddhistProc, hinduProc, jewishProc, muslimProc, sikhProc, other_religionProc, no_religionProc, religion_not_statedProc);
        }

        if (wardN !== "Bristol" && wardN !== "England & Wales") {
          mean = mean + to15sf(christian / totPop * 100);
          mean2 = mean2 + to15sf(buddhist / totPop * 100);
          mean3 = mean3 + to15sf(hindu / totPop * 100);
          mean4 = mean4 + to15sf(jewish / totPop * 100);
          mean5 = mean5 + to15sf(muslim / totPop * 100);
          mean6 = mean6 + to15sf(sikh / totPop * 100);
          mean7 = mean7 + to15sf(other_religion / totPop * 100);
          mean8 = mean8 + to15sf(no_religion / totPop * 100);
          mean9 = mean9 + to15sf(religion_not_stated / totPop * 100);
          countWard++;
          eachWardNr.push(to15sf(christian / totPop * 100));
          eachWardNr2.push(to15sf(buddhist / totPop * 100));
          eachWardNr3.push(to15sf(hindu / totPop * 100));
          eachWardNr4.push(to15sf(jewish / totPop * 100));
          eachWardNr5.push(to15sf(muslim / totPop * 100));
          eachWardNr6.push(to15sf(sikh / totPop * 100));
          eachWardNr7.push(to15sf(other_religion / totPop * 100));
          eachWardNr8.push(to15sf(no_religion / totPop * 100));
          eachWardNr9.push(to15sf(religion_not_stated / totPop * 100));
        }
      });
      var buddhistProcBrs = buddhistBrs / totPopBrs * 100;
      var christianProcBrs = christianBrs / totPopBrs * 100;
      var hinduProcBrs = hinduBrs / totPopBrs * 100;
      var jewishProcBrs = jewishBrs / totPopBrs * 100;
      var muslimProcBrs = muslimBrs / totPopBrs * 100;
      var no_religionProcBrs = no_religionBrs / totPopBrs * 100;
      var other_religionProcBrs = other_religionBrs / totPopBrs * 100;
      var religion_not_statedProcBrs = religion_not_statedBrs / totPopBrs * 100;
      var sikhProcBrs = sikhBrs / totPopBrs * 100;
      tempBrs.push(christianProcBrs, buddhistProcBrs, hinduProcBrs, jewishProcBrs, muslimProcBrs, sikhProcBrs, other_religionProcBrs, no_religionProcBrs, religion_not_statedProcBrs);
      var standard = standardDeviation(mean, countWard, eachWardNr);
      var standard2 = standardDeviation(mean2, countWard, eachWardNr2);
      var standard3 = standardDeviation(mean3, countWard, eachWardNr3);
      var standard4 = standardDeviation(mean4, countWard, eachWardNr4);
      var standard5 = standardDeviation(mean5, countWard, eachWardNr5);
      var standard6 = standardDeviation(mean6, countWard, eachWardNr6);
      var standard7 = standardDeviation(mean7, countWard, eachWardNr7);
      var standard8 = standardDeviation(mean8, countWard, eachWardNr8);
      var standard9 = standardDeviation(mean9, countWard, eachWardNr9);
      var clrs = colStd(fileName, tempWard[0], tempBrs[0], standard);
      var clrs2 = colStd(fileName, tempWard[1], tempBrs[1], standard2);
      var clrs3 = colStd(fileName, tempWard[2], tempBrs[2], standard3);
      var clrs4 = colStd(fileName, tempWard[3], tempBrs[3], standard4);
      var clrs5 = colStd(fileName, tempWard[4], tempBrs[4], standard5);
      var clrs6 = colStd(fileName, tempWard[5], tempBrs[5], standard6);
      var clrs7 = colStd(fileName, tempWard[6], tempBrs[6], standard7);
      var clrs8 = colStd(fileName, tempWard[7], tempBrs[7], standard8);
      var clrs9 = colStd(fileName, tempWard[8], tempBrs[8], standard9);
      var clrArray = [clrs, clrs2, clrs3, clrs4, clrs5, clrs6, clrs7, clrs8, clrs9];
      var religionObj = new Object();
      religionObj.group = "Christian";
      religionObj.currWard = tempWard[0];
      religionObj.bristol = tempBrs[0];
      relH.push(religionObj);
      var religionObj2 = new Object();
      religionObj2.group = "Buddhist";
      religionObj2.currWard = tempWard[1];
      religionObj2.bristol = tempBrs[1];
      relH.push(religionObj2);
      var religionObj3 = new Object();
      religionObj3.group = "Hindu";
      religionObj3.currWard = tempWard[2];
      religionObj3.bristol = tempBrs[2];
      relH.push(religionObj3);
      var religionObj4 = new Object();
      religionObj4.group = "Jewish";
      religionObj4.currWard = tempWard[3];
      religionObj4.bristol = tempBrs[3];
      relH.push(religionObj4);
      var religionObj5 = new Object();
      religionObj5.group = "Muslim";
      religionObj5.currWard = tempWard[4];
      religionObj5.bristol = tempBrs[4];
      relH.push(religionObj5);
      var religionObj6 = new Object();
      religionObj6.group = "Sikh";
      religionObj6.currWard = tempWard[5];
      religionObj6.bristol = tempBrs[5];
      relH.push(religionObj6);
      var religionObj7 = new Object();
      religionObj7.group = "Other religions";
      religionObj7.currWard = tempWard[6];
      religionObj7.bristol = tempBrs[6];
      relH.push(religionObj7);
      var religionObj8 = new Object();
      religionObj8.group = "No religion";
      religionObj8.currWard = tempWard[7];
      religionObj8.bristol = tempBrs[7];
      relH.push(religionObj8);
      var religionObj9 = new Object();
      religionObj9.group = "Religion not stated";
      religionObj9.currWard = tempWard[8];
      religionObj9.bristol = tempBrs[8];
      relH.push(religionObj9);

      for (var i = 0; i < 5; i++) {
        $(".relH").find(".relNumb").append("<div class='ethField'><span class='dotz " + clrArray[i] + "'></span><div class='wardPart'><div class='wardProc'><div class='procNr'>" + oneDcm(relH[i].currWard) + "%</div></div></div><div class='brsPart'><div class='brsProc'><div class='procNr'>" + oneDcm(relH[i].bristol) + "%</div></div></div><p class='ethText'>" + relH[i].group + "</p></div>");
      }

      for (var i = 5; i < 9; i++) {
        $(".relH2").find(".relNumb").append("<div class='ethField'><span class='dotz " + clrArray[i] + "'></span><div class='wardPart'><div class='wardProc'><div class='procNr'>" + oneDcm(relH[i].currWard) + "%</div></div></div><div class='brsPart'><div class='brsProc'><div class='procNr'>" + oneDcm(relH[i].bristol) + "%</div></div></div><p class='ethText'>" + relH[i].group + "</p></div>");
      }

      $(".relH").find(".relNumb").prepend("<div class='ethField'><div class='wardPart'><div class='first'>" + ward + "</div></div><div class='brsPart'><div class='first'>Bristol</div></div></div>");
      $(".relH2").find(".relNumb").prepend("<div class='ethField'><div class='wardPart'><div class='first'>" + ward + "</div></div><div class='brsPart'><div class='first'>Bristol</div></div></div>");
      sampleBar(".relBar", barJson);
    });
  }

  ; //end of func

  religion("/population/religion-2016-ward.json", "&");
});