"use strict";

$(function () {
  function household_size(fileName, sign, yr) {
    $.getJSON("../../Json/" + fileName, function (data) {
      var testing = changeName(ward, sign);
      ward = testing;
      var barJson = [];
      var brsHolder = [];
      var wardHolder = [];
      var wardOne = 0;
      var wardTwo = 0;
      var wardThree = 0;
      var wardFour = 0;
      var wardAvgBed = 0;
      var wardAvgPer = 0;
      var wardOver = 0;
      var brsOne = 0;
      var brsTwo = 0;
      var brsThree = 0;
      var brsFour = 0;
      var brsTot = 0;
      var brsAvgBed = 0;
      var brsAvgPer = 0;
      var brsOver = 0;
      var overall = 0;
      var wardCount = 0;
      var countWard = 0;
      var eachWardNr = [];
      var eachWardNr2 = [];
      var eachWardNr3 = [];
      var eachWardNr4 = [];
      var eachWardNr5 = [];
      var eachWardNr6 = [];
      var mean = 0;
      var mean2 = 0;
      var mean3 = 0;
      var mean4 = 0;
      var mean5 = 0;
      var mean6 = 0;
      $.each(data, function (i, obj) {
        wardCount++;
        var wardN = data[i].fields["2016_ward_name"];
        var avgBedperHouse = data[i].fields.average_number_of_bedrooms_per_household;
        var avgPersonperHouse = data[i].fields.average_household_size_persons_per_household;
        var total = data[i].fields.total_number_of_households;
        var oneBedNr = data[i].fields["1_bedroom_household"];
        var twoBedNr = data[i].fields["2_bedroom_household"];
        var threeBedNr = data[i].fields["3_bedroom_household"];
        var fourBedNr = data[i].fields["4_or_more_bedroom_household"];
        var overCrwdNr = data[i].fields["overcrowded_households_ie_occupancy_rating_bedrooms_of_1_or_less"];
        var oneBed = oneBedNr / total * 100;
        var twoBed = twoBedNr / total * 100;
        var threeBed = threeBedNr / total * 100;
        var fourBed = fourBedNr / total * 100;
        var overCrowded = overCrwdNr / total * 100;
        brsOne = brsOne + oneBedNr;
        brsTwo = brsTwo + twoBedNr;
        brsThree = brsThree + threeBedNr;
        brsFour = brsFour + fourBedNr;
        brsTot = brsTot + total;
        brsAvgBed = brsAvgBed + avgBedperHouse;
        brsAvgPer = brsAvgPer + avgPersonperHouse;
        brsOver = brsOver + overCrwdNr;
        var feed = new Object();
        feed.ward = wardN;
        feed.info = overCrowded;
        barJson.push(feed);

        if (wardN == ward) {
          overall = total;
          wardOne = oneBed;
          wardTwo = twoBed;
          wardThree = threeBed;
          wardFour = fourBed;
          wardAvgBed = avgBedperHouse;
          wardAvgPer = avgPersonperHouse;
          wardOver = overCrowded;
        }

        if (wardN !== "Bristol" && wardN !== "England & Wales") {
          mean = mean + oneBed;
          mean2 = mean2 + twoBed;
          mean3 = mean3 + to15sf(threeBed + fourBed);
          mean4 = mean4 + overCrowded;
          mean5 = mean5 + avgBedperHouse;
          mean6 = mean6 + avgPersonperHouse;
          countWard++;
          eachWardNr.push(oneBed);
          eachWardNr2.push(twoBed);
          eachWardNr3.push(to15sf(threeBed + fourBed));
          eachWardNr4.push(overCrowded);
          eachWardNr5.push(avgBedperHouse);
          eachWardNr6.push(avgPersonperHouse);
        }
      });
      var wardThreePlus = to15sf(wardThree + wardFour);
      var brsOneBedPrc = to15sf(brsOne / brsTot * 100);
      var brsTwoBedPrc = to15sf(brsTwo / brsTot * 100);
      var brsThreeBedPrc = to15sf(brsThree / brsTot * 100);
      var brsFourBedPrc = to15sf(brsFour / brsTot * 100);
      var brsAvgbedPro = to15sf(brsAvgBed / wardCount);
      var brsAvgperPro = to15sf(brsAvgPer / wardCount);
      var brsAvgoverPro = to15sf(brsOver / brsTot * 100);
      barJson.push({
        ward: "Bristol",
        info: brsAvgoverPro
      });
      var brsThreePlus = to15sf(brsThreeBedPrc + brsFourBedPrc);
      wardHolder.push(wardOne, wardTwo, wardThreePlus, wardOver, wardAvgBed, wardAvgPer);
      brsHolder.push(brsOneBedPrc, brsTwoBedPrc, brsThreePlus, brsAvgoverPro, brsAvgbedPro, brsAvgperPro);
      var standard = standardDeviation(mean, countWard, eachWardNr);
      var standard2 = standardDeviation(mean2, countWard, eachWardNr2);
      var standard3 = standardDeviation(mean3, countWard, eachWardNr3);
      var standard4 = standardDeviation(mean4, countWard, eachWardNr4);
      var standard5 = standardDeviation(mean5, countWard, eachWardNr5);
      var standard6 = standardDeviation(mean6, countWard, eachWardNr6);
      var clrs = colStd(fileName, wardHolder[0], brsHolder[0], standard);
      var clrs2 = colStd(fileName, wardHolder[1], brsHolder[1], standard2);
      var clrs3 = colStd(fileName, wardHolder[2], brsHolder[2], standard3);
      var clrs4 = colStd(fileName, wardHolder[3], brsHolder[3], standard4);
      var clrs5 = colStd(fileName, wardHolder[4], brsHolder[4], standard5);
      var clrs6 = colStd(fileName, wardHolder[5], brsHolder[5], standard6);
      var clrArray = [clrs, clrs2, clrs3, clrs4, clrs5, clrs6];
      donut(oneDcm(wardHolder[0]), ".oneBed .wrC", clrArray[0]);
      donut(oneDcm(wardHolder[1]), ".twoBed .wrC", clrArray[1]);
      donut(oneDcm(wardHolder[2]), ".threePlus .wrC", clrArray[2]);
      donut(oneDcm(wardHolder[3]), ".over .wrC", clrArray[3]);
      donut3(oneDcm(wardHolder[4]), ".abgBedPerH .wrC", clrArray[4]);
      donut3(oneDcm(wardHolder[5]), ".avgHSize .wrC", clrArray[5]);
      donut(oneDcm(brsHolder[0]), ".oneBed .brC", colourScheme.reusedColours.grey);
      donut(oneDcm(brsHolder[1]), ".twoBed .brC", colourScheme.reusedColours.grey);
      donut(oneDcm(brsHolder[2]), ".threePlus .brC", colourScheme.reusedColours.grey);
      donut(oneDcm(brsHolder[3]), ".over .brC", colourScheme.reusedColours.grey);
      donut3(oneDcm(brsHolder[4]), ".abgBedPerH .brC", colourScheme.reusedColours.grey);
      donut3(oneDcm(brsHolder[5]), ".avgHSize .brC", colourScheme.reusedColours.grey);
      sampleBar(".houseHoldBar", barJson);
    });
  }

  ;
  household_size("/housing/household-size-and-bedrooms-2011-census-by-2016-ward.json", "&");
});