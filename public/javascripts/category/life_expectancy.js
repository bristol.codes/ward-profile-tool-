"use strict";

$(function () {
  function life_expectancy(fileName, sign, timePeriods) {
    var options = {
      tooltipDecimals: 1,
      bristolAverageDecimals: 1,
      isBristolAverageFromJsonIfAvailable: true,
      displayBristolBar: false,
      addedToMouseOverUnits: ' years'
    };
    var year1 = timePeriods[0];
    $.getJSON("../../Json/" + fileName, function (data) {
      var testing = changeName(ward, sign);
      ward = testing;
      /* For Bars */

      var femaleJson = [];
      var maleJson = [];
      /*For trend Line*/

      var trendYearsF = [];
      var trendYearsM = [];
      var wardValuesM = {},
          wardValuesF = {},
          bristolValuesM = {},
          bristolValuesF = {};
      /* For circles */

      var lowerLevelConfidenceWardm = 0;
      var uppperLevelConfidenceWardm = 0;
      var lowerLevelConfidenceWardf = 0;
      var uppperLevelConfidenceWardf = 0;
      var lowerLevelConfidenceBristolm = 0;
      var uppperLevelConfidenceBristolm = 0;
      var lowerLevelConfidenceBristolf = 0;
      var uppperLevelConfidenceBristolf = 0;
      /* for horizontal bars */

      var femaleW3 = {},
          maleW3 = {},
          femaleB3 = {},
          maleB3 = {};
      $.each(data, function (i, obj) {
        var wardN = data[i].fields.ward_name;
        var maleLifeX = +data[i].fields.male_life_expectancy;
        var femaleLifeX = +data[i].fields.female_life_expectancy;
        var date = data[i].fields.year;
        var lowerCLevelm = +data[i].fields.male_life_expectancy_ll;
        var upperCLevelm = +data[i].fields.male_life_expectancy_ul;
        var lowerCLevelf = +data[i].fields.female_life_expectancy_ll;
        var upperCLevelf = +data[i].fields.female_life_expectancy_ul;

        if (wardN == ward) {
          wardValuesM[date] = (Object.prototype.hasOwnProperty.call(wardValuesM, date) ? wardValuesM[date] : 0) + maleLifeX;
          wardValuesF[date] = (Object.prototype.hasOwnProperty.call(wardValuesF, date) ? wardValuesF[date] : 0) + femaleLifeX;
          var maleObj = new Object();
          maleObj.name = ward + " Males";
          maleObj.value = maleLifeX;
          maleObj.ward = ward;
          var femaleObj = new Object();
          femaleObj.name = ward + " Females";
          femaleObj.value = femaleLifeX;
          femaleObj.ward = ward;

          if (wardN !== "Outside of Area" && wardN !== "Unknown" && date == year1) {
            femaleW3 = femaleObj;
            maleW3 = maleObj;
            lowerLevelConfidenceWardm = lowerCLevelm;
            uppperLevelConfidenceWardm = upperCLevelm;
            lowerLevelConfidenceWardf = lowerCLevelf;
            uppperLevelConfidenceWardf = upperCLevelf;
          }
        } else if (wardN == "Bristol") {
          bristolValuesM[date] = (Object.prototype.hasOwnProperty.call(bristolValuesM, date) ? bristolValuesM[date] : 0) + maleLifeX;
          bristolValuesF[date] = (Object.prototype.hasOwnProperty.call(bristolValuesF, date) ? bristolValuesF[date] : 0) + femaleLifeX;

          var _maleObj = new Object();

          _maleObj.name = "Bristol Males";
          _maleObj.value = maleLifeX;
          _maleObj.ward = "Bristol";

          var _femaleObj = new Object();

          _femaleObj.name = "Bristol Females";
          _femaleObj.value = femaleLifeX;
          _femaleObj.ward = "Bristol";

          if (wardN !== "Outside of Area" && wardN !== "Unknown" && date == year1) {
            femaleB3 = _femaleObj;
            maleB3 = _maleObj;
            lowerLevelConfidenceBristolm = lowerCLevelm;
            uppperLevelConfidenceBristolm = upperCLevelm;
            lowerLevelConfidenceBristolf = lowerCLevelf;
            uppperLevelConfidenceBristolf = upperCLevelf;
          }
        }

        if (date == year1 && wardN !== "Bristol") {
          var feed = new Object();
          feed.ward = wardN;
          feed.info = maleLifeX;
          maleJson.push(feed);
          var feed2 = new Object();
          feed2.ward = wardN;
          feed2.info = femaleLifeX;
          femaleJson.push(feed2);
        }
      }); // Colour circles

      var clrFemale = confidenceLevels(fileName, uppperLevelConfidenceWardf, lowerLevelConfidenceWardf, uppperLevelConfidenceBristolf, lowerLevelConfidenceBristolf);
      var clrMale = confidenceLevels(fileName, uppperLevelConfidenceWardm, lowerLevelConfidenceWardm, uppperLevelConfidenceBristolm, lowerLevelConfidenceBristolm); //gpahic1 = female

      $(".graphic1").find(".dot3").css('background-color', clrFemale);
      $(".graphic2").find(".dot3").css('background-color', clrMale);
      horizontalBar([femaleB3, femaleW3], ".graphic1");
      horizontalBar([maleB3, maleW3], ".graphic2"); // avg not caluclating correctly, used fixed value for latest year used in above arrays.

      femaleJson.push({
        ward: "Bristol",
        info: femaleB3.value
      });
      maleJson.push({
        ward: "Bristol",
        info: maleB3.value
      });
      sampleBar(".bar1", femaleJson, options);
      sampleBar(".bar2", maleJson, options); // Trend line data setup:
      // trend years need to be ascending so sorted first then datapoints collected in order.

      timePeriods.sort().forEach(function (yearElement) {
        // years field must be a numerical value for d3's Linear scale, if ordinal then should be unaffected.
        var sanitisedLabel = stripAnythingAfterDash(stripAnythingAfterSlash(yearElement));

        if (true) {
          //!isNaN(wardValuesF[yearElement]) && !isNaN(bristolValuesF[yearElement])) {
          trendYearsF.push({
            "year": parseInt(sanitisedLabel),
            "ward": wardValuesF[yearElement],
            "bristol": bristolValuesF[yearElement],
            "label": yearElement
          });
        }

        if (true) {
          //!isNaN(wardValuesM[yearElement]) && !isNaN(bristolValuesM[yearElement])) {
          trendYearsM.push({
            "year": parseInt(sanitisedLabel),
            "ward": wardValuesM[yearElement],
            "bristol": bristolValuesM[yearElement],
            "label": yearElement
          });
        }
      });

      if (trendYearsM.length <= 1) {
        $(".trend2").hide();
      } else {
        $(".trend2").show();
      }

      if (trendYearsF.length <= 1) {
        $(".trend").hide();
      } else {
        $(".trend").show();
      }

      trendYearsF.forEach(function (d) {
        d.year = +d.year;
        d.ward = +d.ward;
        d.bristol = +d.bristol;
      });
      trendYearsM.forEach(function (d) {
        d.year = +d.year;
        d.ward = +d.ward;
        d.bristol = +d.bristol;
      });
      var chart = makeLineChart(trendYearsF, 'year', {
        'Ward': {
          column: 'ward'
        },
        'Bristol': {
          column: 'bristol'
        }
      }, {
        xAxis: 'Years',
        yAxis: 'Life Expectancy'
      }, trendYearsF.length); //Todo:

      chart.bind("#chart-line1");
      chart.render();
      var chart = makeLineChart(trendYearsM, 'year', {
        'Ward': {
          column: 'ward'
        },
        'Bristol': {
          column: 'bristol'
        }
      }, {
        xAxis: 'Years',
        yAxis: 'Life Expectancy'
      }, trendYearsF.length); //Todo:

      chart.bind("#chart-line2");
      chart.render();
    });
  }

  life_expectancy("health_and_wellbeing/life-expectancy-in-bristol.json", "&", ["2015-2017", "2014-2016", "2013-2015"]);
});