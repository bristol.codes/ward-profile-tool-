"use strict";

function houseHCalc() {
  var mainH = 250;
  /*
          Working out a specific percent of a number for 
          situations like this is easy. First, convert the 
          percentage you want into a fraction or a decimal.
           So in this case, 25 percent = 0.25 = 1/4. Then 
           multiply the decimal or fraction by the number you want the percentage of.
  */

  $('.prc').find('.textPrc').each(function (index) {
    var newW = $(this).text();
    newW = newW.replace(/[^\d.-]/g, '');
    var decim = newW / 100;
    var newCalc = decim * mainH;
    $(this).parent().parent().find(".picH").height(newCalc);
    $(this).parent().parent().find(".picH2").height(newCalc);
  });
}

;
setTimeout(houseHCalc, 200);