"use strict";

$(function () {
  // specify a format function we'll use later
  // https://github.com/mbostock/d3/wiki/Formatting
  var comma = d3.format(",");
  d3.json("../../Json/free-school-meals-in-bristol.json", function (data) {
    // create the drop down menu of cities
    var selector = d3.select("#testSelect").append("select").attr("id", "cityselector").selectAll("option").data(data).enter().append("option").text(function (d) {
      return d.fields.ward_name;
    }).attr("value", function (d, i) {
      return i;
    }); // generate a random index value and set the selector to the city
    // at that index value in the data array

    var index = Math.round(Math.random() * data.length);
    d3.select("#cityselector").property("selectedIndex", index); // append a paragraph tag to the body that shows the city name and it's population

    d3.select("#testSelect").append("p").classed("my-selector", true).data(data).text(function (d) {
      //var rounded = Math.round( meals * 10 ) / 10;
      return Math.round(data[index].fields.free_school_meals_of_all_pupils * 10) / 10 + "% - Year:  " + data[index].fields.year;
    }); //data[i].fields.year;
    // when the user selects a city, set the value of the index variable
    // and call the update(); function

    d3.select("#cityselector").on("change", function (d) {
      index = this.value;
      update();
    }); // update the paragraph text to match the selection made by the user

    function update() {
      d3.selectAll(".my-selector").data(data).text(function (d) {
        return Math.round(data[index].fields.free_school_meals_of_all_pupils * 10) / 10 + "% - Year:  " + data[index].fields.year;
      });
    }
  });
});