"use strict";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function sampleFuncConf(fileName, amountFieldName, year, timePoints, sign, suppliedOptions) {
  var defaultOptions = {};

  var options = _objectSpread({}, defaultOptions, {}, suppliedOptions);

  var latestYear = timePoints[0];
  var testing = changeName(ward, sign);
  ward = testing;
  $.getJSON("../../Json/" + fileName, function (data) {
    var allWards = [];
    var trendYears2 = [];
    /*For trend Line*/

    var wardValues = {},
        bristolValues = {};
    var circleCheckerWard = [];
    var circleCheckerBristol = [];
    /*Standard Deviation / Confidence Intervals */

    var lowerLevelConfidenceWard = 0;
    var uppperLevelConfidenceWard = 0;
    var lowerLevelConfidenceBristol = 0;
    var uppperLevelConfidenceBristol = 0;
    $.each(data, function (i, obj) {
      var wardN = data[i].fields.ward_name;
      var dataz = data[i].fields[amountFieldName];
      var date = data[i].fields[year];

      if (fileName == "/health_and_wellbeing/premature-mortality-all-causes-in-bristol.json") {
        var lowerCLevel = data[i].fields.all_cause_prem_mortality_ll_persons;
        var upperCLevel = data[i].fields.all_cause_prem_mortality_ul_persons;

        if (wardN == ward && date == latestYear && wardN !== "Outside of Area" && wardN !== "Unknown") {
          lowerLevelConfidenceWard = lowerCLevel;
          uppperLevelConfidenceWard = upperCLevel;
          console.log("ward CONF", lowerLevelConfidenceWard, uppperLevelConfidenceWard);
        } else if (wardN == "Bristol" && date == latestYear && wardN !== "Outside of Area" && wardN !== "Unknown") {
          lowerLevelConfidenceBristol = lowerCLevel;
          uppperLevelConfidenceBristol = upperCLevel;
          console.log("BRISTOL CONF", lowerLevelConfidenceBristol, uppperLevelConfidenceBristol);
        }
      } else if (amountFieldName == "cancer_premature_mortality_asr_persons") {
        var lowerCLevel = data[i].fields.cancer_premature_mortality_ll_persons;
        var upperCLevel = data[i].fields.cancer_premature_mortality_ul_persons;

        if (wardN == ward && date == latestYear && wardN !== "Outside of Area" && wardN !== "Unknown") {
          lowerLevelConfidenceWard = lowerCLevel;
          uppperLevelConfidenceWard = upperCLevel;
          console.log("ward CONF", lowerLevelConfidenceWard, uppperLevelConfidenceWard);
        } else if (wardN == "Bristol" && date == latestYear && wardN !== "Outside of Area" && wardN !== "Unknown") {
          lowerLevelConfidenceBristol = lowerCLevel;
          uppperLevelConfidenceBristol = upperCLevel;
          console.log("BRISTOL CONF", lowerLevelConfidenceBristol, uppperLevelConfidenceBristol);
        }
      } else if (amountFieldName == "cardiovascular_disease_premature_mortality_asr_persons") {
        var lowerCLevel = data[i].fields.cardiovascular_disease_premature_mortality_ll_persons;
        var upperCLevel = data[i].fields.cardiovascular_disease_premature_mortality_ul_persons;

        if (wardN == ward && date == latestYear && wardN !== "Outside of Area" && wardN !== "Unknown") {
          lowerLevelConfidenceWard = lowerCLevel;
          uppperLevelConfidenceWard = upperCLevel;
          console.log("ward CONF", lowerLevelConfidenceWard, uppperLevelConfidenceWard);
        } else if (wardN == "Bristol" && date == latestYear && wardN !== "Outside of Area" && wardN !== "Unknown") {
          lowerLevelConfidenceBristol = lowerCLevel;
          uppperLevelConfidenceBristol = upperCLevel;
          console.log("BRISTOL CONF", lowerLevelConfidenceBristol, uppperLevelConfidenceBristol);
        }
      } else if (amountFieldName == "respiratory_disease_premature_mortality_asr_persons") {
        var lowerCLevel = data[i].fields.respiratory_disease_premature_mortality_ll_persons;
        var upperCLevel = data[i].fields.respiratory_disease_premature_mortality_ul_persons;

        if (wardN == ward && date == latestYear && wardN !== "Outside of Area" && wardN !== "Unknown") {
          lowerLevelConfidenceWard = lowerCLevel;
          uppperLevelConfidenceWard = upperCLevel;
          console.log("ward CONF", lowerLevelConfidenceWard, uppperLevelConfidenceWard);
        } else if (wardN == "Bristol" && date == latestYear && wardN !== "Outside of Area" && wardN !== "Unknown") {
          lowerLevelConfidenceBristol = lowerCLevel;
          uppperLevelConfidenceBristol = upperCLevel;
          console.log("BRISTOL CONF", lowerLevelConfidenceBristol, uppperLevelConfidenceBristol);
        }
      } else if (fileName == "/health_and_wellbeing/reception-children-with-excess-weight-in-bristol.json") {
        var lowerCLevel = data[i].fields.reception_children_with_excess_weight_ll;
        var upperCLevel = data[i].fields.reception_children_with_excess_weight_ul;

        if (wardN == ward && date == latestYear && wardN !== "Outside of Area" && wardN !== "Unknown") {
          lowerLevelConfidenceWard = lowerCLevel;
          uppperLevelConfidenceWard = upperCLevel;
          console.log("ward CONF", lowerLevelConfidenceWard, uppperLevelConfidenceWard);
        } else if (wardN == "Bristol" && date == latestYear && wardN !== "Outside of Area" && wardN !== "Unknown") {
          lowerLevelConfidenceBristol = lowerCLevel;
          uppperLevelConfidenceBristol = upperCLevel;
          console.log("BRISTOL CONF", lowerLevelConfidenceBristol, uppperLevelConfidenceBristol);
        }
      } else if (fileName == "/health_and_wellbeing/year-6-children-age-10-11-years-with-excess-weight-in-bristol.json") {
        var lowerCLevel = data[i].fields.year_6_children_with_excess_weight_ll;
        var upperCLevel = data[i].fields.year_6_children_with_excess_weight_ul;

        if (wardN == ward && date == latestYear && wardN !== "Outside of Area" && wardN !== "Unknown") {
          lowerLevelConfidenceWard = lowerCLevel;
          uppperLevelConfidenceWard = upperCLevel;
          console.log("ward CONF", lowerLevelConfidenceWard, uppperLevelConfidenceWard);
        } else if (wardN == "Bristol" && date == latestYear && wardN !== "Outside of Area" && wardN !== "Unknown") {
          lowerLevelConfidenceBristol = lowerCLevel;
          uppperLevelConfidenceBristol = upperCLevel;
          console.log("BRISTOL CONF", lowerLevelConfidenceBristol, uppperLevelConfidenceBristol);
        }
      } // Main data loop


      if (date == latestYear && wardN !== "Outside of Area" && wardN !== "Unknown") {
        var feed2 = new Object();
        feed2.ward = wardN;
        feed2.info = dataz;
        allWards.push(feed2);
      }

      if (wardN == ward && wardN !== "Outside of Area" && wardN !== "Unknown") {
        wardValues[date] = (Object.prototype.hasOwnProperty.call(wardValues, date) ? wardValues[date] : 0) + dataz;

        if (date == latestYear) {
          var circleObj = new Object();
          circleObj.name = wardN;
          circleObj.info = dataz;
          circleObj.date = date;
          circleObj.x = "220";
          circleCheckerWard.push(circleObj);
        }
      } //if it is the bristol ward and the specified year


      if (wardN === "Bristol") {
        bristolValues[date] = (Object.prototype.hasOwnProperty.call(bristolValues, date) ? bristolValues[date] : 0) + dataz;

        if (date == latestYear) {
          var circleObj = new Object();
          circleObj.name = wardN;
          circleObj.info = dataz;
          circleObj.date = date;
          circleObj.x = "220";
          circleCheckerBristol.push(circleObj);
        }
      }
    }); //json
    // trend years need to be ascending so sorted first then datapoints collected in order.

    timePoints.sort().forEach(function (yearElement) {
      if (true) {
        //!isNaN(wardValues[yearElement]) && !isNaN(bristolValues[yearElement])) {
        // years field must be a numerical value for d3's Linear scale, if ordinal then should be unaffected.
        var sanitisedLabel = stripAnythingAfterDash(stripAnythingAfterSlash(yearElement));
        trendYears2.push({
          "year": parseInt(sanitisedLabel),
          "ward": wardValues[yearElement],
          "bristol": bristolValues[yearElement],
          "label": yearElement
        });
      }
    });

    if (trendYears2.length - countFullMissingYears(trendYears2) <= 1) {
      $("#Trend").hide();
      console.log("one trend year, hiding trend graph");
    } else {
      $("#Trend").show();
    }

    trendYears2.forEach(function (d) {
      d.year = +d.year;
      d.ward = +d.ward;
      d.bristol = +d.bristol;
    });
    var chart = makeLineChart(trendYears2, 'year', {
      'Ward': {
        column: 'ward'
      },
      'Bristol': {
        column: 'bristol'
      }
    }, {
      xAxis: 'Years',
      yAxis: 'Amount'
    }, trendYears2.length);
    chart.bind("#chart-line1");
    chart.render();
    sampleBar("#test", allWards, options);
    var wardChecker = circleCheckerWard[0].info;
    var brsChecker = circleCheckerBristol[0].info;
    var fixedSize = 20; //100% in other words

    var clrs = confidenceLevels(fileName, uppperLevelConfidenceWard, lowerLevelConfidenceWard, uppperLevelConfidenceBristol, lowerLevelConfidenceBristol);

    if (wardChecker > brsChecker) {
      var calcNew = 100 / wardChecker;
      var test = brsChecker * calcNew;
      var make20 = test / 5;
      make20 = 20 / make20;
      make20 = make20 * 20;
      sampleCirc("#test2", colourScheme.sampleCircle.grey, circleCheckerBristol, "right", fixedSize);
      sampleCirc("#test2", clrs, circleCheckerWard, "left", make20);
    } else if (wardChecker < brsChecker) {
      var calcNew = 100 / brsChecker;
      var test = wardChecker * calcNew;
      var make20 = test / 5;
      sampleCirc("#test2", colourScheme.sampleCircle.grey, circleCheckerBristol, "right", fixedSize);
      sampleCirc("#test2", clrs, circleCheckerWard, "left", make20);
    } else {
      sampleCirc("#test2", colourScheme.sampleCircle.grey, circleCheckerBristol, "right", fixedSize);
      sampleCirc("#test2", clrs, circleCheckerWard, "left", fixedSize);
    }
  }); //end of json
} //end of func