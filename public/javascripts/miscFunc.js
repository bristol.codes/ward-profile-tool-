"use strict";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/* eslint-disable no-unused-vars */

/* eslint-disable no-redeclare */

/* eslint-disable no-undef */

/* Main function file- this is where most of the common functions are stored*/
//changing a whole array from numbers to %
function functionChangArr(arr, tot, target) {
  for (var i = 0; i < arr.length; i++) {
    var newPrecentage = oneDcm(arr[i] / tot * 100);
    target.push(newPrecentage);
  }
}

function countFullMissingYears(params) {
  var counter = 0;
  params.slice(0).forEach(function (element) {
    if (isNaN(element.ward) && isNaN(element.bristol)) counter++;
  });
  return counter;
} //creating commas for thousands


function numberWithCommas(x) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
} //making a number negative


function makeNeg(arr) {
  for (var i = 0; i < arr.length; i++) {
    arr[i] = -Math.abs(arr[i]);
  }
} //function used withing age and gender


function populationFunc(fileName, sign) {
  var testing = changeName(ward, sign);
  ward = testing;
  var agesArray = [];
  var agesArray2 = [];
  var total_age_0_15 = 0;
  var total_age_16_24 = 0;
  var total_age_25_39 = 0;
  var total_age_40_54 = 0;
  var total_age_55_64 = 0;
  var total_age_65 = 0;
  var totalAge = 0;
  var total2 = 0;
  var bristol_total_age_0_15 = 0;
  var bristol_total_age_16_24 = 0;
  var bristol_total_age_25_39 = 0;
  var bristol_total_age_40_54 = 0;
  var bristol_total_age_55_64 = 0;
  var bristol_total_age_65 = 0;
  var wardHolderArray = [];
  var bristolHolderArray = [];
  var overallHolder = [];
  var combinedSexes = {};
  var bristol_total_all_ages = 0;
  /*Standard Deviation*/

  var sumOfMean = 0;
  var sumOfMean2 = 0;
  var sumOfMean3 = 0;
  var sumOfMean4 = 0;
  var sumOfMean5 = 0;
  var sumOfMean6 = 0;
  var countWard = 0;
  $.getJSON("../../Json/" + fileName, function (data) {
    //todo: switch to $.each
    data.forEach(function (x) {
      var t = combinedSexes[x.fields["2016_ward_code"]] || {};
      t["age_0_4"] = (+t["age_0_4"] || 0) + +x.fields.age_0_4;
      t["age_5_9"] = (+t["age_5_9"] || 0) + +x.fields.age_5_9;
      t["age_10_14"] = (+t["age_10_14"] || 0) + +x.fields.age_10_14;
      t["age_15"] = (+t["age_15"] || 0) + +x.fields.age_15;
      t["age_16_19"] = (+t["age_16_19"] || 0) + +x.fields.age_16_19;
      t["age_20_24"] = (+t["age_20_24"] || 0) + +x.fields.age_20_24;
      t["age_25_29"] = (+t["age_25_29"] || 0) + +x.fields.age_25_29;
      t["age_30_34"] = (+t["age_30_34"] || 0) + +x.fields.age_30_34;
      t["age_35_39"] = (+t["age_35_39"] || 0) + +x.fields.age_35_39;
      t["age_40_44"] = (+t["age_40_44"] || 0) + +x.fields.age_40_44;
      t["age_45_49"] = (+t["age_45_49"] || 0) + +x.fields.age_45_49;
      t["age_50_54"] = (+t["age_50_54"] || 0) + +x.fields.age_50_54;
      t["age_55_59"] = (+t["age_55_59"] || 0) + +x.fields.age_55_59;
      t["age_60_64"] = (+t["age_60_64"] || 0) + +x.fields.age_60_64;
      t["age_65_69"] = (+t["age_65_69"] || 0) + +x.fields.age_65_69;
      t["age_70_74"] = (+t["age_70_74"] || 0) + +x.fields.age_70_74;
      t["age_75_79"] = (+t["age_75_79"] || 0) + +x.fields.age_75_79;
      t["age_80_84"] = (+t["age_80_84"] || 0) + +x.fields.age_80_84;
      t["age_85_89"] = (+t["age_85_89"] || 0) + +x.fields.age_85_89;
      t["age_90"] = (+t["age_90"] || 0) + +x.fields.age_90;
      t["allAge"] = (+t["allAge"] || 0) + +x.fields.all_ages;
      t["sexes"] = (t["sexes"] || "") + x.fields.sex;
      t["wardName"] = x.fields["2016_ward_name"];
      combinedSexes[x.fields["2016_ward_code"]] = t;
    });
    $.each(data, function (i, obj) {
      var age_0_4 = data[i].fields.age_0_4;
      var age_5_9 = data[i].fields.age_5_9;
      var age_10_14 = data[i].fields.age_10_14;
      var age_15 = data[i].fields.age_15;
      var age_16_19 = data[i].fields.age_16_19;
      var age_20_24 = data[i].fields.age_20_24;
      var age_25_29 = data[i].fields.age_25_29;
      var age_30_34 = data[i].fields.age_30_34;
      var age_35_39 = data[i].fields.age_35_39;
      var age_40_44 = data[i].fields.age_40_44;
      var age_45_49 = data[i].fields.age_45_49;
      var age_50_54 = data[i].fields.age_50_54;
      var age_55_59 = data[i].fields.age_55_59;
      var age_60_64 = data[i].fields.age_60_64;
      var age_65_69 = data[i].fields.age_65_69;
      var age_70_74 = data[i].fields.age_70_74;
      var age_75_79 = data[i].fields.age_75_79;
      var age_80_84 = data[i].fields.age_80_84;
      var age_85_89 = data[i].fields.age_85_89;
      var age_90 = data[i].fields.age_90;
      var allAge = data[i].fields.all_ages;
      var wardName = data[i].fields["2016_ward_name"];
      bristol_total_all_ages = bristol_total_all_ages + allAge;
      bristol_total_age_0_15 = bristol_total_age_0_15 + age_0_4 + age_5_9 + age_10_14 + age_15;
      bristol_total_age_16_24 = bristol_total_age_16_24 + age_16_19 + age_20_24;
      bristol_total_age_25_39 = bristol_total_age_25_39 + age_25_29 + age_30_34 + age_35_39;
      bristol_total_age_40_54 = bristol_total_age_40_54 + age_40_44 + age_45_49 + age_50_54;
      bristol_total_age_55_64 = bristol_total_age_55_64 + age_55_59 + age_60_64;
      bristol_total_age_65 = bristol_total_age_65 + age_65_69 + age_70_74 + age_75_79 + age_80_84 + age_85_89 + age_90;

      if (wardName == ward) {
        var tempArray = [];
        total_age_0_15 = age_0_4 + age_5_9 + age_10_14 + age_15;
        total_age_16_24 = age_16_19 + age_20_24;
        total_age_25_39 = age_25_29 + age_30_34 + age_35_39;
        total_age_40_54 = age_40_44 + age_45_49 + age_50_54;
        total_age_55_64 = age_55_59 + age_60_64;
        total_age_65 = age_65_69 + age_70_74 + age_75_79 + age_80_84 + age_85_89 + age_90;
        totalAge = totalAge + allAge;
        tempArray.push(total_age_0_15);
        tempArray.push(total_age_16_24);
        tempArray.push(total_age_25_39);
        tempArray.push(total_age_40_54);
        tempArray.push(total_age_55_64);
        tempArray.push(total_age_65);
        agesArray.push(tempArray);
      }
    });
    var total = 0;

    for (var n = 0; n < agesArray[0].length; n++) {
      total = agesArray[0][n] + agesArray[1][n];
      total2 = total2 + total;
      agesArray2.push(total);
    }

    var wardTotalPop = Math.round(total2 / 100) * 100;
    $(".wrds").append(" (population " + numberWithCommas(wardTotalPop) + ")");
    var new_total_age_0_15 = agesArray2[0];
    var new_total_age_16_24 = agesArray2[1];
    var new_total_age_25_39 = agesArray2[2];
    var new_total_age_40_54 = agesArray2[3];
    var new_total_age_55_64 = agesArray2[4];
    var new_total_age_65 = agesArray2[5];
    var perecentage_new_total_age_0_15 = new_total_age_0_15 / totalAge * 100;
    var perecentage_new_total_age_16_24 = new_total_age_16_24 / totalAge * 100;
    var perecentage_new_total_age_25_39 = new_total_age_25_39 / totalAge * 100;
    var perecentage_new_total_age_40_54 = new_total_age_40_54 / totalAge * 100;
    var perecentage_new_total_age_55_64 = new_total_age_55_64 / totalAge * 100;
    var perecentage_new_total_age_65 = new_total_age_65 / totalAge * 100;
    var bristol_perecentage_new_total_age_0_15 = bristol_total_age_0_15 / bristol_total_all_ages * 100;
    var bristol_perecentage_new_total_age_16_24 = bristol_total_age_16_24 / bristol_total_all_ages * 100;
    var bristol_perecentage_new_total_age_25_39 = bristol_total_age_25_39 / bristol_total_all_ages * 100;
    var bristol_perecentage_new_total_age_40_54 = bristol_total_age_40_54 / bristol_total_all_ages * 100;
    var bristol_perecentage_new_total_age_55_64 = bristol_total_age_55_64 / bristol_total_all_ages * 100;
    var bristol_perecentage_new_total_age_65 = bristol_total_age_65 / bristol_total_all_ages * 100;
    wardHolderArray.push(perecentage_new_total_age_0_15);
    wardHolderArray.push(perecentage_new_total_age_16_24);
    wardHolderArray.push(perecentage_new_total_age_25_39);
    wardHolderArray.push(perecentage_new_total_age_40_54);
    wardHolderArray.push(perecentage_new_total_age_55_64);
    wardHolderArray.push(perecentage_new_total_age_65);
    bristolHolderArray.push(bristol_perecentage_new_total_age_0_15);
    bristolHolderArray.push(bristol_perecentage_new_total_age_16_24);
    bristolHolderArray.push(bristol_perecentage_new_total_age_25_39);
    bristolHolderArray.push(bristol_perecentage_new_total_age_40_54);
    bristolHolderArray.push(bristol_perecentage_new_total_age_55_64);
    bristolHolderArray.push(bristol_perecentage_new_total_age_65);
    overallHolder.push(wardHolderArray, bristolHolderArray);
    var setLayout = 0;
    var ageGrp = "text";
    var setR = "right";
    var setL = "left";
    var eachWardNr = $.map(combinedSexes, function (x) {
      return +x["age_0_4"] + +x["age_5_9"] + +x["age_10_14"] + +x["age_15"];
    });
    var eachWardNr2 = $.map(combinedSexes, function (x) {
      return +x["age_16_19"] + +x["age_20_24"];
    });
    var eachWardNr3 = $.map(combinedSexes, function (x) {
      return +x["age_25_29"] + +x["age_30_34"] + +x["age_35_39"];
    });
    var eachWardNr4 = $.map(combinedSexes, function (x) {
      return +x["age_40_44"] + +x["age_45_49"] + +x["age_50_54"];
    });
    var eachWardNr5 = $.map(combinedSexes, function (x) {
      return +x["age_55_59"] + +x["age_60_64"];
    });
    var eachWardNr6 = $.map(combinedSexes, function (x) {
      return +x["age_65_69"] + +x["age_70_74"] + +x["age_75_79"] + +x["age_80_84"] + +x["age_85_89"] + +x["age_90"];
    });
    var wardTotals = $.map(combinedSexes, function (x) {
      return x["allAge"];
    });
    eachWardNr = eachWardNr.map(function (x, index, arr) {
      return to15sf(+x / +wardTotals[index] * 100);
    }); // +bristol_total_all_ages})

    eachWardNr2 = eachWardNr2.map(function (x, index, arr) {
      return to15sf(+x / +wardTotals[index] * 100);
    }); // +bristol_total_all_ages}) 

    eachWardNr3 = eachWardNr3.map(function (x, index, arr) {
      return to15sf(+x / +wardTotals[index] * 100);
    }); // +bristol_total_all_ages}) 

    eachWardNr4 = eachWardNr4.map(function (x, index, arr) {
      return to15sf(+x / +wardTotals[index] * 100);
    }); // +bristol_total_all_ages}) 

    eachWardNr5 = eachWardNr5.map(function (x, index, arr) {
      return to15sf(+x / +wardTotals[index] * 100);
    }); // +bristol_total_all_ages}) 

    eachWardNr6 = eachWardNr6.map(function (x, index, arr) {
      return to15sf(+x / +wardTotals[index] * 100);
    }); // +bristol_total_all_ages}) 

    sumOfMean = eachWardNr.reduce(function (s, v) {
      return s += +v;
    }, 0);
    sumOfMean2 = eachWardNr2.reduce(function (s, v) {
      return s += +v;
    }, 0);
    sumOfMean3 = eachWardNr3.reduce(function (s, v) {
      return s += +v;
    }, 0);
    sumOfMean4 = eachWardNr4.reduce(function (s, v) {
      return s += +v;
    }, 0);
    sumOfMean5 = eachWardNr5.reduce(function (s, v) {
      return s += +v;
    }, 0);
    sumOfMean6 = eachWardNr6.reduce(function (s, v) {
      return s += +v;
    }, 0);
    var standard = standardDeviation(sumOfMean, eachWardNr.length, eachWardNr);
    var standard2 = standardDeviation(sumOfMean2, eachWardNr2.length, eachWardNr2);
    var standard3 = standardDeviation(sumOfMean3, eachWardNr3.length, eachWardNr3);
    var standard4 = standardDeviation(sumOfMean4, eachWardNr4.length, eachWardNr4);
    var standard5 = standardDeviation(sumOfMean5, eachWardNr5.length, eachWardNr5);
    var standard6 = standardDeviation(sumOfMean6, eachWardNr6.length, eachWardNr6);
    var clrs = colStd(fileName, wardHolderArray[0], bristolHolderArray[0], standard);
    var clrs2 = colStd(fileName, wardHolderArray[1], bristolHolderArray[1], standard2);
    var clrs3 = colStd(fileName, wardHolderArray[2], bristolHolderArray[2], standard3);
    var clrs4 = colStd(fileName, wardHolderArray[3], bristolHolderArray[3], standard4);
    var clrs5 = colStd(fileName, wardHolderArray[4], bristolHolderArray[4], standard5);
    var clrs6 = colStd(fileName, wardHolderArray[5], bristolHolderArray[5], standard6);
    var clrArray = [clrs, clrs2, clrs3, clrs4, clrs5, clrs6];

    for (var z = 0; z < overallHolder[0].length; z++) {
      if (setLayout == 0) {
        ageGrp = "0-15";
      } else if (setLayout == 1) {
        ageGrp = "16-24";
      } else if (setLayout == 2) {
        ageGrp = "25-39";
      } else if (setLayout == 3) {
        ageGrp = "40-54";
      } else if (setLayout == 4) {
        ageGrp = "55-64";
      } else if (setLayout == 5) {
        ageGrp = "65+";
      }

      $("#popDisplay").append("<div class='holderAge'><div class='dh'><span class='dot4 " + clrArray[z] + "'></span><p class='ageP'>" + ageGrp + " years</p></div><div class='innerH'><div class='barImg'></div><div class='wardPop'>" + oneDcm(overallHolder[0][z]) + "%</div></div><div class='innerH'><div class='barImg'></div><div class='brsPop'>" + oneDcm(overallHolder[1][z]) + "%</div></div></div>");
      setLayout++;
    }

    var holderWidth = $(".holderAge").width();
    $('.innerH').find('.wardPop').each(function (index) {
      var newW = $(this).text();
      $(this).parent().find(".barImg").width(newW);
    });
    $('.innerH').find('.brsPop').each(function (index) {
      var newW = $(this).text();
      '.barImg';
      $(this).parent().find(".barImg").width(newW);
    });
    $('.innerH').find('.barImg').each(function (index) {
      var imgSize = $(this).width();
      var checkImg = oneDcm(imgSize / holderWidth * 100);
      var dotsize = $(this).parent().parent().find(".dh").width();
      var makePrcDot = oneDcm(dotsize / holderWidth * 100);
      var addup = checkImg + makePrcDot;
      var summary = 100 - addup;
      console.log("width of writing", summary);

      if ($(this).parent().find(".brsPop").length !== 0) {
        $(this).parent().find(".brsPop").width(summary - 2 + "%");
        $(this).css('background-color', colourScheme.reusedColours.darkOrange);
      } else if ($(this).parent().find(".wardPop").length !== 0) {
        $(this).parent().find(".wardPop").width(summary - 2 + "%");
        $(this).css('background-color', colourScheme.reusedColours.darkBlue);
      }
    });
  });
}

function sampleFunc(fileName, colValue, colYear, timePeriods, sign, suppliedOptions) {
  // support option for 2dp, set education -> progress8 to use 2dp, will then match.
  var firstYear = timePeriods[0];
  var defaultOptions = {
    decimals: 1
  };

  var options = _objectSpread({}, defaultOptions, {}, suppliedOptions);

  console.log("sampleFunc with " + options.decimals + "dp");
  var testing = changeName(ward, sign);
  ward = testing;
  $.getJSON("../../Json/" + fileName, function (data) {
    //console.log(data);
    var allWards = [];
    var trendValues = [];
    var eachWardNr = [];
    var stndName = fileName;
    /*For trend Line*/

    var bristolValues = {},
        wardValues = {};
    var calcWardLatestYr = 0;
    var calcBristolLatestYr = 0;
    var wardCircle = [];
    var bristolCircle = [];
    /*Standard Deviation*/

    var mean = 0;
    var countWard = 0;
    $.each(data, function (i, obj) {
      var wardN = data[i].fields.ward_name;
      var dataz = +data[i].fields[colValue];
      var date = data[i].fields[colYear];
      var rounded = correctDp(dataz, options.decimals);

      if (date == firstYear && wardN !== "Outside of Area" && wardN !== "Unknown") {
        var feed2 = new Object();
        feed2.ward = wardN;
        feed2.info = dataz;
        allWards.push(feed2);
      } //if it is the current ward and the specified year
      //console.log(wardN,ward,date,yr2)


      if (wardN == ward && wardN !== "Outside of Area" && wardN !== "Unknown") {
        wardValues[date] = (Object.prototype.hasOwnProperty.call(wardValues, date) ? wardValues[date] : 0) + dataz;

        if (date == firstYear) {
          var circleObj = new Object();
          circleObj.name = wardN;
          circleObj.info = rounded;
          circleObj.date = date;
          circleObj.x = "220";
          wardCircle.push(circleObj);
          calcWardLatestYr = calcWardLatestYr + dataz;
        }
      }

      if (wardN !== "Bristol" && date == firstYear && wardN !== "Outside of Area" && wardN !== "Unknown") {
        // Other ward - uninteresting except for average and sampleBars.
        mean = mean + dataz;
        countWard++;
        eachWardNr.push(dataz);
      } //if it is the bristol ward and the specified year


      if (wardN === "Bristol") {
        bristolValues[date] = (Object.prototype.hasOwnProperty.call(bristolValues, date) ? bristolValues[date] : 0) + dataz;

        if (date == firstYear) {
          var circleObj = new Object();
          circleObj.name = wardN;
          circleObj.info = rounded;
          circleObj.date = date;
          circleObj.x = "220";
          bristolCircle.push(circleObj);
          calcBristolLatestYr = calcBristolLatestYr + dataz;
        }
      }
    }); //json
    // trend years need to be ascending so sorted first then datapoints collected in order.

    timePeriods.sort().forEach(function (yearElement) {
      if (true) {
        //!isNaN(wardValues[yearElement]) && !isNaN(bristolValues[yearElement])) {
        // years field must be a numerical value for d3's Linear scale, if ordinal then should be unaffected.
        var sanitisedLabel = stripAnythingAfterDash(stripAnythingAfterSlash(yearElement));
        trendValues.push({
          "year": parseInt(sanitisedLabel),
          "ward": wardValues[yearElement],
          "bristol": bristolValues[yearElement],
          "label": yearElement
        });
      }
    });

    if (trendValues.length - countFullMissingYears(trendValues) <= 1) {
      $("#Trend").hide();
      console.log("one trend year, hiding trend graph");
    } else {
      $("#Trend").show();
    }

    trendValues.forEach(function (d) {
      d.year = +d.year;
      d.ward = +d.ward;
      d.bristol = +d.bristol;
    });
    var chart = makeLineChart(trendValues, 'year', {
      'Ward': {
        column: 'ward'
      },
      'Bristol': {
        column: 'bristol'
      }
    }, {
      xAxis: 'Years',
      yAxis: 'Amount'
    }, trendValues.length); //Todo:

    chart.bind("#chart-line1");
    chart.render();

    if (colValue == "average_progress_8_score") {
      sampleBar2("#test", allWards, options);
    } else {
      sampleBar("#test", allWards, options);
    } // Circles


    var wardChecker = wardCircle[0].info;
    var brsChecker = bristolCircle[0].info;
    var fixedSize = 20; //100% in other words

    var standard = standardDeviation(mean, countWard, eachWardNr);
    var clrs; //CALL FUNC LATER ------------------------------------>
    //clrs = colStd(stndName,wardChecker,brsChecker,standard);
    //high bad free , absence low is good
    //high is good att8 , early years ks2 expected standrd low is bad
    //high is high enlgish as additionl low is low
    //high is signi bad

    if (stndName === "pupils-classed-as-disadvantaged-in-bristol.json" || stndName === "pupils-with-special-educational-needs-in-bristol.json" || stndName === "free-school-meals-in-bristol.json" || stndName === "pupil-absence-in-bristol.json" || stndName === "% overweight or obese" || stndName === "/crime/crime-recorded-by-police-by-selected-offence-groups-in-bristol-by-ward.json" || stndName === "/crime/asb-incidents-reported-to-the-police-in-bristol.json" || stndName === "/crime/youth-offender-aged-10-17-years-in-bristol-by-ward.json") {
      if (wardChecker < brsChecker - standard) {
        console.log("Significantly less");
        clrs = colourScheme.reusedColours.green;
      } else if (wardChecker > brsChecker + standard) {
        console.log("Significanly greater");
        clrs = colourScheme.reusedColours.red;
      } else {
        console.log("not significantlyy Different");
        clrs = colourScheme.reusedColours.yellow;
      } //high is signi good
      //console.log("1");

    } else if (stndName === "progress-8-and-attainment-8-scores-in-bristol.json" || stndName === "early-years-pupils-achieving-a-good-level-of-development-in-bristol.json" || stndName === "key-stage-2-pupils-level-4-in-reading-writing-maths-combined.json" || stndName === "progress-8-and-attainment-8-scores-in-bristol.json") {
      if (wardChecker < brsChecker - standard) {
        console.log("Significantly less");
        clrs = colourScheme.reusedColours.red;
      } else if (wardChecker > brsChecker + standard) {
        console.log("Significanly greater");
        clrs = colourScheme.reusedColours.green;
      } else {
        console.log("not significantlyy Different");
        clrs = colourScheme.reusedColours.yellow;
      } //high or low
      //console.log("2");

    } else if (stndName === "pupils-with-english-as-an-additional-language-in-bristol-by-ward.json" || stndName === "/socialCare/adult-social-care-in-bristol-by-ward.json" || stndName === "/socialCare/children-in-social-care-within-bristol-by-ward.json") {
      if (wardChecker < brsChecker - standard) {
        console.log("Significantly less");
        clrs = colourScheme.reusedColours.pink;
      } else if (wardChecker > brsChecker + standard) {
        console.log("Significanly greater");
        clrs = colourScheme.reusedColours.purple;
      } else {
        console.log("not significantlyy Different");
        clrs = colourScheme.reusedColours.yellow;
      }
    } else {
      clrs = colourScheme.reusedColours.red;
    }

    if (wardChecker > brsChecker) {
      var calcNew = 100 / wardChecker;
      var test = brsChecker * calcNew;
      var make20 = test / 5;
      make20 = 20 / make20;
      make20 = make20 * 20;

      if (wardChecker < 0 || brsChecker < 0) {
        sampleCirc("#test2", colourScheme.sampleCircle.grey, bristolCircle, "right", fixedSize, options);
        sampleCirc("#test2", clrs, wardCircle, "left", fixedSize, options);
      } else {
        sampleCirc("#test2", colourScheme.sampleCircle.grey, bristolCircle, "right", fixedSize, options);
        sampleCirc("#test2", clrs, wardCircle, "left", make20, options);
      }
    } else if (wardChecker < brsChecker) {
      var calcNew = 100 / brsChecker;
      var test = wardChecker * calcNew;
      var make20 = test / 5;

      if (wardChecker < 0 || brsChecker < 0) {
        sampleCirc("#test2", colourScheme.sampleCircle.grey, bristolCircle, "right", fixedSize, options);
        sampleCirc("#test2", clrs, wardCircle, "left", fixedSize, options);
      } else {
        sampleCirc("#test2", colourScheme.sampleCircle.grey, bristolCircle, "right", fixedSize, options);
        sampleCirc("#test2", clrs, wardCircle, "left", make20, options);
      }
    } else {
      sampleCirc("#test2", colourScheme.sampleCircle.grey, bristolCircle, "right", fixedSize, options);
      sampleCirc("#test2", clrs, wardCircle, "left", fixedSize, options);
    }
  }); //end of json
} //end of func


var correctDp = function correctDp(num, numberOfDecimalPlaces) {
  return +(+num).toFixed(numberOfDecimalPlaces);
};

function to15sf(num) {
  return correctDp(num, 14);
} //function which rounds up a number to 1 decimal point


function oneDcm(calc) {
  return correctDp(calc, 1);
} //function that calculates standard deviation


function standardDeviation(sumOfMeans, countWard, eachWardNr) {
  //  return standardDeviation2(eachWardNr);
  var totzSqr = 0;
  var meanDivided = sumOfMeans / countWard;

  for (var loopWard = 0; loopWard < eachWardNr.length; loopWard++) {
    var numberSubtractMean = eachWardNr[loopWard] - meanDivided;
    var sq = numberSubtractMean * numberSubtractMean;
    totzSqr = totzSqr + sq;
  }

  var meanOfSquared = totzSqr / (eachWardNr.length - 1);
  var finalRes = Math.sqrt(meanOfSquared);
  return finalRes;
} //function to fix the year as d3 linear does not accept anything apart from a numeric number so i strip anything after / or - 


function stripAnythingAfterSlash(year) {
  var n = year.indexOf('/');
  year = year.substring(0, n != -1 ? n : year.length);
  return year;
}

function stripAnythingAfterDash(year) {
  var n = year.indexOf('-');
  year = year.substring(0, n != -1 ? n : year.length);
  return year;
} //function to change the category name so it replaces every _ with a space on the front end


function changeName(wardName, sign) {
  var desired = wardName.replace(/_/g, " ");
  var desired2 = titleCase(desired); //function which makes the string look nice on the front end

  function titleCase(str) {
    var splitString = str.toLowerCase().split(' ');

    for (var i = 0; i < splitString.length; i++) {
      // You do not need to check if i is larger than splitStr length, as your for does that for you
      // Assign it back to the array
      splitString[i] = splitString[i].charAt(0).toUpperCase() + splitString[i].substring(1);
    } // Directly return the joined string


    return splitString.join(' ');
  } //if it is wesbury make sure that it is changed to capital T as in the data set


  if (desired2 == "Westbury-on-trym And Henleaze") {
    desired2 = "Westbury-on-Trym and Henleaze";
  } else if (desired2 == "Westbury-on-trym & Henleaze") {
    desired2 = "Westbury-on-Trym & Henleaze";
  } //replace the and with whatever sign but ignore and as the last word.


  var desired2 = desired2.replace(/and(?!$)/gi, "and");

  if (sign == "and") {
    var res = desired2.replace(/&/gi, sign);
  } else {
    var res = desired2.replace(/and(?!$)/gi, sign);
  }

  return res;
} // change &amp; to the actual symbol


function makeAMP(str) {
  str = str.replace(/&amp;/g, '&');
  return str;
} //return the average of two numbers to two decimal points


function avg(x, y) {
  var average = ((+x + +y) / 2).toFixed(2);
  return average;
}