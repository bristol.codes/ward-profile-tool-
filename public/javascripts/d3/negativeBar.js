"use strict";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function sampleBar2(div, data, suppliedOptions) {
  var defaultOptions = {
    decimals: 1
  };

  var options = _objectSpread({}, defaultOptions, {}, suppliedOptions);

  console.log("sampleBar2 : " + options.decimals + "dp");

  function draw(data) {
    var margin = {
      top: 20,
      right: 20,
      bottom: 150,
      left: 40
    },
        width = 800 - margin.left - margin.right,
        height = 360 - margin.top - margin.bottom;
    data.sort(function (a, b) {
      return b.info - a.info;
    });
    var xScale = d3.scale.ordinal().rangeRoundBands([0, width], .2);
    var yScale = d3.scale.linear().domain([d3.min(data, function (d) {
      return d.info - 0.1;
    }), d3.max([0, d3.max(data, function (d) {
      return d.info + 0.1;
    })])]).range([height, 0]);
    var xAxis = d3.svg.axis().scale(xScale).orient("bottom").ticks(function (d) {
      return d.ward;
    });
    var yAxis = d3.svg.axis().scale(yScale).orient("left");
    xScale.domain(data.map(function (d) {
      return d.ward;
    }));
    var svg = d3.select(div).append("svg").attr("width", width + margin.left + margin.right).attr("height", height + 2 * margin.top + margin.bottom).attr("class", "svgs2").append("g").attr("transform", "translate(" + margin.left + "," + margin.top + ")");
    svg.append("g").attr("class", "axis").attr("transform", "translate(0," + height + ")").call(xAxis).selectAll("text").style("text-anchor", "end").attr("dx", "-.8em").attr("dy", "-.55em").attr("transform", "rotate(-90)");
    svg.append("g").attr("class", "axis").call(yAxis);
    var tooltip = d3.select(div).append("div").attr("class", "tooltip").style("opacity", 0);
    svg.selectAll("bar").data(data).enter().append("rect").on("mouseover", function (d) {
      tooltip.text(d.ward + ", " + correctDp(d.info, options.decimals)).style("opacity", 0.8).style("left", d3.event.pageX + 0 + "px").style("top", d3.event.pageY - 0 + "px");
    }).on("mouseout", function (d) {
      tooltip.style("opacity", 0);
    }).attr("x", function (d) {
      return xScale(d.ward);
    }).attr("y", function (d) {
      return d.info < 0 ? yScale(0) : yScale(d.info);
    }).attr("width", xScale.rangeBand()).attr("height", function (d) {
      return Math.abs(yScale(d.info) - yScale(0));
    }).style("fill", function (d) {
      //uses colourScheme.reusedColours
      if (d.ward == ward) {
        return colourScheme.reusedColours.darkBlue;
      } else {
        return colourScheme.reusedColours.darkOrange;
      }
    });
  }

  draw(data);
}