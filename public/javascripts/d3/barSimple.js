"use strict";

function smpBar(div, data) {
  var margin = {
    top: 20,
    right: 20,
    bottom: 30,
    left: 40
  },
      width = 400 - margin.left - margin.right,
      height = 500 - margin.top - margin.bottom;
  var x = d3.scale.ordinal().rangeRoundBands([0, width], .1);
  var y = d3.scale.linear().range([height, 0]);
  var svg = d3.select(div).append("svg").attr("width", width + margin.left + margin.right).attr("height", height + margin.top + margin.bottom).append("g").attr("transform", "translate(" + margin.left + "," + margin.top + ")");
  x.domain(data.map(function (d) {
    return d.letter;
  }));
  y.domain([0, d3.max(data, function (d) {
    return d.frequency;
  })]);
  svg.selectAll(".bar").data(data).enter().append("rect").attr("class", "bar").attr("x", function (d) {
    return x(d.letter);
  }).attr("width", x.rangeBand()).attr("y", function (d) {
    return y(d.frequency);
  }).attr("height", function (d) {
    return height - y(d.frequency);
  }).attr("fill", colourScheme.reusedColours.red);
}