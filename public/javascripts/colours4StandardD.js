"use strict";

function colStd(stndName, wardChecker, brsChecker, standard) {
  var clrs = "grey"; //high is signi bad

  if (stndName === "pupils-classed-as-disadvantaged-in-bristol.json" || stndName === "pupils-with-special-educational-needs-in-bristol.json" || stndName === "free-school-meals-in-bristol.json" || stndName === "pupil-absence-in-bristol.json" || stndName === "% overweight or obese" || stndName === "% who consume above recommended amount of sugar" || stndName === "% households where someone smokes regularly within the home" || stndName == "% with illness or health condition which limits day-to-day activities") {
    if (wardChecker < brsChecker - standard) {
      clrs = "green";
    } else if (wardChecker > brsChecker + standard) {
      clrs = "red";
    } else {
      clrs = "yellow";
    } //high is signi good

  } else if (stndName === "progress-8-and-attainment-8-scores-in-bristol.json" || stndName === "early-years-pupils-achieving-a-good-level-of-development-in-bristol.json" || stndName === "key-stage-2-pupils-level-4-in-reading-writing-maths-combined.json" || stndName === "progress-8-and-attainment-8-scores-in-bristol.json" || stndName === "% who do enough regular exercise each week (at least 150 mins moderate or 75 mins vigorous exercise)" || stndName === "% above average mental wellbeing" || stndName === "% in good health") {
    if (wardChecker < brsChecker - standard) {
      clrs = "red";
    } else if (wardChecker > brsChecker + standard) {
      clrs = "green";
    } else {
      clrs = "yellow";
    } //high or low

  } else if (stndName === "pupils-with-english-as-an-additional-language-in-bristol-by-ward.json" || stndName === "population-estimates-mid-2017-by-five-year-age-band-and-sex-2016-ward.json" || stndName === "/population/population-estimates-mid-2017-by-five-year-age-band-and-sex-2016-ward.json" || stndName === "/population/ethnicity.json" || stndName === "/population/religion-2016-ward.json" || stndName === "/housing/housing-tenure-2011-census-by-2016-ward.json" || stndName === "/housing/housing-type-2011-census-by-2016-ward.json" || stndName === "/housing/household-size-and-bedrooms-2011-census-by-2016-ward.json" || stndName === "/housing/car-availability.json") {
    console.log("High or low");

    if (wardChecker < brsChecker - standard) {
      console.log("Significantly less");
      clrs = "mauve";
    } else if (wardChecker > brsChecker + standard) {
      console.log("Significanly greater");
      clrs = "purple";
    } else {
      console.log("not significantlyy Different");
      clrs = "yellow";
    }
  }

  return clrs;
}