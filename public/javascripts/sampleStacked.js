"use strict";

function stackedv2(indicators, ward) {
  console.log("indicators", indicators); // select the svg area

  var initStackedBarChart = {
    draw: function draw(config) {
      var me = this,
          domEle = config.element,
          stackKey = config.key,
          data = config.data,
          margin = {
        top: 20,
        right: 100,
        bottom: 60,
        left: 180
      },
          width = 660 - margin.left - margin.right,
          height = 500 - margin.top - margin.bottom,
          xScale = d3.scaleLinear().rangeRound([0, width]),
          yScale = d3.scaleBand().rangeRound([height, 0]).padding(0.3),
          xAxis = d3.axisBottom(xScale),
          yAxis = d3.axisLeft(yScale),
          svg = d3.select("#" + domEle).append("svg").attr("width", width + margin.left + margin.right).attr("height", height + margin.top + margin.bottom).append("g").attr("transform", "translate(" + margin.left + "," + margin.top + ")");
      var stack = d3.stack().keys(stackKey).offset(d3.stackOffsetNone);
      var layers = stack(data);
      data.sort(function (a, b) {
        return b.total - a.total;
      });
      yScale.domain(data.map(function (d) {
        return d.name;
      }));
      xScale.domain([0, d3.max(data, function (d) {
        return Math.max(d.total);
      })]).nice();
      var layer = svg.selectAll(".layer").data(layers).enter().append("g").attr("class", "layer").style("fill", function (d, i) {
        //uses colourScheme
        if (d[i].data.name == ward) {
          return color[i];
        } else {
          return color[i];
        }
      });
      layer.selectAll("rect").data(function (d) {
        return d;
      }).enter().append("rect").attr("y", function (d) {
        return yScale(d.data.name);
      }).attr("x", function (d) {
        return xScale(d[0]);
      }).attr("height", yScale.bandwidth()).attr("width", function (d) {
        return xScale(d[1]) - xScale(d[0]);
      }).attr("class", function (d, i) {
        return d.data.name == ward ? "Current" : "other";
      }).on("mouseover", function () {
        tooltip.style("display", null);
      }).on("mouseout", function () {
        tooltip.style("display", "none");
      }).on("mousemove", function (d, i) {
        var xPosition = d3.mouse(this)[0] - 15;
        var yPosition = d3.mouse(this)[1] - 25;
        tooltip.attr("transform", "translate(" + xPosition + "," + yPosition + ")");
        tooltip.select("text").text(numberWithCommas(Math.round(d[1] - d[0])));
      });
      svg.append("g").attr("class", "axis axis--x").attr("transform", "translate(0," + (height + 5) + ")").call(xAxis.tickFormat(d3.format(".0s")).ticks(3));
      svg.append("text").attr("text-anchor", "middle") // this makes it easy to centre the text as the transform is applied to the anchor
      .attr("x", 192).attr("y", 460).text("Population");
      svg.append("g").attr("class", "axis axis--y").attr("transform", "translate(0,0)").call(yAxis); // Draw legend

      var legend = svg.selectAll(".legend").data(color).enter().append("g").attr("class", "legend").attr("transform", function (d, i) {
        return "translate(30," + i * 19 + ")";
      });
      legend.append("rect").attr("x", width - 30).attr("width", 18).attr("height", 18).style("fill", function (d, i) {
        return color[i];
      });
      legend.append("text").attr("x", width - 5).attr("y", 9).attr("dy", ".35em").style("text-anchor", "start").text(function (d, i) {
        switch (i) {
          case 0:
            return "Children";

          case 1:
            return "Working Age";

          case 2:
            return "Older People";

          case 2:
            return "Ward English";

          case 4:
            return "Ward Not English";
        }
      }); // Prep the tooltip bits, initial display is hidden

      var tooltip = svg.append("g").attr("class", "tooltip").style("display", "none");
      tooltip.append("rect").attr("width", 30).attr("height", 20).attr("fill", colourScheme.reusedColours.white).style("opacity", 0.5);
      tooltip.append("text").attr("x", 15).attr("dy", "1.2em").style("text-anchor", "middle").attr("font-size", "12px").attr("font-weight", "bold");
    }
  }; //var data = [{"date":"4/1854","total":8571,"disease":1,"wounds":0,"other":5},{"date":"5/1854","total":23333,"disease":12,"wounds":0,"other":9},{"date":"6/1854","total":28333,"disease":11,"wounds":0,"other":6},{"date":"7/1854","total":28772,"disease":359,"wounds":0,"other":23},{"date":"8/1854","total":30246,"disease":828,"wounds":1,"other":30},{"date":"9/1854","total":30290,"disease":788,"wounds":81,"other":70},{"date":"10/1854","total":30643,"disease":503,"wounds":132,"other":128},{"date":"11/1854","total":29736,"disease":844,"wounds":287,"other":106},{"date":"12/1854","total":32779,"disease":1725,"wounds":114,"other":131},{"date":"1/1855","total":32393,"disease":2761,"wounds":83,"other":324},{"date":"2/1855","total":30919,"disease":2120,"wounds":42,"other":361},{"date":"3/1855","total":30107,"disease":1205,"wounds":32,"other":172},{"date":"4/1855","total":32252,"disease":477,"wounds":48,"other":57},{"date":"5/1855","total":35473,"disease":508,"wounds":49,"other":37},{"date":"6/1855","total":38863,"disease":802,"wounds":209,"other":31},{"date":"7/1855","total":42647,"disease":382,"wounds":134,"other":33},{"date":"8/1855","total":44614,"disease":483,"wounds":164,"other":25},{"date":"9/1855","total":47751,"disease":189,"wounds":276,"other":20},{"date":"10/1855","total":46852,"disease":128,"wounds":53,"other":18},{"date":"11/1855","total":37853,"disease":178,"wounds":33,"other":32},{"date":"12/1855","total":43217,"disease":91,"wounds":18,"other":28},{"date":"1/1856","total":44212,"disease":42,"wounds":2,"other":48},{"date":"2/1856","total":43485,"disease":24,"wounds":0,"other":19},{"date":"3/1856","total":46140,"disease":15,"wounds":0,"other":35}];

  var data = indicators;
  var key = ["children", "workingAge", "olderPeople"];
  var color = [colourScheme.stack.darkOrange, colourScheme.stack.midOrange, colourScheme.stack.lightOrange];
  var formatSuffix = d3.format("s");
  initStackedBarChart.draw({
    data: data,
    key: key,
    element: 'stacked-bar'
  });
  $("rect.Current").first().addClass("blue1");
  $("rect.Current").eq(1).addClass("blue2");
  $("rect.Current").last().addClass("blue3");
}