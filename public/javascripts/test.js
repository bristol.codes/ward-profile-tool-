"use strict";

function sampleFuncForQofL(fileName, amount, sign) {
  var isBristolAverageFromJsonIfAvailable = true;
  var displayBristolBar = false;
  console.log("sampleFuncForQofL: isBristolAverageFromJsonIfAvailable:" + isBristolAverageFromJsonIfAvailable); // ward is global!

  var testing = changeName(ward, sign);
  ward = testing;
  $.getJSON("../../Json/" + fileName, function (data) {
    var filteredData = data;

    if (!displayBristolBar) {
      filteredData = filteredData.filter(function (item, index, arr) {
        return !(item.ward == "Bristol" || item.ward == ".Bristol Average");
      });
    }

    var allWards = [];
    var eachWardNr = [];
    /*For trend Line*/

    var thisWard = 0;
    var justBristol = 0;
    var circleChecker = [];
    var circleChecker2 = [];
    /*Standard Deviation*/

    var mean = 0;
    var clrs = colourScheme.reusedColours.red;
    $.each(data, function (i, obj) {
      var indicator = data[i].fields.indicator;
      var wardN = data[i].fields.ward_name;
      var dataz = data[i].fields.statistic;
      var rounded = to15sf(dataz);
      var colR = data[i].fields.significance_colour;

      if (amount == indicator) {
        var feed2 = new Object();
        feed2.ward = wardN;
        feed2.info = to15sf(dataz);
        allWards.push(feed2);
      } // ward and indicator we're interested in amongst the json data points


      if (wardN == ward && amount == indicator) {
        clrs = returnReusableColourFromQoLColour(colR, clrs);
        var circleObj = new Object();
        circleObj.name = wardN;
        circleObj.info = rounded; //feed1.date = date;

        circleObj.x = "220";
        circleChecker.push(circleObj);
        thisWard = thisWard + dataz;
      }

      if (wardN !== "Bristol" && amount == indicator) {
        mean = mean + dataz;
        eachWardNr.push(dataz);
      } //if it is the bristol ward and the specified year


      if (wardN === ".Bristol Average" && amount == indicator) {
        var _circleObj = new Object();

        _circleObj.name = wardN;
        _circleObj.info = rounded; // feed1.date = date;

        _circleObj.x = "220";
        circleChecker2.push(_circleObj);
        justBristol = justBristol + dataz;
      }
    }); //json

    var wardChecker = circleChecker[0].info;
    var brsChecker = circleChecker2[0].info;
    var fixedSize = 20; //100% in other words

    sampleBar("#test", allWards, {
      tooltipDecimals: 1,
      displayBristolBar: false,
      addedToMouseOverUnits: '%'
    });

    if (wardChecker > brsChecker) {
      var calcNew = 100 / wardChecker;
      var test = brsChecker * calcNew;
      var make20 = test / 5;
      make20 = 20 / make20;
      make20 = make20 * 20;
      sampleCirc("#test2", colourScheme.reusedColours.grey, circleChecker2, "right", fixedSize);
      sampleCirc("#test2", clrs, circleChecker, "left", make20);
    } else if (wardChecker < brsChecker) {
      var calcNew = 100 / brsChecker;
      var test = wardChecker * calcNew;
      var make20 = test / 5;
      sampleCirc("#test2", colourScheme.sampleCircle.grey, circleChecker2, "right", fixedSize);
      sampleCirc("#test2", clrs, circleChecker, "left", make20);
    } else {
      sampleCirc("#test2", colourScheme.sampleCircle.grey, circleChecker2, "right", fixedSize);
      sampleCirc("#test2", clrs, circleChecker, "left", fixedSize);
    }
  }); //end of json
} //end of func


function returnReusableColourFromQoLColour(colR, clrs) {
  if (colR == "Green") {
    clrs = colourScheme.reusedColours.green;
  } else if (colR == "Purple") {
    clrs = colourScheme.reusedColours.purple;
  } else if (colR == "Amber") {
    clrs = colourScheme.reusedColours.offyellow;
  } else if (colR == "Mauve") {
    clrs = colourScheme.reusedColours.pink;
  } else if (colR == "Red") {
    clrs = colourScheme.reusedColours.red;
  }

  return clrs;
}