var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var d3 = require("d3");
var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
var xhr = new XMLHttpRequest();

var indexRouter = require('./routes/index');
var ashleyRouter = require('./routes/ashley');
var bedminsterRouter = require('./routes/bedminster');
var bishopRouter = require('./routes/bishopston');
var avonRouter = require('./routes/avonmouth');
var bishopsworthRouter = require('./routes/bishopsworth');
var brislingtonEastRouter = require('./routes/brislington_east');
var brislingtonWestRouter = require('./routes/brislington_west');
var central = require('./routes/central');
var clifton = require('./routes/clifton');
var clifton_down = require('./routes/clifton_down');
var cotham = require('./routes/cotham');
var easton = require('./routes/easton');
var eastville = require('./routes/eastville');
var filwood = require('./routes/filwood');
var frome_vale = require('./routes/frome_vale');
var hartcliffe_and_withywood = require('./routes/hartcliffe_and_withywood');
var henbury_and_brentry = require('./routes/henbury_and_brentry');
var hengrove_and_whitchurch_park = require('./routes/hengrove_and_whitchurch_park');
var hillfields = require('./routes/hillfields');
var horfield = require('./routes/horfield');
var hotwells_and_harbourside = require('./routes/hotwells_and_harbourside');
var knowle = require('./routes/knowle');
var lawrence_hill = require('./routes/lawrence_hill');
var lockleaze = require('./routes/lockleaze');
var redland = require('./routes/redland');
var st_george_central = require('./routes/st_george_central');
var st_george_west = require('./routes/st_george_west');
var southmead = require('./routes/southmead');
var southville = require('./routes/southville');
var stockwood = require('./routes/stockwood');
var stoke_bishop = require('./routes/stoke');
var westbury = require('./routes/westbury');
var windmill_hill = require('./routes/windmill_hill');
var st_george_troopers_hill = require('./routes/st_george_troopers_hill');


var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/scripts', express.static(__dirname + '/node_modules/bootstrap/dist/'));
app.use('/', indexRouter);
app.use('/wards/ashley', ashleyRouter);
app.use('/wards/bedminster', bedminsterRouter);
app.use('/wards/bishopston_and_ashley_down', bishopRouter);
app.use('/wards/avonmouth_and_lawrence_weston', avonRouter);
app.use('/wards/bishopsworth', bishopsworthRouter);
app.use('/wards/brislington_east', brislingtonEastRouter);
app.use('/wards/brislington_west', brislingtonWestRouter);
app.use('/wards/central', central);
app.use('/wards/clifton', clifton);
app.use('/wards/clifton_down', clifton_down);
app.use('/wards/cotham', cotham);
app.use('/wards/easton', easton);
app.use('/wards/eastville', eastville);
app.use('/wards/filwood', filwood);
app.use('/wards/frome_vale', frome_vale);
app.use('/wards/hartcliffe_and_withywood', hartcliffe_and_withywood);
app.use('/wards/henbury_and_brentry', henbury_and_brentry);
app.use('/wards/hengrove_and_whitchurch_park', hengrove_and_whitchurch_park);
app.use('/wards/hillfields', hillfields);
app.use('/wards/horfield', horfield);
app.use('/wards/hotwells_and_harbourside', hotwells_and_harbourside);
app.use('/wards/knowle', knowle);
app.use('/wards/lawrence_hill', lawrence_hill);
app.use('/wards/lockleaze', lockleaze);
app.use('/wards/redland', redland);
app.use('/wards/st_george_central', st_george_central);
app.use('/wards/st_george_west', st_george_west);
app.use('/wards/southmead', southmead);
app.use('/wards/southville', southville);
app.use('/wards/stockwood', stockwood);
app.use('/wards/stoke_bishop', stoke_bishop);
app.use('/wards/westbury-on-Trym_and_henleaze', westbury);
app.use('/wards/windmill_hill', windmill_hill);
app.use('/wards/st_george_troopers_hill', st_george_troopers_hill);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
